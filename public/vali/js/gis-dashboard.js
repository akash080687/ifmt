var workspace = "ifmt";
var baseurl = dpurl + "/" + workspace +"/wms";
var layerList=[];
var map, extent;

var http = new XMLHttpRequest();
http.open('HEAD',baseurl, false);
http.send();

if(http.status == 200)
{
	$(document).ready(function () {
		var height = window.innerHeight - $(".app-header").height() - 100;
		$("#layerController").height(height);
		if(verifiedStatus == 0){
			var seconds = 11;
			function decrementSeconds() {
				seconds -= 1;
				$("#secCounter").html(seconds);
			}
			window.setTimeout(function(){
				window.location.href = "/DataUpload/ForestAdminBoundaries";
			}, 10000);
			setInterval(decrementSeconds, 1000);
		} else{
			var view = new ol.View({
				center:  ol.proj.transform([78.96, 21.59], 'EPSG:4326', 'EPSG:3857'),
				zoom: 4,
				projection: 'EPSG:3857'
			});

			var osmLayer = new ol.layer.Tile({
				source: new ol.source.OSM()
			});

			var googleLayer = new olgm.layer.Google();

			var mousePositionControl = new ol.control.MousePosition({
					coordinateFormat: ol.coordinate.createStringXY(4),
					projection: 'EPSG:3857',
				// comment the following two lines to have the mouse position
				// be placed within the map.
				className: 'custom-mouse-position',
				target: document.getElementById('latlonInfo'),
				undefinedHTML: '&nbsp;'
			});

			var scaleLine = new ol.control.ScaleLine({
				minWidth: 100
			});


			map = new ol.Map({
				target: 'map',
				interactions: olgm.interaction.defaults()
			});
			map.setView(view);
			map.addLayer(googleLayer);
			map.addControl(mousePositionControl);
			map.addControl(scaleLine);
			var olGM = new olgm.OLGoogleMaps({map: map});
			olGM.activate();
			$.ajax({
				url : "/GisDashboard/getLayerList",
				type: 'get',
				success : function(list) {
					parsedList = JSON.parse(list);
					var layerCtr = document.createElement('div');
					layerCtr.className = 'list-group';
					for(i=0;i<parsedList.length;i++){
						if(parsedList[i]['h_order'] == 999){
							baseurl = dpurl + "/" + parsedList[i]['workspace'] + "/wms";
							layerList[i] = new ol.layer.Tile({
								source : new ol.source.TileWMS({
						            // crossOrigin : "anonymous",
						            params : {
						            	'LAYERS' : parsedList[i]['workspace']+":"+parsedList[i]['layername']
						            },
						            url : baseurl
						        })
							});
						} else{
							layerList[i] = new ol.layer.Tile({
								source : new ol.source.TileWMS({
						            // crossOrigin : "anonymous",
						            params : {
						            	'LAYERS' : "ifmt:"+parsedList[i]['layername'],
						            	'cql_filter': "create_by='" + username + "'"
						            },
						            url : baseurl
						        })
							});	
						}
						layerList[i].set('Name', parsedList[i]['name']);
						map.addLayer(layerList[i]);

						if(i==0){
							/*var extent = layerList[i].getExtent();
		 					map.getView().fit(extent, map.getSize());
		 					*/
		 					var parser = new ol.format.WMSCapabilities();
		 					fetch(baseurl + '?service=WMS&VERSION=1.3.0&REQUEST=GetCapabilities').then(function(response) {
		 						return response.text();
		 					}).then(function(text) {
		 						var result = parser.read(text);
		 						for(j=0;j<result.Capability.Layer.Layer.length;j++){
		 							if(result.Capability.Layer.Layer[j]['Name'] == parsedList[0]['layername']){
		 								var extent = result.Capability.Layer.Layer[j].EX_GeographicBoundingBox;
								    	//extent = ol.proj.transform(extent_temp, 'EPSG:4326', 'EPSG:3857');	
								    	map.getView().setCenter(ol.proj.transform(ol.extent.getCenter(extent), 'EPSG:4326', 'EPSG:3857'));
								    	map.getView().setZoom(7);
								    }
								}
						    	//var extent_temp = result.Capability.Layer.Layer.find(l => l.Name === workspace + ":" + parsedList[0]['layername']).EX_GeographicBoundingBox;
						    });
		 				} else{
		 					layerList[i].setVisible(false);
		 				}

		 				var legendRow = document.createElement('div');
		 				legendRow.id = 'row_'+i;
		 				legendRow.className = 'legendRow';
		 				var img = document.createElement('img');
		 				img.src = baseurl + '?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER='+ workspace +':'+ parsedList[i]['layername'] +'';
		 				var label = document.createElement('div');
		 				label.className = 'label';
		 				label.innerHTML = parsedList[i]['name'];
		 				legendRow.appendChild(label);
		 				legendRow.appendChild(img);
		 				document.getElementById('legendContent').appendChild(legendRow);

		 				var layerCtrR = document.createElement('div');
		 				layerCtrR.id = 'labelCtr_'+i;
		 				layerCtrR.className = 'list-group-item row';	
		 				layerCtrDiv1 = document.createElement('div');
		 				layerCtrDiv1.innerHTML = parsedList[i]['name'];
		 				layerCtrDiv1.className = 'col-md-12 lyrCtrLeft';
		 				var layerCtrShowHideC = document.createElement('div');
		 				layerCtrShowHideC.className = 'material-switch pull-right';
		 				layerCtrShowHideC.title = 'Set layer visibility';
		 				var layerCtrI = document.createElement('input');
		 				layerCtrI.id = 'layerCtrInput_'+i;
		 				layerCtrI.name = 'layerCtrInput_'+i;
		 				layerCtrI.type = 'checkbox';
		 				if(i==0){
		 					layerCtrI.checked = true;
		 				}
		 				layerCtrI.onclick = function(){
		 					var id = this.id.split("_");
		 					if (this.checked){
		 						layerList[id[1]].setVisible(true);
		 					} else {
		 						layerList[id[1]].setVisible(false);
		 					}
		 				}
		 				var layerCtrL = document.createElement('label');
		 				layerCtrL.setAttribute ('for', 'layerCtrInput_'+i);
						//layerCtrL.for = 'layerCtrInput'+i;
						layerCtrL.className = 'label-success';

						var layerCtrDiv2 = document.createElement('div');
						layerCtrDiv2.className = 'col-md-4 lyrCtrRight';
						layerCtrDiv2.id = 'div_'+i;
						layerCtrShowHideC.appendChild(layerCtrI);
						layerCtrShowHideC.appendChild(layerCtrL);
						layerCtrDiv1.appendChild(layerCtrShowHideC);
						layerCtrR.appendChild(layerCtrDiv1);
						//layerCtrR.appendChild(layerCtrDiv2);
						layerCtr.appendChild(layerCtrR);			
					}
					document.getElementById('layerController').appendChild(layerCtr);	
				},
				error: function() {
					swal("","Error: Fetching layers list failed. Contact site administrator","error");
				}
			});
			var popup = new ol.Overlay.Popup;
			popup.setOffset([0, -55]);
			map.addOverlay(popup);
			//Feature info
			map.on('click', function(evt) {
				var f = map.forEachFeatureAtPixel(
					evt.pixel,
					function(ft, layer){return ft;}
					);
				if (f && f.get('type') == 'click') {
					var geometry = f.getGeometry();
					var coord = geometry.getCoordinates();

					var content = '<p>'+f.get('desc')+'</p>';

					popup.show(coord, content);

				} else { popup.hide(); }

			});
		}
		$(window).on('resize', function(){
			var height = window.innerHeight - $(".app-header").height() - 100;
			$("#layerController").height(height);
		});
	});

	$('#addBoundary').on('click',function(){
		
	});
}else{
	alert("Maintenance is going on your patience is appreciated.");
}


