$(document).ready(function(){	
	$('INPUT[type="file"]').change(function () {
		var ext = $('#csv_file').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['csv']) == -1) {
			swal("","Invalid file format. Please upload csv file only.","error");
			$('#csv_file').val("");
			$("#submitButton").hide();
		} else{
			$("#submitButton").show();
		}
	});
});

function getChild(elemID, childIndex, childName){
	childVal = 2;
	parentVal = $("#"+elemID).val();
	$("#row_" + childVal).remove();
	$("#csvUpload").hide();
	if($("#"+elemID).val() > 0){
		$.ajax({
			url: "/DataUpload/getChildList",
			type: "post",
			data: {'id': childVal, 'parentVal' : parentVal},
			success: function(response){
				var data = JSON.parse(response);
				if("error" in data){
					swal("Sorry!", "There is a problem processing your request. Please wait for some time or you can contact site administrator", "error");
					$("#bndTypeSuccess, #formContainer").hide();
					$("#bndTypeContainer").show();
				} else{
					renderChildDropdown(data, childVal, childName, childIndex);
				}
			},
			error: function(){
				swal("Sorry!", "There is a problem processing your request. Please wait for some time or you can contact site administrator", "error");
			}
		});
	}
}

function renderChildDropdown(data, value, name, childIndex){
	if($("#select_"+value).length){
		$("#row_"+value).nextAll('div').remove();
		$("#row_"+value).remove();
	}
	var html = '<div class="row" id="row_'+value+'">';
	html += '	<div class="col-md-1"><span class="badge-custom badge-primary">'+ childIndex +'</span></div>';
	html += '	<div class="col-md-11">';
	html += '		<div class="form-group">';
	html += '			<label for="exampleSelect1">Select '+name+'</label>';
	html += '			<select class="form-control" id="select_sop" name= "select_sop" onchange="showCSVUpload(this.id)">';
	html += '				<option value="0">Select</option>';
	for(i=0;i<data.length;i++){
		html += '					<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>';
	}
	html += '			</select>';
	html += '		</div>';
	html += '	</div>';
	html += '</div>';
	$("#parentsWrapper").append(html);
}

function showCSVUpload(id){
	if($("#"+ id).val() > 0){
		$("#csvUpload").show();
	} else{
		$("#csvUpload").hide();
	}
}
