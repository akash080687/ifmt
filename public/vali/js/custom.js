$(document).ready(function() {
  $('#sdmForm').hide();
  $('#runNewSdm').click(function() {
    $('#sdmForm').slideToggle();
    //.toggleClass('outPort');
  });
  $('#downloadReport').hide();
  $('#loader').hide();
  
  $('.filters').click(function(){
  	$('.filterSection').toggle('slow');
  	text = $('#filterMap h4').text();
  	$('#filterMap h4').text(text == "Hide filters" ? "Show filters" : "Hide filters");
  });

  $('#selectRange').hide();

  $(document).on('change','#selectReport',function(){
    $('#downloadReport').hide();
    $('#getReport').removeClass('disabled');
    $('#getReport').css('cursor','pointer');
    if($('#selectReport option:selected').val() == 'CH3')
    {
      $('#selectRange').show();
    }else{
      $('#selectRange').hide();
    }
  });

  $('#getReportDownload').submit(function(e){
    e.preventDefault();
    if($('#selectReport option:selected').val() == 'CH3')
    {
      data = 'rep=CH3&range='+$('#selectRange select option:selected').val();
    }else{
      data = 'rep='+$('#selectReport option:selected').val();
    }
    $('#loader').show();
    $.ajax({
      url: "/Report/index",
      data: data,
      type: 'POST',
      success:function(response)
      {
        $('#getReport').addClass('disabled');
        $('#getReport').css('cursor','not-allowed');
        $('#downloadReport').show();
        $('#loader').hide();
        $('#downloadReport').on('click',function(){
          repName=$('#selectReport option:selected').val();
          if(repName == 'CH1')
          {
            file = 'compartment_history_1';
          }if(repName == 'CH2A'){
            file = 'compartment_history_2a';
          }if(repName == 'CH2B'){
            file = 'compartment_history_2b';
          }if(repName == 'CH3'){
            file = 'compartment_history_3';
          }if(repName == 'CH5'){
            file = 'compartment_history_5';
          }
          window.location = '/files/temp/'+file+'.xlsx';
        });
      }
    });

  });

// -----------------------
// Check login on ajax
// -----------------------
// setInterval(function(){
//   $.ajax({
//     url : "/User/checkAccess",
//     success : function(response)
//     {
//       if(!response && sessionStorage.stopCheck)
//       {
//           window.location = "/Home/index";
//           sessionStorage.stopCheck = true;
//       }
//     }
//   });
// }, 3500);
});