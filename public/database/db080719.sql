/*
* Query for new table for storing point
****************************************
* Point saves is served locally no need of geoserver
*/
create sequence user_point_data_seq start 1;

create table user_point_data
(
	id integer NOT NULL DEFAULT nextval('user_point_data_seq'::regclass),
	user_id text REFERENCES user_master (user_id),
	location_info jsonb,
	layer_name text,
	nice_name text,
	geoserver_url text,
	date_created timestamp,
	isverified boolean default false
)

/*
* Vector saves is served locally no need of geoserver
*/
create sequence user_vector_data_seq start 1;

create table user_vector_data
(
	id integer NOT NULL DEFAULT nextval('user_vector_data_seq'::regclass),
	user_id text REFERENCES user_master (user_id),
	file_name text,
	layer_name text,
	nice_name text,
	geoserver_url text,
	date_created timestamp,
	isverified boolean default false
)

create sequence user_raster_data_seq start 1;

/*
* Raster saves in geoserver 
*/
create table user_raster_data
(
	id integer NOT NULL DEFAULT nextval('user_raster_data_seq'::regclass),
	user_id text REFERENCES user_master (user_id),
	file_name text,
	layer_name text,
	nice_name text,
	geoserver_url text,
	date_created timestamp,
	isverified boolean default false
)

grant all on user_raster_data_seq to ifmt_user;
grant all on user_raster_data to ifmt_user;
	
grant all on user_point_data_seq to ifmt_user;
grant all on user_point_data to ifmt_user;

grant all on user_vector_data_seq to ifmt_user;
grant all on user_vector_data to ifmt_user;

grant all on user_raster_data_seq to ifmt_user;
grant all on user_raster_data to ifmt_user;