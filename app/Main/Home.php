<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
/**
* Home Controller
*/
class Home extends App
{
        protected $auth;
        public function __construct()
        {
                parent::__construct();
        }

        public function index()
        {
                $title = 'Homepage';
                return $this->view('index.html',compact('title'));
        }
        
        public function ifmtHome()
        {
                if($this->auth->userExists())
                {
                        $title = "Home";
                        return $this->view('index.html');
                } else{
                        // prompt login
                        $this->redirect(array('User','login'));
                }
        }

        public function about()
        {
                $title = "About";
                $this->view('about.html',compact('title'));
        }
}