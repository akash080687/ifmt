<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\ReportModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
/**
 * Report Module
 */
class Report extends App
{
	private $reportModel;
	private $repeatsAttr;
	private $speciesAttributes;
	private $ch1CellRows;
	private $sheet;
	private $writer;
	public function __construct()
	{
		parent::__construct();
		$spreadsheet = new Spreadsheet();
		$this->reportModel = new ReportModel;
		$this->writer = IOFactory::createWriter($spreadsheet, "Xlsx");
		$this->sheet = $spreadsheet->getActiveSheet();
		$this->repeatsAttr = array('76','21','82','84','85','86','31');
		$this->speciesAttributes = array('324');
		$this->ch1CellRows = array();
	}

	public function index()
	{
		if(isset($_POST['rep']))
		{
			switch ($_POST['rep']) {
				case 'CH1':
					$table = $this->getCh1Report();
					break;
				case 'CH2A':
					$table = $this->getCh2aReport();
					break;
				case 'CH2B':
					$table = $this->getCh2bReport();
					break;
				case 'CH3':
					$table = $this->getCh3Report($_POST['range']);
					break;
				case 'CH5':
					$table = $this->getCh5Report();
					break;
			}
		}
	}

	public function getCh1Report()
	{
		$colspanMove = 2;
		$subHead = 2;
		$num = 1;
		$arr = $this->reportModel->getCh1Report();
		$this->sheet->setCellValue('A'.$num,'Category');
		$this->sheet->setCellValue('B'.$num,'Compartment Number');
		foreach (array_keys($arr) as $key => $value) {
			$row = $this->num2alpha($key+2);
			$this->sheet->setCellValue($row.$num, $value);
		}
		$innerTab = $this->getHeads('ch1');
		$inneTabCounts = array_sum(array_map("count", $innerTab));
		$this->generateCellRow($arr,$inneTabCounts);
		foreach ($innerTab as $key=>$value) 
		{
			$cell = $this->num2alpha(0).$colspanMove;
			$cellsMoved = $colspanMove+count($value)-1;
			$span = $this->num2alpha(0).$cellsMoved;
			$this->sheet->mergeCells("$cell:$span");
			$this->sheet->getCell($cell)->setValue($key);
			$colspanMove = $cellsMoved+1;
			foreach ($value as $ik => $iv) 
			{
				$cell = $this->num2alpha(1).$subHead;
				$this->sheet->setCellValue($cell,$ik);
				foreach (array_keys($arr) as $k => $v) {
					$data = isset($arr[$v][$iv]) ? $arr[$v][$iv] : "";
					if($data == "")
					{
						$tmp = $arr[$v];
						$finKey = true;
						while($finKey !== false)
						{
							$finKey = $this->findKey($tmp, $iv);
							if($finKey !== false)
							{
								if(!in_array($iv, $this->repeatsAttr))
								{
									$nameVal = explode(":", $tmp[$finKey][$iv][0]);
									$valExtracted = count($nameVal) == 2 ? $nameVal[1] : $nameVal[2];
									$data .= ",".$valExtracted;
								}else{
									foreach ($tmp[$finKey][$iv] as $repVal) {
										$nameVal = explode(":", $repVal);
										$data .= ",".$nameVal[1];	
									}
								}
								unset($tmp[$finKey]);
							}
						}
						$data = ltrim($data, ",");
						$data = implode(",", array_unique(explode(",", $data)));
					}
					$cell = $this->getRow();
					$this->sheet->setCellValue($cell,$data);
				}
				$subHead++;
			}
		}
		$this->writer->save($this->publicDir."/files/temp/compartment_history_1.xlsx");
	}

	public function generateCellRow($data, $sizeOf)
	{
		$minCell = 2;
		$maxCell = count(array_keys($data));
		for($i=2;$i<=$sizeOf+3;$i++)
		{
			for($j=$minCell;$j<=$maxCell+1;$j++)
			{
				$this->ch1CellRows[] = $this->num2alpha($j).$i;
			}
		}	
	}

	public function getRow()
	{
		return array_shift($this->ch1CellRows);
	}

	public function getCh2aReport()
	{
		$data = $this->reportModel->getCh2aReport();
		$heads = $this->getHeads('ch2a');
		$colCnt=0;
		foreach ($heads as $key=>$value) {
			$cell = $this->num2alpha($colCnt)."1";
			$this->sheet->setCellValue($cell, $key);
			$colCnt++;
		}
		$cnt = 1;
		$rowCnt=2;
		unset($heads['Serial']);
		foreach ($data as $k => $v) {
			$this->sheet->setCellValue("A".($cnt+1), $cnt);
			$colCnt = 2;
			foreach ($heads as $key => $value) {
				$valPrint = isset($v[$value]) ? $v[$value] : "-";
				$cell = $this->num2alpha($colCnt).$rowCnt;
				$this->sheet->setCellValue($cell, $valPrint);
				$colCnt++;
			}
			$rowCnt++;
			$cnt++;
		}
		$this->writer->save($this->publicDir."/files/temp/compartment_history_2a.xlsx");
		exit;
	}

	public function getCh2bReport()
	{
		$data = $this->reportModel->getCh2bReport();
		$heads = $this->getHeads('ch2b');
		$colCnt=0;
		foreach ($heads as $key=>$value) {
			$cell = $this->num2alpha($colCnt)."1";
			$this->sheet->setCellValue($cell, $key);
			$colCnt++;
		}
		$cnt = 1;
		$rowCnt=2;
		unset($heads['Serial']);
		foreach ($data as $k => $v) {
			$this->sheet->setCellValue("A".($cnt+1), $cnt);
			$colCnt = 2;
			foreach ($heads as $key => $value) {
				$valPrint = isset($v[$value]) ? $v[$value] : "-";
				$cell = $this->num2alpha($colCnt).$rowCnt;
				$this->sheet->setCellValue($cell, $valPrint);
				$colCnt++;
			}
			$rowCnt++;
			$cnt++;
		}
		$this->writer->save($this->publicDir."/files/temp/compartment_history_2b.xlsx");
		exit;
	}

	public function getCh3Report($rangeId)
	{
		$data = $this->reportModel->getCh3Report($rangeId);
		$heads = $this->getHeads('ch3');
		$colCnt=0;
		foreach ($heads as $key=>$value) {
			$cell = $this->num2alpha($colCnt)."1";
			$this->sheet->setCellValue($cell, $key);
			$colCnt++;
		}
		$cnt = 1;
		$rowCnt=2;
		unset($heads['Serial']);
		foreach ($data as $k => $v) {
			$this->sheet->setCellValue("A".($cnt+1), $cnt);
			$colCnt = 1;
			foreach ($heads as $key => $value) {
				$valPrint = isset($v[$value]) ? $v[$value] : ($value == '0.1' ? '0.1' : "-");
				$cell = $this->num2alpha($colCnt).$rowCnt;
				$this->sheet->setCellValue($cell, $valPrint);
				$colCnt++;
			}
			$rowCnt++;
			$cnt++;
		}
		$this->writer->save($this->publicDir."/files/temp/compartment_history_3.xlsx");
		exit;
	}

	public function getCh5Report()
	{
		$data = $this->reportModel->getCh5Report();
		$heads = $this->getHeads('ch5');
		$colCnt=0;
		foreach ($heads as $key=>$value) {
			$cell = $this->num2alpha($colCnt)."1";
			$this->sheet->setCellValue($cell, $key);
			$colCnt++;
		}
		$cnt = 1;
		$rowCnt=2;
		unset($heads['Serial']);
		foreach ($data as $k => $v) {
			$this->sheet->setCellValue("A".($cnt+1), $cnt);
			$colCnt = 2;
			foreach ($heads as $key => $value) {
				$valPrint = isset($v[$value]) ? $v[$value] : ($value == '0.1' ? '0.1' : "-");
				$cell = $this->num2alpha($colCnt).$rowCnt;
				$this->sheet->setCellValue($cell, $valPrint);
				$colCnt++;
			}
			$rowCnt++;
			$cnt++;
		}
		$this->writer->save($this->publicDir."/files/temp/compartment_history_5.xlsx");
		exit;
	}

	public function getHeads($formId)
	{
		switch ($formId) {
			case 'ch1':
				return array(
					'Location'=>[
						'Plot Number'=>'plot',
						'Forest Range'=>'range',
						'Forest Block'=>'block',
						'GPS Reading (Latitude)'=>'89',
						'GPS Reading (Longitude)'=>'90'
					],
					'Land use and Topography'=>[
						'Legal status'=>'17',
						'Land Use'=>'18',
						'General Topography'=>'19',
						'Altitude from GIS'=>'91',
						'Gradient/slope'=>'35',
						'Rock and Geology'=>'71'
					],
					'Land use and Topography'=>[
						'Legal status'=>'17',
						'Land Use'=>'18',
						'General Topography'=>'19',
						'Altitude from GIS'=>'91',
						'Gradient/slope'=>'35',
						'Rock and Geology'=>'71'
					],
					'Soil'=>[
						'Soil Depth (in cm)'=>'74',
						'Soil Texture'=>'34',
						'Soil Permeability'=>'33',
						'Soil Erosion'=>'36',
					],
					'Crop Status'=>[
						'Crop composition of the plot'=>'76',
						'Regeneration status of the plot'=>'32',
						'Injury/Damage to crop, if any'=>'31'
					],
					'Grazing' => [
						'Grazing Incidence' => '30'
					],
					'Bamboo'=>[
						'Presence of Bamboo in plot'=>'319',
						'Bamboo density (per ha)'=>'',
						'Bamboo quality'=>'29',
						'Bamboo regeneration'=>'28',
					],
					'Grasses'=>[
						'Presence of Grasses'=>'27',
					],
					'Weed'=>[
						'Presence of Weeds'=>'26',
						'Name of Weed'=>'322',
						'Area of Plantation (ha)'=>'81',
					],
					'Plantation Details'=>[
						'Year of Plantation'=>'82',
						'Plantation Species'=>'84',
						'Spacement in meter'=>'85',
						'Average Crop Diameter'=>'86',
					],
					'Water Body	'=>[
						'Presence of Water body'=>'',
						'Type of Water body'=>'25',
						'Status of Water body'=>'24',
						'Seasonality of Water body'=>'23',
						'Potability of Water body'=>'22',
					],
					'Degradation'=>[
						'Drivers of Plot Degradation'=>'21',
					],
					'Faunal sightings'=>[
						'Faunal sightings'=>'',
						'Mammals'=>'Mammals',
						'Birds'=>'Birds',
						'Reptiles'=>'Reptiles',
						'Amphibians'=>'Amphibians',
					]
				);
				break;
			case 'ch2a':
				return array(
					'Serial'=>'cnt',
					'Forest Section'=>'range',
					'Forest Block'=>'block',
					'Compartment'=>'compartment',
					'Plot Numbers'=>'plot',
					'Sub compartment'=>'sub_compartment',
					'Total Area (ha)'=>'total_area',
					'Area enumerated'=>'area_enumerated',
					'Sampling method if Partial'=>'sampling_method_if_partial',
					'Year of Enumeration'=>'date_part'
				);
			case 'ch2b':
				return array(
					'Serial' => 'cnt',
					'Speices Name' => 'name',
					'Habit' => 'habit',
					'Total No. of Individual' => 'count',
					'DBH_0-10 in cm' => 0,
					'DBH_10-20 in cm' => 1,
					'DBH_20-30 in cm'=> 2,
					'DBH_30-40 in cm'=> 3,
					'DBH_40-50 in cm'=> 4,
					'DBH_50-60 in cm'=> 5,
					'DBH_60-70 in cm'=> 6,
					'DBH_70-80 in cm'=> 7,
					'DBH_80-90 in cm'=> 8,
					'DBH_90 or more'=> 9,
				);
			case 'ch3':
				return array(
					'Serial'=>'cnt',
					'Habit Type'=>'habit',
					'Name of the species'=>'spName',
					'Density (No. of tree/unit area)'=>'density',
					'Frequency (%)'=>'frequecy',
					'Total basal area (m2/ha)'=>'basal_area',
					'Relative Density'=>'rel_den',
					'Relative Frequency'=>'rel_freq',
					'Relative Basal Area'=>'rel_basal',
					'IVI'=>'ivi'
				);
			case 'ch5':
				return array(
					'Serial' => 'cnt',
					'Scientific Name' => 'sc_name',
					'Local Name' => 'local_name',
					'Type of plant(Habit)' => 'habit',
					'NTFP Parts' => 'parts',
					'Location where found (Compartment No./Beat/Range)' => 'area',
					'Area in Ha' => '0.1',
					'Potential harvesting quality per ha' => 'potential',
					'Estimated harvest/ha' => null,
					'Remark' => null
				);	
		}
	}

	function findKey($array, $keySearch)
	{
	    foreach ($array as $key => $item) {
	        if ($key == $keySearch) {
	            return $key;
	        } elseif (is_array($item) && $this->findKey($item, $keySearch)) {
	            return $key;
	        }
	    }
	    return false;
	}

	function num2alpha($n)
	{
	    for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
	        $r = chr($n%26 + 0x41) . $r;
	    return $r;
	}
}