<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\FormModel;
use IFMT\App\Model\ODKModel;
use IFMT\App\Model\SurveyModel;

/**
* Home Controller
*/
class ODK extends App
{
	protected $model;
    protected $userModel;
    protected $formModel;
    protected $surveyModel;

    protected $skipFields = array('_URI', '_CREATOR_URI_USER', '_CREATION_DATE', '_LAST_UPDATE_URI_USER',
                                    '_LAST_UPDATE_DATE', '_MODEL_VERSION', '_UI_VERSION', '_IS_COMPLETE', 
                                    '_SUBMISSION_DATE', '_MARKED_AS_COMPLETE_DATE', 'GEN__SURVEYOR', 'ID__PLOT_NO',
                                    'BEAT', 'CIRCLE', 'DEVICEID', 'ID__COMPARTMENT', 'END', 'GEN__PHONE',
                                    'RANGE', 'PHONENUMBER', 'SURVEYDATE', 'DIVISION', 'META_INSTANCE_ID',
                                    'GEN__STATE', 'SIMSERIAL', 'DISTRICT', 'TODAY', 'START', 'SECTION', 'FG__COUNT', 
                                    '_ORDINAL_NUMBER', '_PARENT_AURI', '_TOP_LEVEL_AURI', 'FLAG_LABEL', 'attr',
                                    'TEMP8', 'H0__TEMP1', 'S1__TEMP11', 'S1__TEMP12', 'S1__TEMP13', 'SD0__TEMP3', 
                                    'SD4__TEMP4', 'SD4__TEMP5', 'SD4__TEMP6', 'SD4__TEMP7', 'SP0__TEMP2', 'CORNERS4_COUNT',
                                    'SECA', 'SECA_2');
    
    protected $regionFields = array('ID__PLOT_NO', 'ID__GRID_NO', 'BEAT', 'CIRCLE', 'ID__COMPARTMENT', 'RANGE', 
                                    'VILLAGE_YD', 'DIVISION', 'GEN__STATE', 'DISTRICT', 'SECTION');

    protected $mappings;

	public function __construct()
	{
		parent::__construct();
		$this->model = new ODKModel($this->dbHandler);
        $this->userModel = new UserModel($this->dbHandler);
        $this->formModel = new FormModel($this->dbHandler);
        $this->surveyModel = new SurveyModel($this->dbHandler);

        $this->mappings = json_decode('{"attr":{"67":{"3":"273","5":"275","2":"272","4":"274","1":"271"},"20":{"2":"52","5":"55","1":"51","3":"53"},"28":{"3":"101","2":"100","1":"99","4":"102"},"31":{"3":"91","4":"92","2":"90","1":"89"},"19":{"3":"37","4":"38","2":"36","8":"42","1":"35"},"18":{"1":"29","2":"30"},"23":{"2":"116","1":"115","3":"117"},"30":{"2":"94","1":"93","3":"95"},"29":{"2":"97","3":"98","1":"96"},"33":{"2":"74","1":"73","3":"75","4":"76"},"24":{"1":"113","2":"114"},"37":{"3":"58","6":"61","1":"56","2":"57","5":"60","7":"62","4":"59"},"34":{"2":"70","4":"72","3":"71","1":"69"},"35":{"1":"66","2":"67","3":"68"},"36":{"2":"64","1":"63","3":"65"},"25":{"1":"111","2":"112"},"26":{"2":"110","1":"109"},"27":{"4":"107","3":"106","1":"104","2":"105","5":"108"},"22":{"grazing":"122","illegal_logging":"123","mining":"124","pathogenic_attack":"125","drought":"128","fire":"120","none":"137","development_projects":"119","invasive_species":"126","other":"136","firewood_extraction":"121","landslides":"129","heavy_snowfall":"135"},"21":{"e":"142","a":"138","d":"141","b":"139","c":"140","birds":"139","reptiles":"140","mammals":"138"},"32":{"top_drying":"86","insect_attack":"82","none":"88","others":"87","damage_by_fire":"77","damage_by_wildlife":"79","fungal_infestation":"80","damage_by_natural_calamities":"78","leaf_defoliator":"83"},"119":{"2":"331","1":"330","4":"333","3":"332"},"4":{"fruit":"375","seed":"380","bark":"373","leaves":"377","stem":"381","flower":"374","plant_whole":"378","inflorescence":"376","root":"379"},"121":{"Bamboo":"384","sapling":"386","herb":"387","seedling":"388","shrub":"385","Tree":"383"},"10":{"north_east":"389","north_west":"392","south_west":"391","sourth_east":"390"},"126":{"2":"394","1":"393"},"97":{"billudu_chloroxylon_swietenia":"340","chintha_tamarindus_indica":"341","kanugu_pongamia_pinnata":"342","kunkudu_sapindus_emarginatus":"343","narlinga_albizia_amara":"344","nemalinara_holoptelea_integrifolia":"345","relachettu_cassia_fistula":"346","seethaphal_annonas_quamosa":"347","amla_phyllanthus_emblica":"339","vepa_azadirachta_indica.":"348","other":"349","chilla_strychnos_potatorum":"350","teku_tectona_grandis":"351","neredu_syzygium_cumini":"352","moduga_butea_monosperma":"353","sima_casssia_siamea":"354","sarkar_tumma_prosopis_juliflora":"355","adaviankudu_wrightia_tinctoria":"337","irugudu_dalbergia_sissoo":"356","thati_borassus_flabellifer":"357","tellamaddi_terminalia_arjuna":"358","sandra_acacia_chundra":"359","bojja,_konda_tangedu_xylia_xylocarpa":"360","ippa_madhuca_latifolia":"361","jittegi_dalbergia_latifolia":"362","jammi_chettu_prosopis_cineraria":"363","kondapachari_dalbergia_paniculata":"364","peddamanu_ailanthus_excelsa":"365","potu_veduru_dendrocalamus_strictus":"366","kadise_cleistanthus_collinus":"367","bomma-medi_ficus_hispida":"368","tapasi,_kovila_firmiana_simplex":"369","nalla_acacia_nilotica":"370","tellatumma_acacia_leucocephala":"371","jeedi_semecarpus_anacardium":"372"},"128":{"1":"445","2":"446"},"39":{"own_farm":"146","purchase":"148","common_land":"147"}}}', true);
	}

	public function sync() {
        $params = array();
		$params['title'] = 'Sync ODK form data';
		$forms = $this->formModel->getAllForms(); //print_r($forms); exit;
		$params['forms'] = $forms;
        $this->view('odk-sync.html',compact('params'));
    }

    public function mapRegions() {

        $params['type'] = 'ODK';
        
        if(!$this->request) {
            $this->setFlash('ERROR! Please select form to sync');
			$this->redirect(array('ODK','sync'));
        }

        $params['region-fields'] = $this->regionFields;
        $formDetails = $this->formModel->getFormDetailsById($this->request['form']); //print_r($forms); exit;

        if($formDetails === false) {
            $this->setFlash('ERROR! Provided form is not avaiilable');
			$this->redirect(array('ODK','sync'));
        }

        $params['title'] = 'Map ODK Form Columns ('.$formDetails['desc'].')';

        $this->model->setCoreTable($formDetails['odk_table_master']);
        $formColumns = $this->model->getTableColumns(); //echo '<pre>'; print_r($formColumns); exit;

        $params['columns'] = $formColumns;
        $params['form_id'] = $formDetails['f_id'];

        $formData = $this->model->getCoreTable(); //echo '<pre>'; print_r($formData); exit;

        if($formData === false) {
            $this->setFlash('ERROR! No data available in provided form');
			$this->redirect(array('ODK','sync'));
        }

        $currentValues = array();
        foreach($formData as $fd) {
            foreach($fd as $key => $value) {
                if(!in_array($key, $this->regionFields) || empty($value)) continue;

                $currentValues[$key][] = $value;
            }
        }

        $currentValues = array_map('array_unique', $currentValues); //echo "<pre>"; print_r($currentValues); exit;

        $params['odk_values'] = $currentValues;

        $this->view('odk-map-regions.html',compact('params'));
    }

    public function map() {

        $params['type'] = 'ODK';
        
        if(!$this->request) {
            $this->setFlash('ERROR! Please select form to sync');
			$this->redirect(array('ODK','sync'));
        }

        $params['skip-fields'] = $this->skipFields;
        $formDetails = $this->formModel->getFormDetailsById($this->request['form']); //print_r($forms); exit;

        if($formDetails === false) {
            $this->setFlash('ERROR! Provided form is not avaiilable');
			$this->redirect(array('ODK','sync'));
        }

        $params['title'] = 'Map ODK Form Columns ('.$formDetails['desc'].')';

        $attributes = $this->formModel->getFormAttributes($formDetails['f_id']); //echo "<pre>"; print_r($attributes); exit;

        if($attributes === false) {
            $this->setFlash('ERROR! No attributes available for selected form');
			$this->redirect(array('ODK','sync'));
        }

        $groupAttributes = $groups = array();
        foreach($attributes as $attr) {

            if(empty($attr['group_id'])) continue;

            $groupId = $attr['group_id'];
            $groupAttributes[$groupId][] = $attr;
            $groups[$groupId] = $attr;
        }

        $params['attributes'] = $attributes;
        
        $this->model->setCoreTable($formDetails['odk_table_master']);
        $formColumns = $this->model->getTableColumns(); //print_r($formColumns); exit;

        $params['columns'] = $formColumns;
        $params['form_id'] = $formDetails['f_id'];
        $params['groups'] = $groups;
        $params['group_attributes'] = $groupAttributes;

        if(sizeof($groups) > 0) {
            foreach($groups as $groupId => $details) {
                $this->model->setCoreTable($details['odk_table_rep']);
                $groupColumns = $this->model->getTableColumns();

                $params['group_'.$groupId] = $groupColumns;
            }
        }

        $params['form_field'] = 'odk_field_master';
        $params['group_field'] = 'odk_field_rep';

        $this->view('odk-map.html',compact('params'));
    }

    public function mapOptions() {
        
        if(!$this->request) {
            $this->setFlash('ERROR! Please select form to sync');
			$this->redirect(array('ODK','sync'));
        }

        $formDetails = $this->formModel->getFormDetailsById($this->request['form']); //print_r($forms); exit;

        if($formDetails === false) {
            $this->setFlash('ERROR! Provided form is not avaiilable');
			$this->redirect(array('ODK','sync'));
        }

        $params['form_id'] = $formId = $formDetails['f_id'];
        $params['title'] = 'Map Select Field Options ('.$formDetails['desc'].')';

        $attributes = $this->formModel->getFormAttributeOptions($formDetails['f_id'], array('select')); //echo "<pre>"; print_r($attributes); exit;

        if($attributes === false) {
            $this->setFlash('ERROR! No attributes available for selected form');
			$this->redirect(array('ODK','sync'));
        }

        $attributeOptions = $attributeDetails = $odkFields = $groupFields = array();
        foreach($attributes as $attr) {
            $attrId = $attr['attr_id'];
            $odkName = !empty($attr['group_id']) ? $attr['odk_field_rep'] : $attr['odk_field_master'];
            $groupTable = $attr['odk_table_rep'];
            $attributeOptions[$odkName][] = $attr;
            $odkFields[$odkName] = $attrId;
            $attributeDetails[$odkName] = $attr;

            if(!empty($attr['group_id'])) {
                $groupFields[$groupTable][$odkName][] = $attr;
            }
        } //echo "<pre>"; print_r($attributeOptions); exit;

        $params['attributes'] = $attributeDetails;
        $params['values'] = $attributeOptions;
        
        $this->model->setCoreTable($formDetails['odk_table_master']);
        $formData = $this->model->getCoreTable(); //print_r($formColumns); exit;

        if($formData === false) {
            $this->setFlash('ERROR! No data available in provided form');
			$this->redirect(array('ODK','sync'));
        }

        $currentValues = array();
        foreach($formData as $fd) {
            foreach($fd as $key => $value) {
                if(!in_array($key, array_keys($odkFields)) || empty($value)) continue;

                $currentValues[$key][] = $value;
            }
        } //echo "<pre>"; print_r($currentValues); exit;

        if(sizeof($groupFields) > 0) {

            foreach($groupFields as $groupTable => $fields) {

                $this->model->setCoreTable($groupTable);
                $gData = $this->model->getCoreTable(); //echo "<pre>"; print_r($gData); exit;

                foreach($gData as $fd) {
                    foreach($fd as $key => $value) {
                        if(!in_array($key, array_keys($fields)) || empty($value)) continue;
        
                        $currentValues[$key][] = $value;
                    }
                }
            }
        }

        $currentValues = array_map('array_unique', $currentValues); //echo "<pre>"; print_r($currentValues); exit;

        $params['odk_values'] = $currentValues;

        $attributes = $this->formModel->getFormAttributeOptions($formDetails['f_id'], array('checkbox')); //echo "<pre>"; print_r($attributes); exit;

        if($attributes === false) {
            $this->setFlash('ERROR! No attributes available for selected form');
			$this->redirect(array('ODK','sync'));
        }

        $odkTables = array();
        $attributeOptions = $attributeDetails = $odkFields = array();
        foreach($attributes as $attr) {
            $attrId = $attr['attr_id'];
            $odkName = !empty($attr['group_id']) ? $attr['odk_field_rep'] : $attr['odk_field_master'];
            $attributeOptions[$odkName][] = $attr;
            $odkFields[$odkName] = $attrId;
            $attributeDetails[$odkName] = $attr;

            $odkTables[$odkName] = $attrId;
        } //echo "<pre>"; print_r($odkTables); exit;

        $params['cb_attributes'] = $attributeDetails;
        $params['cb_values'] = $attributeOptions;

        $checkboxValues = array();
        if(sizeof($odkTables) > 0) {
            foreach($odkTables as $cbTable => $attrId) {
                $this->model->setCoreTable($cbTable);
                $cbData = $this->model->getCoreTable(); //print_r($cbData); exit;

                if($cbData !== false) {
                    foreach($cbData as $cb) {
                        foreach($cb as $key => $value) {
                            if(in_array($key, array('VALUE', 'FLAG_SEL')) && !empty($value)) {
                                $checkboxValues[$cbTable][] = $value;
                            }
                        }
                    }
                }
            }
        }

        $checkboxValues = array_map('array_unique', $checkboxValues); //echo "<pre>"; print_r($currentValues); exit;

        $params['odk_cb_values'] = $checkboxValues;

        $params['selected_values'] = (isset($this->mappings['attr'])) ? $this->mappings['attr'] : array();
        //echo "<pre>"; print_r($params['selected_values']); exit;

        //echo json_encode($_SESSION['FORM_OPTIONS']); exit;

        $this->view('odk-map-options.html',compact('params'));
    }

    public function mapFields() {
        if($this->request) {
            $response = $this->formModel->setFormODKAttributes($this->request);
            extract($response);
			if($status)
			{
				$this->setFlash($msg);
				$this->redirect(array('ODK','data?form='.$this->request['form']));
			}else{
				$this->setFlash($msg);
				$this->redirect(array('ODK','map?form='.$this->request['form']));
			}
        } else {
            $this->redirect(array('ODK', 'sync'));
        }
    }

    public function mapFieldOptions() {
        if($this->request) {
            if(!isset($this->request['form'])) {
                $this->setFlash('ERROR! Please select form to map options');
                $this->redirect(array('ODK','sync'));
            }

            $formId = $this->request['form'];
            echo json_encode($this->request['attr']); exit;

            $this->redirect(array('ODK','mapOptions?form='.$this->request['form']));
        } else {
            $this->redirect(array('ODK', 'sync'));
        }
    }

    public function data() {
        //print_r($this->request); exit;
        if(!$this->request) {
            $this->setFlash('ERROR! Please select form to sync');
			$this->redirect(array('ODK','sync'));
        }

        $params = array();
        $formDetails = $this->formModel->getFormDetailsById($this->request['form']); //print_r($forms); exit;
        $params['form_id'] = $formId = $formDetails['f_id'];
        $params['title'] = 'ODK Form Data ('.$formDetails['desc'].')';
        
        $this->model->setCoreTable($formDetails['odk_table_master']);
        $formData = $this->model->getCoreTable(); //print_r($formData); exit;

        $params['data'] = $formData;

        $this->view('odk-data.html',compact('params'));
    }
    
    public function syncFormData() {

        // echo "<pre>"; print_r($formOptions); exit;

        if(!$this->request) {
            $this->setFlash('ERROR! Please select form to sync');
			$this->redirect(array('ODK','sync'));
        }

        $formId = $this->request['form'];

        // echo "<pre>"; print_r($formOptions[$formId]); exit;

        $designations = array();
        if(isset($this->mappings['attr']['67'])) {
            $desgValues = $this->formModel->getAllValues($this->mappings['attr']['67']); //print_r($desgValues); exit;

            if($desgValues !== false) {
                foreach($desgValues as $desg) {
                    $id = $desg['id'];
                    $designations[$id] = $desg['values'];
                }
            }
        } // echo "<pre>"; print_r($designations); exit;

        $allRanges = $this->surveyModel->getRanges();

        $rangeArray = array();
        if($allRanges !== false) {
            foreach($allRanges as $range) {
                $name = strtolower($range['name']);
                $rangeArray[$name] = $range['id'];
            }
        } // echo "<pre>"; print_r($rangeArray); exit;

        $allBeats = $this->surveyModel->getBeatsByRange();

        $beatArray = array();
        if($allBeats !== false) {
            foreach($allBeats as $beat) {
                $name = str_replace(array('(m)', ' '), array('', '_'), strtolower($beat['name']));
                $beatArray[$name] = $beat['id'];
            }
        } // echo "<pre>"; print_r($beatArray); exit;

        $allCompartments = $this->surveyModel->getCompartmentsByRange();

        $compartmentArray = array();
        if($allCompartments !== false) {
            foreach($allCompartments as $comp) {
                $name = strtolower($comp['name']);
                $compartmentArray[$name] = $comp['id'];
            }
        }  //echo "<pre>"; print_r($compartmentArray); exit;

        $allPlots = $this->surveyModel->getPlotsByRange();

        $plotArray = array();
        if($allPlots !== false) {
            foreach($allPlots as $plot) {
                $name = strtolower($plot['pname']);
                $compId = $plot['comp_id'];
                $plotArray[$compId][$name] = $plot['id'];
            }
        }  //echo "<pre>"; print_r($plotArray); exit;

        $odkSurvey = $this->surveyModel->getAllSurveyByFormId($formId);

        $savedForms = array();
        if($odkSurvey !== false) {
            foreach($odkSurvey as $survey) {
                $uri = $survey['odk_uri_id'];
                $savedForms[$uri] = $survey['id'];
            }
        } // echo "<pre>"; print_r($savedForms); exit;

        $formDetails = $this->formModel->getFormDetailsById($this->request['form']); //print_r($forms); exit;
        
        $this->model->setCoreTable($formDetails['odk_table_master']);
        $formData = $this->model->getCoreTable(); //print_r($formData); exit;

        if($formData === false) {
            $this->setFlash('ERROR! No data available in selected form');
			$this->redirect(array('ODK','sync'));
        }

        $userData = $this->userModel->getAllUsers();

        $formUsers = array();
        if($userData !== false) {
            foreach($userData as $user) {
                $userId = $user['user_id'];
                $formUsers[$userId] = $user['name'];
            }
        }

        // echo "<pre>"; print_r(array_unique($formUsers)); exit;

        $attributes = $this->formModel->getFormAttributes($formId); //echo "<pre>"; print_r($attributes); exit;

        if($attributes === false) {
            $this->setFlash('ERROR! No attributes available for selected form');
			$this->redirect(array('ODK','sync'));
        }

        $odkAttributes = $groups = $checkBoxes = array();
        foreach($attributes as $attr) {

            $odkField = !empty($attr['group_id']) ? $attr['odk_field_rep'] : $attr['odk_field_master'];
            $odkAttributes[$odkField] = $attr;

            if($attr['ui_element'] == 'checkbox') {
                $checkBoxes[$odkField] = $attr;
            }

            if(!empty($attr['group_id'])) {
                $groupId = $attr['group_id'];
                $groups[$groupId] = $attr;
            }
        }

        // echo "<pre>"; print_r($checkBoxes); exit;

        $odkGroups = array();
        if(sizeof($groups) > 0) {
            foreach($groups as $group) {
                $groupId = $group['group_id'];
                $this->model->setCoreTable($group['odk_table_rep']);
                $groupData = $this->model->getCoreTable(); //print_r($groupData); exit;

                if($groupData !== false) {
                    foreach($groupData as $gd) {
                        $uri = $gd['_TOP_LEVEL_AURI'];
                        $odkGroups[$groupId][$uri][] = $gd;
                    }
                }
            }
        }

        // echo "<pre>"; print_r($odkGroups); exit;

        $odkCheckBoxes = array();
        if(sizeof($checkBoxes) > 0) {
            foreach($checkBoxes as $cb) {
                $faId = $cb['id'];
                $this->model->setCoreTable($cb['odk_field_master']);
                $cbData = $this->model->getCoreTable(); //print_r($cbData); exit;

                if($cbData !== false) {
                    foreach($cbData as $cd) {
                        $uri = $cd['_TOP_LEVEL_AURI'];
                        $cd['attr'] = $cb['attri_id'];
                        $odkCheckBoxes[$faId][$uri][] = $cd;
                    }
                }
            }
        }

        // echo "<pre>"; print_r($odkCheckBoxes); exit;
        //echo "<pre>";
        
        $surveyors = $errors = array();
        foreach($formData as $fd) { //print_r($fd);
            $surveyorName1 = $fd['GEN__SURVEYOR'] == 'Mahesh ravali teja sri' ? 'Tejasri, Ravali , Mahesh' : $fd['GEN__SURVEYOR'];
            $surveyorName = str_replace(array('.', ' ', ','), '_', strtolower($surveyorName1));

            $uri = $fd['_URI'];
            $createdDate = $fd['_CREATION_DATE'];

            if(!in_array($surveyorName, array_keys($formUsers))) {
                $odkDesg = $fd['GEN__DESIG'];

                $desgId = isset($this->mappings['attr']['67'][$odkDesg]) ? $this->mappings['attr']['67'][$odkDesg] : 0;
                $desgLabel = isset($designations[$desgId]) ? $designations[$desgId] : '';

                $userData = array(
                    'user_id' => $surveyorName,
                    'name' => $surveyorName1,
                    'designation' => $desgLabel,
                    'mob_number' => $fd['GEN__PHONE']
                ); //print_r($userData);

                $result = $this->userModel->setODKUser($userData);
                if($result !== false) {
                    $formUsers[$surveyorName] = $surveyorName1;
                }
            }

            $range = $fd['RANGE'];

            if(!isset($rangeArray[$range])) {
                $errors['range-error'] = $range." is not available in new structure";
                continue;
            }

            $rangeId = $rangeArray[$range];

            $beat = $fd['BEAT'];

            if(!isset($beatArray[$beat])) {
                $errors['beat-error'] = $beat." is not available in new structure";
                continue;
            }

            $beatId = $beatArray[$beat];

            $compartmentId = NULL;
            if(isset($fd['ID__COMPARTMENT'])) {

                $compartment = $fd['ID__COMPARTMENT'];

                if(!empty($compartment) && $compartment != 0) {
                    if(isset($compartmentArray[$compartment])) {
                        $compartmentId = $compartmentArray[$compartment];
                    }
                }
            }

            $plotId = NULL;
            if(isset($fd['ID__PLOT_NO'])) {
                
                $plot = $fd['ID__PLOT_NO'];

                if(!empty($plot) && $plot != 0) {
                
                    if(!isset($plotArray[$compartmentId][$plot])) {
                        
                        $plotData = array(
                            'c_id' => 1,
                            'd_id' => 1,
                            'r_id' => $rangeId,
                            'b_id' => $beatId,
                            'comp_id' => $compartmentId,
                            'pname' => $plot,
                            'start_date' => $createdDate,
                            'create_by' => $surveyorName
                        ); //print_r($plotData);
                        $plotId = $this->surveyModel->setPlot($plotData);

                        if($plotId !== false) {
                            $plotArray[$compartmentId][$plot] = $plotId;
                        } else {
                            $errors['plot-error'][] = "Not able to add plot: ".$plot;
                            continue;
                        }

                    } else {
                        $plotId = $plotArray[$compartmentId][$plot];
                    }
                }
            }

            if(!in_array($uri, array_keys($savedForms))) {

                $surveyData = array(
                    'f_id' => $formId,
                    'device_id' => $fd['DEVICEID'],
                    'odk_uri_id' => $uri,
                    'create_date' => $createdDate,
                    'create_by' => $surveyorName,
                    'b_id' => $beatId,
                    'comp_id' => $compartmentId,
                    'plot_id' => $plotId
                ); //print_r($surveyData);
                $surveyId = $this->surveyModel->setSurveyODKForm($surveyData); //exit;

                if($surveyId !== false) {
                    //echo "<pre>";

                    foreach($fd as $key => $value) {
                        if(in_array($key, $this->skipFields) || empty($value)) continue;

                        if(!isset($odkAttributes[$key])) die('error: '.$key);

                        $attr = $odkAttributes[$key];

                        if($attr['ui_element'] != 'checkbox') {
                            $dataArray = array(
                                'fa_id' => $attr['id'],
                                's_id' => $surveyId,
                                'g_id' => $attr['group_id'],
                                'seq_id' => 0
                            );

                            if($attr['ui_element'] == 'select') {
                                $dataArray['value'] = NULL;
                                $dataArray['p_id'] = $this->mappings['attr'][$attr['attri_id']][$value];
                            } else {
                                $dataArray['p_id'] = NULL;
                                $dataArray['value'] = $value;
                            } //print_r($dataArray); //exit;
                            $this->surveyModel->setFormData($dataArray); //exit;
                        }
                    }

                    if(sizeof($odkCheckBoxes) > 0) {
                        foreach($odkCheckBoxes as $faId => $cbData) {
                            //echo $faId;
                            if(isset($cbData[$uri])) {
                                $i = 1;
                                $groupSeq = array();
                                foreach($cbData[$uri] as $gd) {
                                    $gUri = $gd['_URI'];

                                    $groupSeq[$gUri] = $i;
                                    foreach($gd as $gKey => $gValue) {
                                        if(in_array($gKey, $this->skipFields) || empty($gValue)) continue;

                                        $dataArray = array(
                                            'fa_id' => $faId,
                                            's_id' => $surveyId,
                                            'seq_id' => $i,
                                            'g_id' => NULL
                                        ); //print_r($dataArray);

                                        if(isset($this->mappings['attr'][$gd['attr']][$gValue])) {
                                            $dataArray['value'] = NULL;
                                            $dataArray['p_id'] = $this->mappings['attr'][$gd['attr']][$gValue];
                                        } else {
                                            $dataArray['value'] = $gValue;
                                            $dataArray['p_id'] = 0;
                                        } //print_r($dataArray);
                                        $this->surveyModel->setFormData($dataArray);
                                    }
                                    $i++;
                                }
                            }
                        }
                    }

                    if(sizeof($odkGroups) > 0) {
                        foreach($odkGroups as $groupId => $groupData) {
                            if(isset($groupData[$uri])) {
                                $i = 1;
                                foreach($groupData[$uri] as $gd) {

                                    $gUri = $gd['_URI'];
                                    $seq = (isset($groupSeq[$gUri])) ? $groupSeq[$gUri] : $i;

                                    foreach($gd as $gKey => $gValue) {

                                        if(in_array($gKey, $this->skipFields) || empty($gValue) || $gValue == -1) continue;

                                        if(!isset($odkAttributes[$gKey])) die('error: '.$gKey);

                                        $attr = $odkAttributes[$gKey];

                                        if($attr['ui_element'] != 'checkbox') {
                                            $dataArray = array(
                                                'fa_id' => $attr['id'],
                                                's_id' => $surveyId,
                                                'g_id' => $attr['group_id'],
                                                'seq_id' => $seq
                                            );

                                            if($attr['ui_element'] == 'select') {
                                                $dataArray['value'] = NULL;
                                                $dataArray['p_id'] = $this->mappings['attr'][$attr['attri_id']][$gValue];
                                            } else {
                                                $dataArray['p_id'] = NULL;
                                                $dataArray['value'] = $gValue;
                                            } //print_r($dataArray);
                                            $this->surveyModel->setFormData($dataArray);
                                        }
                                    }
                                    $i++;
                                }
                            }
                        }
                    }
                }
            }
            //exit;
        }  //echo "<pre>"; print_r(array_unique($surveyors)); exit;

        echo "<pre>"; print_r($errors); exit;
	}
}