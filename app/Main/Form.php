<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\FormModel;
use IFMT\App\Model\SurveyModel;
/**
* Home Controller
*/
class Form extends App
{
	protected $model;
    protected $userModel;
    protected $surveyModel;

	public function __construct()
	{
		parent::__construct();
		$this->model = new FormModel($this->dbHandler);
        $this->userModel = new UserModel($this->dbHandler);
        $this->surveyModel = new SurveyModel($this->dbHandler);
	}
}