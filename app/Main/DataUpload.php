<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\DataUploadModel;
use IFMT\App\Model\UtilityModel;
/**
* Home Controller
*/
class DataUpload extends App
{
	protected $utilityModel;
	protected $uploadModel;
	
	public function __construct()
	{
		parent::__construct();
		$this->utilityModel = new UtilityModel();
		$this->uploadModel = new DataUploadModel();
	}
	
	public function ForestAdminBoundaries(){
		$params = array();
		$params['title'] = 'Upload Forest Admin Boundaries';
		$forestAdminHierarchy = $this->utilityModel->forestAdminHierarchy();
		if($forestAdminHierarchy === false)
		{
			$this->setFlash(["State code is not set. With this setting you can't proceed to set admin boundaries.Please contant your <u>web administrator</u> for help.",'info']);
			$this->redirect(array('Home','index'));
		}
		foreach($forestAdminHierarchy as $index=>$elem){
			$response = $this->uploadModel->checkForestAdminBoundaries($elem['table_name'], $_SESSION['user']['user_id']);
			if(!isset($response['error']) && sizeof($response)>0){
				$forestAdminHierarchy[$index]['uploadedStatus'] = 1;
				$regionNames = '';
				foreach($response as $row){
					 $regionNames.= $row['name'].", ";
				}
				$forestAdminHierarchy[$index]['regionName'] = rtrim($regionNames,", ");
			} else{
				$forestAdminHierarchy[$index]['uploadedStatus'] = 0;
			}
		}
		$params['forestAdminHierarchy'] = $forestAdminHierarchy;
		return $this->view('forest-admin-boundries.html',compact('params'));
	}
	
	public function postForestAdminBoundaries(){
		if(isset($_FILES['zip_file']['name']) && isset($this->request['h_id'])){
			$file_name = $_FILES['zip_file']['name'];
			$file_name = str_replace(' ','_', $file_name);
			if($file_name)
			{
				$currentTime = time();
				$dirPath = "files/temp/zip_upload/_".$currentTime;
				mkdir($dirPath, 0777, true);
				$path= $dirPath.'/'.$file_name;
				if(copy($_FILES['zip_file']['tmp_name'], $path)){
					$shapeFilePath = $this->extractZip($path, "shp", $currentTime);
					if($shapeFilePath){
						$tableName = $this->utilityModel->getTableName($this->request['h_id']);
						$response = $this->uploadModel->importShapeFileToPG($_SERVER['DOCUMENT_ROOT']."/".glob($shapeFilePath."/*.shp")[0], $tableName[0]['table_name']);
						if(isset($response['error'])){
							$this->setFlash(["Error in uploading. Please verify the format is correct.",'danger']);
							$this->redirect(array('DataUpload','ForestAdminBoundaries'));
						} else{
							if($tableName[0]['table_name'] != "division_master"){
								$parentOrder = (int)$this->request['h_id'] - 1;
								$result = $this->uploadModel->setParentByIntersect($response, $tableName[0]['table_name'], $parentOrder);
								if(isset($result['error'])){
									$ids = array();
									foreach($response as $index=>$row){
										$ids[$index] = $row['id'];
									}
									$result = $this->uploadModel->rollbackBoundariesDataUpload($ids, $tableName[0]['table_name']);
									$this->setFlash(["Error: Failed to link boundaries with parents. (Hint: Projection must be EPSG:4326)",'danger']);
									$this->redirect(array('DataUpload','ForestAdminBoundaries'));
								} else{
									$this->setFlash("Success: Boundaries uploaded successfully.");
									$this->redirect(array('DataUpload','ForestAdminBoundaries'));
								}
							}
							$this->setFlash("Success: Boundaries uploaded successfully.");
							$this->redirect(array('DataUpload','ForestAdminBoundaries'));
						}
					} else{
						$this->setFlash(["Error in uploading. Please verify the format is correct.",'danger']);
						$this->redirect(array('DataUpload','ForestAdminBoundaries'));
					}
				} else{
					$this->setFlash(["Error in uploading. Please verify the format is correct.",'danger']);
					$this->redirect(array('DataUpload','ForestAdminBoundaries'));
				}
			}
		} else{
			$this->setFlash(["Missing parameter(s)",'danger']);
			$this->redirect(array('DataUpload','ForestAdminBoundaries'));
		}
	}
	
	public function speciesData(){
		$params = array();
		$params['title'] = "Upload Species' Data";
		$forestAdminHierarchy = $this->utilityModel->forestAdminHierarchy("1");
		$params['topMostParentList'] = $this->utilityModel->getParentOptions($forestAdminHierarchy[0]['id']);
		$params['topMostParentName'] = $forestAdminHierarchy[0]['name'];
		$params['allHabits'] = $this->uploadModel->getAllHabits();
		$params['speciesChck'] = $this->uploadModel->getSpeciesListExists();
		return $this->view('species-data.html',compact('params'));
	}
	
	public function postSpeciesData(){
		if(isset($_FILES['csv_file']['tmp_name']) && isset($this->request['topMostparent'])){ //sop - second order parent
			$csvPath = $_FILES['csv_file']['tmp_name'];
			$ext = pathinfo($_FILES['csv_file']['name'], PATHINFO_EXTENSION);
			if($ext == "csv"){
				$csvData = $this->readCSV($csvPath);
				if(sizeof($csvData)>1){
					if(strtolower($csvData[0][0]) == 'habit type' && strtolower($csvData[0][1]) == 'species id' && strtolower($csvData[0][2]) == 'local name' && strtolower($csvData[0][3]) == 'scientific name'){
						array_shift($csvData);
						$habits = array();
						$i=0;
						foreach ($csvData as $row) {
							if(sizeof($row)==4){
								$habits[] = $row[0];
							} else{
								unset($csvData[$i]);
							}
							$i++;
						}
						$uniqueHabit = array_unique($habits);
						$habitMsg = array();
						foreach($uniqueHabit as $habit){
							$result = $this->uploadModel->getHabitType("name", $habit, true);
							if(isset($result['error'])){
								$this->setFlash(["Error: Habit type is incorrect.",'danger']);
								$this->redirect(array('DataUpload','speciesData'));
							}
							if(!isset($result[0])){
								$habitMsg[]= $habit;
								unset($csvData[$i]);
							} else{
								$habitValId[$habit] = $result[0]['id'];
								foreach($csvData as $key => $val){
									if(strtolower($val[0]) == $habit){
										$csvData[$key][0] = $result[0]['id'];
									}
								}
							}
							
						}
						$info = (sizeof($habitMsg)>0 ? implode(", ", $habitMsg).' are not supported habit type. Supported ' : '');
						$result = $this->uploadModel->loadSpeciesList($csvData, $this->request['topMostparent']);
						if(sizeof($result)>1){
                        	header('Content-Disposition: attachment; filename="species-not-uploaded.txt"');
							header('Content-Type: text/plain; charset=utf-8');
							header('Content-Length: ' . strlen(implode("\n", $result)));
                        	header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
      						header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
                        	header("Pragma: no-cache");
                        	flush();
                        	ob_start();
							echo implode("\n", $result);
							$this->setFlash(["Notice: All species data are uploaded except those listed in a file which is being downloaded.",'Warning']);
							$this->redirect(array('DataUpload','speciesData'));
						} else{
							$this->setFlash(["Species data upload successfull.",'success']);
							$this->redirect(array('DataUpload','speciesData'));
						}
					} else{
						$this->setFlash(["Error: Invalid file structure. Please check column names and thier order.",'danger']);
						$this->redirect(array('DataUpload','speciesData'));
					}
				} else{
					$this->setFlash(["Error: File is empty.",'danger']);
					$this->redirect(array('DataUpload','speciesData'));
				}
			} else{
				$this->setFlash(["Error: Incorrect file format. Please upload csv file.",'danger']);
				$this->redirect(array('DataUpload','speciesData'));
			}
			
		} else{
			$this->setFlash(["Error: File is missing",'danger']);
			$this->redirect(array('DataUpload','speciesData'));
		}
	}
	
	function getParentOptions(){
		echo json_encode($this->utilityModel->getParentOptions($this->request['parentID']));
	}
	
	public function getChildList(){
		echo json_encode($this->utilityModel->getChildList($this->request['id'], $this->request['parentVal']));
	}
	
	public function extractZip($file, $fileType, $currentTime){
		$zipArch = new \ZipArchive;
		//$currentTime = time();
		$childDir = ($fileType == 'shp'?'shapeFiles' : 'rasterFiles');
		try{
			mkdir("files/".$childDir."/".$currentTime, 0777, true);
			$destPath = "files/".$childDir."/_".$currentTime;
			$res = $zipArch->open($file);
			if ($res === TRUE) {
				$zipArch->extractTo($destPath);
				$zipArch->close();
				return $destPath;
			} else {
				return false;
			}
		} catch(Exception $e){
			$errormsg = $e->getMessage();
			return false;
		}
	}
	
	public function readCSV($filePath){
		$handler = fopen($filePath, "r");
		if($handler !== FALSE) {
			$data = array();
			while(!feof($handler)) {
				$data[] = fgetcsv($handler, 1000000, ",");
			}
		}
		fclose($handler);
		return $data;
	}
}