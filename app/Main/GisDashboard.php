<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\DataVerificationModel;
use IFMT\App\Model\GisDashboardModel;
use IFMT\App\Model\UtilityModel;
/**
* Home Controller
*/
class GisDashboard extends App
{
	private $utilityModel;
	private $verificationModel;
	private $gisDashboardModel;
	
	public function __construct()
	{
		parent::__construct();
		$this->utilityModel = new UtilityModel();
		$this->verificationModel = new DataVerificationModel();
		$this->gisDashboardModel = new GisDashboardModel();
	}

	function getGisDashboard(){
		$params = array();
		$params['title'] = "GIS Dashboard";
		$params['dpurl'] = $_ENV['DPURL'];
		$url = isset($_SERVER['HTTPS']) && @$_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://'.$_SERVER['SERVER_NAME'];
		$create_by = $_SESSION['user']['user_id'];
		$forestAdminHierarchy = $this->utilityModel->forestAdminHierarchy();
		$params['verified'] = 0;
		$verificationStatus = $this->gisDashboardModel->boundaryVerificationStatus();
		if($verificationStatus[0]['status'] == 1){
			$params['verified'] = 1;
		}	
		$this->view('gis-dashboard.html',compact('params','url','create_by'));
	}

	function getLayerList(){
		echo json_encode($this->gisDashboardModel->getAllLayers());	
	}

	function addBoundary()
	{
		if(empty($this->request))
		{
			$uploadedPointData = $this->gisDashboardModel->getUploadedPointLayers();
			$uploadedRasterData = $this->gisDashboardModel->getUploadedRasterLayers();
			$uploadedVectorData = $this->gisDashboardModel->getUploadedVectorLayers();
			$tablePoint = $this->gisDashboardModel->getPointTable();
			$tableVector = $this->gisDashboardModel->getVectorTable();
			$tableRaster = $this->gisDashboardModel->getRasterTable();
			$baseurl = $_ENV['BASEURL'];
			if($uploadedPointData === false && $uploadedGisData === false)
			{
				$this->setFlash(array('Error in fetching uploaded data.',"danger"));	
			}
			$this->view('addBoundary.html',compact('uploadedPointData','uploadedRasterData','tableRaster','tablePoint','uploadedVectorData','tableVector', 'baseurl'));
		}else{
			$mapType = $this->request['mapType'];
			if($mapType == 'point')
			{
				$this->processPointData($_FILES, $this->request);
			}elseif($mapType == 'vector'){
				$this->processVectorData($_FILES, $this->request);
			}elseif($mapType == 'raster'){
				$this->processRasterData($_FILES, $this->request);
			}
		}	
	}

	function processPointData($files, $request)
	{
		$destination_path = getcwd().DIRECTORY_SEPARATOR;
		$fileName = $destination_path."files/temp/tmp".uniqid().".csv";
		move_uploaded_file($files['uploadFile']['tmp_name'],$fileName);
		$fh = fopen($fileName, "r");
		$csvData = array();
		while (($row = fgetcsv($fh, 0, ",")) !== FALSE) {
		    $csvData[] = $row;
		}	
		unlink($fileName);
		$attrs = array(
			'user_id' => $this->session['user']['user_id'],
			'location_info' => json_encode($csvData),
			'nice_name' => $request['niceName'],
			'layer_name' => "",
			'geoserver_url' => ""
		);
		if($this->gisDashboardModel->addUserData($request['mapType'], $attrs))
		{
			$this->setFlash(array('Data added successfully.',"success"));	
		}else{
			$this->setFlash(array('Error occured. Please contact site adminsitrator.',"danger"));	
		}
		$this->redirect(array('GisDashboard','addBoundary'));
	}

	function getLocationData()
	{
		echo json_encode($this->gisDashboardModel->getLocationData($this->request['id']));
	}

	function setVerifyPointVectorLayerUpload()
	{
		echo json_encode($this->gisDashboardModel->updateVerifyPointVectorLayerUpload($this->request['table'],$this->request['id'], $this->request['val']));
	}

	function processVectorData($files, $request)
	{
		$destination_path = getcwd().DIRECTORY_SEPARATOR;
		$fileName = $destination_path."files/userVector/".$files['uploadFile']['name'];
		$pathForDb = "/files/userVector/".$files['uploadFile']['name'];
		move_uploaded_file($files['uploadFile']['tmp_name'],$fileName);
		$attrs = array(
			'user_id' => $this->session['user']['user_id'],
			'file_name' => $pathForDb,
			'nice_name' => $request['niceName'],
			'layer_name' => "",
			'geoserver_url' => ""
		);
		if($this->gisDashboardModel->addUserData($request['mapType'], $attrs))
		{
			$this->setFlash(array('Data added successfully.',"success"));	
		}else{
			$this->setFlash(array('Error occured. Please contact site adminsitrator.',"danger"));	
		}
		$this->redirect(array('GisDashboard','addBoundary'));
	}

	function processRasterData($files, $request)
	{
		$destination_path = getcwd().DIRECTORY_SEPARATOR;
		$fileName = $destination_path."files/userRaster/".$files['uploadFile']['name'];
		move_uploaded_file($files['uploadFile']['tmp_name'],$fileName);
		$newName = str_replace(".tif",uniqid().".tif", $fileName);
		rename($fileName, $newName);
		$lyrName = str_replace("/var/www/html/ifmt/public/files/userRaster/","",$newName);
		$lyrName = str_replace(".tif","",$lyrName);
		$lyrName = str_replace("-","_",$lyrName);
		$attrs = array(
			'user_id' => $this->session['user']['user_id'],
			'file_name' => $newName,
			'nice_name' => $request['niceName'],
			'layer_name' => $lyrName,
			'geoserver_url' => $_ENV['BASEURL']
		);
		if($this->gisDashboardModel->addUserData($request['mapType'], $attrs))
		{
			$this->setFlash(array('Data added successfully.',"success"));	
		}else{
			$this->setFlash(array('Error occured. Please contact site adminsitrator.',"danger"));	
		}
		$this->redirect(array('GisDashboard','addBoundary'));
	}
}