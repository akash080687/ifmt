<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\SurveyModel;
use IFMT\App\Model\RareSpeciesModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
/**
* Home Controller
*/
class DataView extends App
{
	private $rareSpeciesModel;
	private $sheet;
	private $writer;
	private $objDrawing;
	public function __construct()
	{
		parent::__construct();
		$spreadsheet = new Spreadsheet();
		$this->writer = IOFactory::createWriter($spreadsheet, "Xlsx");
		$this->sheet = $spreadsheet->getActiveSheet();
		$this->rareSpeciesModel = new RareSpeciesModel($this->dbHandler);
		$this->surveyModel = new SurveyModel($this->dbHandler);
		$this->userModel = new UserModel($this->dbHandler);
	}

	public function rareSpecies()
	{
		$ranges = $this->rareSpeciesModel->getRanges($this->session['user']['user_id']);
		return $this->view('dataView.html',compact('ranges'));
	}

	public function getRareData()
	{
		$data = $this->rareSpeciesModel->getData($_POST['rangeId']);
		if(!empty($data))
		{
			$html = "<thead>";
			foreach (array_keys($data[0]) as $value) {
				$html .= "<th>$value</th>";
			}
			$html .= "</thead><tbody>";
			foreach ($data as $value) {
				$html .= "<tr>";
				foreach ($value as $k=>$v) {
					if($k == 'image')
					{
						$v = str_replace($this->publicDir, '', $v);
						$html .= "<th><img src='$v' height='50px;' width='50px;'/></th>";	
					}else{
						$html .= "<th>$v</th>";
					}
				}
				$html .= "</tr>";
			}
			$html .= "</tbody>";
		}else{
			$html = "<thead><tr><td>Rare Species</td></tr></thead><tbody><tr><td>No Data Found.</td></tr></tbody>";
		}
		return $html;
	}

	public function getAllExcel()
	{
		if(file_exists($this->publicDir."/files/temp/rareData.xlsx"))
		{
			unlink($this->publicDir."/files/temp/rareData.xlsx");
		}

		if(isset($_POST['data']) && $_POST['data'] = 'All')
		{
			$data = $this->rareSpeciesModel->getData($_POST['rangeId'], true);
		}else{
			$data = $this->rareSpeciesModel->getData($_POST['rangeId']);
		}
		
		if(!empty($data))
		{
			$col = 0;
			foreach (array_keys($data[0]) as $value) {
				$this->sheet->setCellValue($this->num2alpha($col)."1", $value);
				$col++;
			}

			$row = 2;
			foreach ($data as $value) {
				$col = 0;
				foreach ($value as $k=>$v) {
					if($k == 'image')
					{
						// $v = str_replace($this->publicDir, '', $v);
						$gdImage = imagecreatefromjpeg($v);
						$objDrawing = new MemoryDrawing();
						$objDrawing->setName('image');
						$objDrawing->setDescription('image');
						$objDrawing->setImageResource($gdImage);
						$objDrawing->setRenderingFunction(MemoryDrawing::RENDERING_JPEG);
						$objDrawing->setMimeType(MemoryDrawing::MIMETYPE_DEFAULT);
						$objDrawing->setHeight(100);
						$cell = $this->num2alpha($col)."$row";
						$objDrawing->setCoordinates($cell);
						$objDrawing->setWorksheet($this->sheet);
					}else{
						$cell = $this->num2alpha($col).$row;
						$this->sheet->setCellValue($cell, $v);
					}
					$col++;
				}
				$row+=7;
			}
			$fileName = 'rareData'.time();
			$this->writer->save($this->publicDir."/files/temp/$fileName.xlsx");
			return json_encode(array('success'=>true, 'filename'=>$fileName));
		}
		return json_encode(array('success'=>false));
	}

	function num2alpha($n)
	{
	    for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
	        $r = chr($n%26 + 0x41) . $r;
	    return $r;
	}
}