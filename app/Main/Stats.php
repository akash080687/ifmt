<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\StatsModel;

/**
 * Stats Plugin
 */
class Stats extends App
{
	private $statsModel;
	function __construct()
	{
		parent::__construct();
		$this->statsModel = new StatsModel;
	}

	public function index()
	{
		$title = 'Statistics';
		#no. of div
		#no. of record in rare
		#no, of entries / divison 
		#no. survey 
		return $this->view('stats.html',compact('title'));
	}
}