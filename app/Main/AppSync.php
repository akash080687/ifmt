<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\FormModel;
use IFMT\App\Model\AppSyncModel;
use IFMT\App\Model\SurveyModel;
use IFMT\App\Model\UtilityModel;
use IFMT\App\Model\RareSpeciesModel;

/**
* AppSync Controller
*/
class AppSync extends App
{
	protected $model;
    protected $userModel;
    protected $formModel;
    protected $surveyModel;
    protected $utilityModel;
    protected $version = 2;

    protected $skipFields = array('id', 'state', 'date_created', 'formname', 'device_id',
                                    'district', 'block', 'circle', 'range', 'compartment', 'grid', 'form_id', 'division',
                                    'enumeration_id', 'approach_id', 'description_id', 'bamboonumber', 'beat', 
                                    'saplingnum', 'seedlingnum', 'treenum', 'herbnum', 'shrubnum', 
                                    'plot', 'plantation_number', 'plantation', 'ocularstockmanagement', 'species_info',
                                    'village_id', 'ntfp_count', 'household_id', 'livestock_number', 'human_wildlife_number',
                                    'cropdamage_number', 'user_id', 'designation', 'deleted', 'delete_date', 'property_number', 'attr',
                                    'version', 'formid', 'form_name', 'app_table', 'sync_date', 'synced', 'group_name', 'climbernum', 
                                    'grassnum', 'form_sync_date', 'SD4__TEMP6', 'SD4__TEMP7', 'SP0__TEMP2', 'CORNERS4_COUNT');

    protected $langMonths = array(1 => 'जनवरी', 'फरवरी', 'मार्च', 'अप्रैल', 'मई', 'जून', 'जुलाई', 'अगस्त', 'सितंबर', 'अक्टूबर', 'नवंबर', 'दिसंबर');

    protected $attrRef = array(282 => 79);

    protected $types = array(
        'village' => array(
            'table' => 'village_details', 
            'sub-table' => array('ntfp'), 
            'yes' => 1, 
            'no' => 2
        ),
        'description' => array(
            'table' => 'plot_description', 
            'sub-table' => array('plantation'), 
            'group' => array('id' => 3, 'fauna' => 20, 'species_name' => 324, 'description' => 93), 
            'habit' => array('amphibians' => 8, 'birds' => 9, 'mammals' => 10, 'reptiles' => 11, 'plants' => 7), 
            'yes' => 1, 
            'no' => 2
        ),
        'enumeration' => array(
            'table' => 'plot_enumeration', 
            'sub-table' => array('bamboo', 'sapling', 'seedling', 'tree', 'shrub', 'herb', 'climber', 'grass'), 
            'habit' => array('bamboo' => 6, 'sapling' => 3, 'seedling' => 4, 'tree' => 7, 'shrub' => 2, 'herb' => 1, 'climber' => 12, 'grass' => 13), 
            'yes' => 1, 
            'no' => 2
        ),
        'approach' => array(
            'table' => 'plot_approach', 
            'sub-table' => array('plant_species'), 
            'group' => array('id' => 5, 'fauna' => 58, 'species_name' => 326, 'description' => 113), 
            'habit' => array('amphibians' => 8, 'birds' => 9, 'mammals' => 10, 'reptiles' => 11),
            'habit-values' => array(330 => 7, 331 => 2, 332 => 6, 333 => 1, 334 => 13, 335 => 15, 336 => 12),
            'yes' => 1, 
            'no' => 2,
            'habit-field' => 'habit',
            'skip' => array('plants')
        ),
        'household' => array(
            'table' => 'household', 
            'sub-table' => array('crop_damage', 'human_damage', 'livestock_damage', 'property_damage', 'forest_lands', 'private_lands', 'livestock_hh'), 
            'yes' => 1, 
            'no' => 2
        )
    );

    protected $tableTypes = array();

    protected $faunaFields = array('amphibians' => 141, 'birds' => 139, 'mammals' => 138, 'plants' => 142, 'reptiles' => 140);

    protected $faunaCommentsFields;

    protected $mappings;
    protected $attrValues;

    protected $prefix = 'static_';

    public function __construct()
	{
        parent::__construct();

		$this->model = new AppSyncModel($this->dbHandler);
        $this->userModel = new UserModel($this->dbHandler);
        $this->formModel = new FormModel($this->dbHandler);
        $this->surveyModel = new SurveyModel($this->dbHandler);
        $this->utilityModel = new UtilityModel($this->dbHandler);
        $this->rareSpeciesModel = new RareSpeciesModel($this->dbHandler);
        if(sizeof($this->faunaFields) > 0) {
            $this->skipFields = array_merge($this->skipFields, array_keys($this->faunaFields));

            $this->faunaCommentsFields = array_map(function($value) { 
                return $value . '_comments';
            }, array_keys($this->faunaFields));

            $this->skipFields = array_merge($this->skipFields, $this->faunaCommentsFields);
        }

        foreach($this->types as $type => $typeData) {
            $tableName = $typeData['table'];
            $typeData['type'] = $type;
            $this->tableTypes[$tableName] = $typeData;
        }

        $attributes = $this->formModel->getAllAttributeValues();

        $attrValues = $speciesAttr = array();
        if($attributes !== false) { 
            foreach($attributes as $attr) {

                $attrId = $attr['attr_id'];
                if(strstr($attr['values'], 'species_master')) {
                    $speciesAttr[$attrId] = array(
                        'fa_id' => $attr['id'],
                        'values' => $attr['values']
                    );
                    continue;
                }

                if(in_array($attr['attr_id'], array(6, 143))) continue;
                $value = strtolower($attr['values']);
                $langValue = $attr['lang_values'];
                $attrValues[$attrId][$value] = $attr['id'];
                $attrValues[$attrId][$langValue] = $attr['id'];

                $this->attrValues[$attrId][$attr['id']] = $attr['values'];
            }
        }

        $this->mappings['attr'] = $attrValues;
        $this->mappings['species'] = $speciesAttr;
    }
    
    public function syncForm(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Cache-Control,Content-Language,Expires,Last-Modified,Pragma,Content-Type,X-Requested-With,Accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers");
        header("Access-Control-Allow-Credentials: false");
        header("Access-Control-Allow-Methods: GET,POST,HEAD,OPTIONS,PUT");

		header("Content-type: application/json");
        
        $json = file_get_contents('php://input');
        
        error_log($json);
        
        $returnNewData = array();
        
        $postData = json_decode($json);

        if(!isset($postData->main)) {
            $response = array(
                'code' => '-1',
                'text' => 'Error! invalid request parameter'
            );
            die(json_encode($response));
        }
        
        $syncData = $postData->main;
        
        if(!isset($syncData->device_id) || empty($syncData->device_id)) {
            $response = array(
                'code' => '-1',
                'text' => 'Error! required parameter is missing or empty: device_id'
            );
            die(json_encode($response));
        }
        
        if(isset($syncData->type)) {
            
            if($postData->main->type == 'rare')
            {
                $error = $this->rareSpeciesModel->syncData($postData->main);
                $error = ($error) ? 0 : 1;
                $returnNewData['action'] = 'rare_insert';
                $returnNewData['code'] = "1";
                $returnNewData['message'] = "Successfully synced.";
            }else{
                $type = $syncData->type;
                
                if(!isset($this->types[$type])) {
                    $response = array(
                        'code' => '-1',
                        'text' => 'Error! Provided type is not configured: '.$type
                    );
                    die(json_encode($response));
                }

                $tableName = $this->types[$type]['table'];
                
                $id = $syncData->form_id = $syncData->id;

                if($syncData->type == 'household' && isset($syncData->typeoflivestock)) unset($syncData->typeoflivestock);

                unset($syncData->id);
                unset($syncData->type);
                
                $timestamp = strtotime($syncData->date_created);
                $syncData->date_created = date('Y-m-d H:i:s', $timestamp);
                $syncData->form_sync_date = date('Y-m-d H:i:s');

                $this->model->begin();
                
                $result = $this->model->insertSyncRecord($tableName, (array) $syncData, 'id');

                $error = 0;
                if($result !== false){
                    $returnNewData['action'] = 'insert';
                    $returnNewData['code'] = "1";
                    
                    $fKeyId = $result['id'];
                    
                    if(isset($this->types[$type]['sub-table'])) {
                        
                        foreach($this->types[$type]['sub-table'] as $tableName) {
                            
                            if(isset($postData->$tableName) && sizeof($postData->$tableName) > 0) {
                        
                                $subArray = array(
                                    $type.'_id' => $fKeyId
                                );

                                foreach($postData->$tableName as $sub) {

                                    if(in_array($tableName, array('forest_lands', 'private_lands'))) {
                                        $subArray['collect_name'] = $sub->name;
                                        unset($sub->name);
                                    }
        
                                    if(in_array($tableName, array('livestock_hh'))) {
                                        $subArray['livestock_name'] = $sub->name;
                                        unset($sub->name);
                                    }

                                    unset($sub->id);
                                    $allData = array_merge($subArray, (array)$sub);
                                    $subResult = $this->model->insertSyncRecord($tableName, $allData);

                                    if($subResult === false) {
                                        $this->model->rollback();
                                        $error = 1;
                                    }
                                }
                            }
                        }
                        
                    }

                    if($this->model->end() === false) $error = 1;

                } else {
                    $error = 1;
                }
            }
        } else{
            $returnNewData['code'] = "-1";
            $returnNewData['text'] = "Error: missing table details";
        }

        if($error == 1) {
            $returnNewData['action'] = 'insert';
            $returnNewData['code'] = "-1";
            $returnNewData['message'] = "Error: Coudn't insert this row.";
        }

        $returnString = json_encode($returnNewData);
        error_log($returnString);
        die(json_encode($returnNewData));
    }

	public function sync() {
        $params = array('type' => 'AppSync');
		$params['title'] = 'Sync AppSync form data';
		$forms = $this->formModel->getAllForms(); 
        $params['forms'] = $forms;
        $this->view('odk-sync.html',compact('params'));
    }

    public function data() {

        $params['type'] = 'AppSync';

        if(!$this->request) {
            $this->setFlash(['ERROR! Please select form to sync', 'danger']);
			$this->redirect(array('AppSync','sync'));
        }

        $formDetails = $this->formModel->getFormDetailsById($this->request['form']);
        $params['form_id'] = $formId = $formDetails['f_id'];
        $params['title'] = 'AppSync Form Data ('.$formDetails['description'].')';
        
        $this->model->setCoreTable($formDetails['app_table']);
        $formData = $this->model->getCoreTable($this->session['user']['user_id']);

        $params['data'] = $formData;
        $params['buttons'] = true;
        $params['data-available'] = (sizeof($formData) > 0) ? true : false;

        $this->view('odk-data.html',compact('params'));
    }

    public function searchSpecies($habitSpecies, $speciesName, $field = 'name') {
        foreach($habitSpecies as $sp) {
            if(strtolower($sp[$field]) == trim($speciesName)) {
                return $sp;
            }
        }
    }
    
    public function syncFormData() {
        if(!$this->request) {
            $this->setFlash(['ERROR! Please select form to sync', 'danger']);
			$this->redirect(array('AppSync','sync'));
        }

        $formId = $this->request['form'];

        if(!isset($this->request['forms'])) {
            $this->setFlash(['ERROR! Please select atleast 1 form to sync', 'danger']);
			$this->redirect(array('AppSync','data?form='.$formId));
        }

        $selectedForms = isset($this->request['forms']) ? $this->request['forms'] : array();

        $syncFilter = array(
            'type' => 'M',
            'create_by' => $this->session['user']['user_id'],
            'state' => $this->session['user']['state_code'],
            'f_id' => $formId,
            'limit' => 0
        );

        $this->syncProcess($syncFilter, $selectedForms);
    }

    public function autoSyncFormData() {
        $oneTimeSync = 100;

        $syncFilter = $this->model->getMaxFormCounts();

        if($syncFilter === false) {
            error_log("autoSyncFormData: No form available to sync");
        } else {
            $syncFilter['limit'] = $oneTimeSync;
            $syncFilter['type'] = 'A';
            $this->errorLog($syncFilter['form_type'], $syncFilter['total_rows'].' forms are available to sync for DFO "'.$syncFilter['create_by'].'". First '.($syncFilter['total_rows'] > $oneTimeSync ? $oneTimeSync. ' out of '.$syncFilter['total_rows'] : $syncFilter['total_rows']).' will be synced now.');
            $this->syncProcess($syncFilter);
        }
        exit;
    }

    public function errorLog($type, $message) {

        $logPath = $this->publicDir.'/errors/';

        $logFile = $logPath.$type.'-appsync-debug.log';

        // Check if log file is available and create new file if size exceeds 4MB
        if(file_exists($logFile)) {
			$size = filesize($logFile);

			if($size >= (1024*1024*4)) {
				rename($logFile, $logPath.date('YmdHis').'-'.$type.'-appsync-debug.log');
			}
		}

        // Append to the log file
        if($fd = fopen($logFile, "a")) {
            $result = fputs($fd, "\n[".date('Y-m-d H:i:s')."] ".implode(": ", array((isset($_SESSION['user']) ? $_SESSION['user']['user_id'] : 'AutoSyncCron'), $message)));
            fclose($fd);
        }
        else {
            error_log('Unable to open log '.$logFile.'!');
        }
    }

    public function viewLog() {

        $logPath = $this->publicDir.'/errors/';

        if(!isset($this->request['t'])) {
            $logFile = $logPath.'error.log';
        } else {
            $logFile = $logPath.$this->request['t'].'-appsync-debug.log';
        }

        // Check if log file is available and create new file if size exceeds 4MB
        if(file_exists($logFile)) {
            $logData = file_get_contents($logFile);
            echo nl2br($logData);
		} else {
            error_log('Unable to open log '.$logFile.'!');
        }
    }

    private function syncProcess($syncFilter, $selectedForms = array()) {

        $stateCode = $syncFilter['state'];
        $formId = $syncFilter['f_id'];
        $userId = $syncFilter['create_by'];
        $limit = $syncFilter['limit'];

        $this->utilityModel->setStateCode($stateCode);
        $regions = $this->utilityModel->getForestHierarchy();

        $hierarchyData = array();
        $assignedDivision = 0;
        if($regions !== false) {
            if(isset($regions['hierarchy']) && sizeof($regions['hierarchy']) > 0) {
                $regions['hierarchy'] = array_reverse($regions['hierarchy']);

                foreach($regions['hierarchy'] as $type) {

                    $order = $type['h_order'];

                    if($order == 6) continue;

                    $name = strtolower($type['name']);

                    $hierarchyData[$name] = array();
                    if(isset($regions['data']) && sizeof($regions['data']) > 0) {
                        foreach($regions['data'] as $data) {

                            $id = $data['h'.$order.'_id'];

                            if($name == 'division') $assignedDivision = $id;

                            $regionArray = array(
                                'id' => $id,
                                'name' => $data['h'.$order.'_name'],
                            );

                            if(isset($data['h'.($order-1).'_id'])) {
                                $regionArray['parent_id'] = $data['h'.($order-1).'_id'];
                                $regionArray['parent_name'] = $data['h'.($order-1).'_name'];
                            }
                            
                            if($name == 'circle') {
                                $regionArray['parent_id'] = $stateCode;
                            }

                            $hierarchyData[$name][$id] = $regionArray;
                        }
                    } else {
                        $this->setFlash(['ERROR! '.$type.' data not available', 'danger']);
			            $this->redirect(array('AppSync','sync'));
                    }
                }
            }
        } else {
            $this->setFlash(['ERROR! Hierarchy data not available', 'danger']);
			$this->redirect(array('AppSync','sync'));
        }

        $species = $this->formModel->getSpeciesByUser($userId);

        $habitSpecies = array();
        if($species !== false) { 
            foreach($species as $sp) {
                $habitId = $sp['habit_id'];
                $habitSpecies[$habitId][] = $sp;
            }
        }

        if(sizeof($habitSpecies) == 0) {
            $this->setFlash(['ERROR! Species data not available', 'danger']);
			$this->redirect(array('AppSync','sync'));
        }

        $formDetails = $this->formModel->getFormDetailsById($formId);

        $t = $formDetails['app_table'];

        $this->errorLog($t, "Syncing started");

        $typeDetails = $this->tableTypes[$formDetails['app_table']];

        if(isset($typeDetails['skip'])) {
            $this->skipFields = array_merge($this->skipFields, $typeDetails['skip']);
        }
 
        $this->model->setCoreTable($formDetails['app_table']);
        $formData = $this->model->getCoreTable($userId, $selectedForms, $limit);

        if($formData === false) {
            $this->setFlash(['ERROR! No data available in selected form', 'danger']);
			$this->redirect(array('AppSync','sync'));
        }

        $userData = $this->userModel->getUsersByFilter();

        $formUsers = array();
        if($userData !== false) {
            foreach($userData as $user) {
                $userId = $user['user_id'];
                $formUsers[$userId] = $user['name'];
            }
        }

        $attributes = $this->formModel->getFormAttributes($formId);

        if($attributes === false) {
            $this->setFlash(['ERROR! No attributes available for selected form', 'danger']);
			$this->redirect(array('AppSync','sync'));
        }

        $appAttributes = $groups = $checkBoxes = $groupAttributes = $sectionFields = $formAttributes = array();
        foreach($attributes as $attr) {

            $appField = !empty($attr['group_id']) ? $attr['app_field_rep'] : $attr['field_name'];
            $appAttributes[$appField] = $attr;

            if(!empty($attr['field_name'])) {
                $appAttributes[$attr['field_name']] = $attr;
            }

            $faId = $attr['fa_id'];
            $formAttributes[$faId] = $attr;

            if($attr['ui_element'] == 'checkbox' && !empty($appField)) {
                $checkBoxes[$appField] = $attr;
            }

            if(empty($attr['app_table_rep']) && !empty($attr['group_id'])) {
                $sectionFields[] = $appField;
            }

            if(!empty($attr['group_id']) && !empty($attr['app_field_rep'])) {
                $groupId = $attr['group_id'];
                $groups[$groupId] = $attr;
                $groupAttributes[$groupId][$appField] = $attr;
            }
        }

        $appGroups = array();
        if(sizeof($groups) > 0) {
            foreach($groups as $group) {

                if(empty($group['app_table_rep'])) continue;

                $groupId = $group['group_id'];
                $this->model->setCoreTable($group['app_table_rep']);
                $groupData = $this->model->getGroupTable($typeDetails['type']);

                if($groupData !== false) {
                    foreach($groupData as $gd) {
                        $gd['app_table'] = $group['app_table_rep'];
                        $gd['group_name'] = $group['group_name'];
                        $uri = $gd[$typeDetails['type'].'_id'];
                        $appGroups[$groupId][$uri][] = $gd;
                    }
                }
            }
        }

        $display = 2;

        if($display == 1) {
            echo "<pre>";
            ini_set('log_errors', FALSE);
        }

        $formErrors = array();

        if($display == 1) $surveyId = 1;
        
        foreach($formData as $fd) {

            $this->errorLog($t, "Syncing form ID: ".$fd['id']);

            if($fd['version'] != $this->version)  {
                $this->errorLog($t, "VERSION: ".$fd['version']);
                continue;
            }

            if($display == 1) echo '<hr />';

            if($display == 2) $this->surveyModel->begin();

            $parentId = $fd['id'];

            $surveyData = array(
                'f_id' => $formId,
                'device_id' => $fd['device_id'],
                'app_ref_id' => $fd['id'],
                'create_date' => $fd['date_created'],
                'create_by' => $fd['user_id'],
                'b_id' => $fd['beat'],
                'comp_id' => isset($fd['compartment']) ? $fd['compartment'] : NULL,
                'plot_id' => isset($fd['plot']) ? $fd['plot'] : NULL,
                'timestamp' => date('Y-m-d H:i:s')
            ); 
            if($display == 1) print_r($surveyData); 
            if($display == 2) {
                $surveyId = $this->surveyModel->setSurveyAppSyncForm($surveyData); 
                $this->errorLog($t, "Survey ID generated: ".$surveyId);
            }

            if((isset($surveyId) && $surveyId !== false) || $display == 1) {

                $fSeq = 1;
                foreach($fd as $key => $value) {

                    if(in_array($key, $this->skipFields) && !in_array($key, array_keys($this->faunaFields))) continue;

                    if(isset($typeDetails['habit']) && in_array($key, array_keys($typeDetails['habit']))) {

                        $groupId = $typeDetails['group']['id']; if($display == 1) echo $key;

                        foreach($typeDetails['group'] as $fKey => $faId) {

                            if($fKey == 'id') continue;

                            if($display == 1) echo $fKey;

                            $dataArray = array(
                                'fa_id' => $faId,
                                's_id' => $surveyId,
                                'g_id' => $groupId,
                                'seq_id' => $fSeq,
                                'ref_table' => 'f'
                            );

                            if($fKey == 'fauna' && isset($this->faunaFields[$key])) {

                                $fieldDesc = $faId.' : '.$fKey.' : '.$key;

                                if($display == 1) echo $fieldDesc;

                                $dataArray['value'] = ucwords($key);
                                $dataArray['p_id'] = $this->faunaFields[$key];

                                if($display == 1) print_r($dataArray); 
                                if($display == 2) {
                                    
                                    
                                    $faunaSave = $this->surveyModel->setFormData($dataArray); 

                                    if($faunaSave === false) {
                                        $formErrors[] = $error = 'DBError: Not able to save fauna: '.$key;
                                        $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                        $this->surveyModel->rollback();
                                        continue 3;
                                    }
                                }
                            }

                            if($fKey == 'description') {

                                $fieldDesc = $faId.' : '.$fKey.' : '.$fd[$key.'_comments'];

                                if($display == 1) echo $fieldDesc;

                                $dataArray['value'] = (!isset($fd[$key.'_comments']) || empty($fd[$key.'_comments'])) ? '' : $fd[$key.'_comments'];
                                $dataArray['p_id'] = NULL;

                                if($display == 1) print_r($dataArray); 
                                if($display == 2) {
                                    $faunaSave = $this->surveyModel->setFormData($dataArray); 

                                    if($faunaSave === false) {
                                        $formErrors[] = $error = 'DBError: Not able to save fauna comments: '.$key;
                                        $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                        $this->surveyModel->rollback();
                                        continue 3;
                                    }
                                }
                            }

                            if($fKey == 'species_name') {
                                $attr = $formAttributes[$faId];

                                $fieldDesc = $attr['name'].'('.$attr['fa_id'].' : '.$attr['attri_id'].' : '.$fKey.' : '.$value.' : '.$attr['ui_element'].')';
                    
                                if($display == 1) echo $fieldDesc;

                                if($display == 1) echo $this->mappings['species'][$attr['attri_id']]['values'];

                                $speciesData = explode(":", $this->mappings['species'][$attr['attri_id']]['values']);

                                if(is_numeric($speciesData[2]) && $speciesData[2] != '{habit_id}') {
                                    $habitId = $speciesData[2];
                                } else {
                                    $habitId = $typeDetails['habit'][$key];
                                }

                                if(!isset($habitSpecies[$habitId])) {
                                    $formErrors[] = $error = '[Fauna] [Species] Value not set for '.$attr['field_name'].': ('.$value.')';
                                    $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                    $this->surveyModel->rollback();
                                    continue 3;
                                }

                                if($attr['ui_element'] == 'checkbox') {

                                    $valueArray = explode(",", $value);

                                    foreach($valueArray as $cValue) {

                                        if(!empty($cValue)) {
                                            if(!is_numeric($cValue)) {                                                
                                                $pValue = $this->searchSpecies($habitSpecies[$habitId], $cValue);

                                                $dataArray['value'] = $pValue['name'];
                                                $dataArray['p_id'] = $pValue['id'];

                                            } else {                                                
                                                $pValue = $this->searchSpecies($habitSpecies[$habitId], $cValue, 'id');

                                                $dataArray['value'] = $pValue['name'];
                                                $dataArray['p_id'] = $cValue;
                                            }
                                        } else {
                                            $dataArray['value'] = $cValue;
                                            $dataArray['p_id'] = NULL;
                                        }
                                        $dataArray['ref_table'] = 't';

                                        if($display == 1) print_r($dataArray); 
                                        if($display == 2) {
                                            
                                            
                                            $faunaSave = $this->surveyModel->setFormData($dataArray); 

                                            if($faunaSave === false) {
                                                $this->surveyModel->rollback();
                                                $formErrors[] = $error = 'DBError: Not able to save fauna species: '.$key;
                                                $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                                continue 3;
                                            }
                                        }
                                    }
                                } else {
                                    if(!is_numeric($value)) {
                                        $pValue = $this->searchSpecies($habitSpecies[$habitId], $value);

                                        $dataArray['value'] = $pValue['name'];
                                        $dataArray['p_id'] = $pValue['id'];
                                    } else {
                                        $pValue = $this->searchSpecies($habitSpecies[$habitId], $value, 'id');

                                        $dataArray['value'] = $pValue['name'];
                                        $dataArray['p_id'] = $value;
                                    }
                                    $dataArray['ref_table'] = 't';

                                    if($display == 1) print_r($dataArray); 
                                    if($display == 2) {
                                        
                                        
                                        $faunaSave = $this->surveyModel->setFormData($dataArray); 

                                        if($faunaSave === false) {
                                            $this->surveyModel->rollback();
                                            $formErrors[] = $error = 'DBError: Not able to save fauna species: '.$key;
                                            $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                            continue 3;
                                        }
                                    }
                                }
                            }
                        }

                        $fSeq++;
                        
                        continue;
                    }

                    if(isset($typeDetails['skip']) && in_array($key, $typeDetails['skip'])) continue;

                    if(!isset($appAttributes[$key])) {
                        $formErrors[] = $error = '[General] Attribute not available in system: '.$key;
                        $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                        $this->surveyModel->rollback();
                        continue 2;
                    }

                    $attr = $appAttributes[$key];

                    $fieldDesc = $attr['name'].'('.$attr['fa_id'].' : '.$attr['attri_id'].' : '.$key.' : '.$value.' : '.$attr['ui_element'].')';
                    
                    if($display == 1) echo $fieldDesc;

                    if(empty($value) || is_null($value) || $value == 'null') {

                        $dataArray = array(
                            'fa_id' => $attr['fa_id'],
                            's_id' => $surveyId,
                            'g_id' => in_array($key, $sectionFields) ? $attr['group_id'] : NULL,
                            'seq_id' => in_array($key, $sectionFields) ? 1 : 0,
                            'ref_table' => 'f',
                            'value' => $value,
                            'p_id' => NULL
                        );
                        if($display == 1) print_r($dataArray); 
                        if($display == 2) {
                            
                            
                            $this->surveyModel->setFormData($dataArray); 
                        }
                            
                    } else {

                        $lowerValue = strtolower($value);

                        if($attr['ui_element'] != 'checkbox') {
                            $dataArray = array(
                                'fa_id' => $attr['fa_id'],
                                's_id' => $surveyId,
                                'g_id' => in_array($key, $sectionFields) ? $attr['group_id'] : NULL,
                                'seq_id' => in_array($key, $sectionFields) ? 1 : 0,
                                'ref_table' => 'f'
                            );

                            if($attr['ui_element'] == 'select') {

                                if(isset($this->mappings['attr'][$attr['attri_id']][$lowerValue]) && $attr['data_type'] != 'month') {
                                    $dataArray['value'] = ucwords($value);
                                    $dataArray['p_id'] = $this->mappings['attr'][$attr['attri_id']][$lowerValue];
                                } else if(!isset($this->mappings['attr'][$attr['attri_id']][$lowerValue]) && $attr['data_type'] != 'month') {

                                    if(isset($this->mappings['species'][$attr['attri_id']])) {

                                        if($display == 1) echo $this->mappings['species'][$attr['attri_id']]['values'];
        
                                        $speciesData = explode(":", $this->mappings['species'][$attr['attri_id']]['values']);
        
                                        if(!is_numeric($value)) {
                                            $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $value);
        
                                            $dataArray['value'] = $pValue['name'];
                                            $dataArray['p_id'] = $pValue['id'];
                                        } else {
                                            $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $value, 'id');
        
                                            $dataArray['value'] = $pValue['name'];
                                            $dataArray['p_id'] = $value;
                                        }
                                        $dataArray['ref_table'] = 't';
                                    } else {

                                        if(!is_numeric($value)) {
                                            $formErrors[] = $error = '[Main Table] [Select] Value not set for '.$attr['field_name'].': ('.$value.')';
                                            $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                            $this->surveyModel->rollback();
                                            continue 2;
                                        } 
                                        
                                        $dataValue = '';
                                        if(isset($this->attrValues[$attr['attri_id']][$value])) {
                                            $dataValue = $this->attrValues[$attr['attri_id']][$value];
                                        } else if(isset($this->attrRef[$attr['attri_id']]) && isset($this->attrValues[$this->attrRef[$attr['attri_id']]][$value])) {
                                            $dataValue = $this->attrValues[$this->attrRef[$attr['attri_id']]][$value];
                                        }

                                        $dataArray['value'] = $dataValue;
                                        $dataArray['p_id'] = $value;
                                    }
                                } else if($attr['data_type'] == 'month') {
                                    if(in_array($value, $this->langMonths)) {
                                        $monthValue = array_search($value, $this->langMonths);
                                    } else {
                                        $date = $value." 01 2000";
                                        $monthValue = date("n", strtotime($date));
                                    }

                                    $monthName = date("F", mktime(0, 0, 0, $monthValue, 10));

                                    $lowerMonth = strtolower($monthName);

                                    $pId = isset($this->mappings['attr'][$attr['attri_id']][$lowerMonth]) ? $this->mappings['attr'][$attr['attri_id']][$lowerMonth] : NULL;

                                    $dataArray['value'] = $monthName;
                                    $dataArray['p_id'] = $pId;
                                } else {
                                    $formErrors[] = $error = '[Main Table] Species value not available '.$attr['field_name'].': ('.$value.')';
                                    $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                    $this->surveyModel->rollback();
                                    continue 2;
                                }
                            } else if($attr['ui_element'] == 'radio') {

                                if(!isset($typeDetails[$lowerValue])) {
                                    $formErrors[] = $error = '[Main Table] [Radio] Value not set for '.$attr['field_name'].': ('.$value.')';
                                    $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                    $this->surveyModel->rollback();
                                    continue 2;
                                }

                                $dataArray['p_id'] = NULL;
                                $dataArray['value'] = $typeDetails[$lowerValue];
                            } else {
                                $dataArray['p_id'] = NULL;
                                $dataArray['value'] = $value;
                            } 
                            
                            if($display == 1) print_r($dataArray); 
                            if($display == 2) {
                                
                                
                                $this->surveyModel->setFormData($dataArray); 
                            }

                        } else if($attr['ui_element'] == 'checkbox') {

                            $valueArray = explode(",", $value);

                            foreach($valueArray as $newValue) {

                                $newValue = trim($newValue);

                                $lowerNewValue = strtolower($newValue);

                                $dataArray = array(
                                    'fa_id' => $attr['fa_id'],
                                    's_id' => $surveyId,
                                    'g_id' => in_array($key, $sectionFields) ? $attr['group_id'] : NULL,
                                    'seq_id' => in_array($key, $sectionFields) ? 1 : 0,
                                    'ref_table' => 'f'
                                );

                                if(isset($this->mappings['attr'][$attr['attri_id']][$lowerNewValue]) && $attr['data_type'] != 'month') {
                                    $dataArray['value'] = ucwords($newValue);
                                    $dataArray['p_id'] = $this->mappings['attr'][$attr['attri_id']][$lowerNewValue];
                                } else if(!isset($this->mappings['attr'][$attr['attri_id']][$lowerNewValue]) && $attr['data_type'] != 'month') {

                                    if(isset($this->mappings['species'][$attr['attri_id']])) {

                                        $speciesData = explode(":", $this->mappings['species'][$attr['attri_id']]['values']);

                                        if(!is_numeric($newValue)) { 
                                            $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $lowerNewValue);

                                            $dataArray['value'] = $pValue['name'];
                                            $dataArray['p_id'] = $pValue['id'];
                                        } else {
                                            $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $lowerNewValue, 'id');

                                            $dataArray['value'] = $pValue['name'];
                                            $dataArray['p_id'] = $newValue;
                                        }
                                        $dataArray['ref_table'] = 't';
                                    } else {

                                        if(!is_numeric($newValue)) {
                                            $formErrors[] = $error = '[Main Table] [Checkbox] Value not set for '.$attr['field_name'].': ('.$newValue.')';
                                            $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                            $this->surveyModel->rollback();
                                            continue 3;
                                        }

                                        $dataValue = '';
                                        if(isset($this->attrValues[$attr['attri_id']][$newValue])) {
                                            $dataValue = $this->attrValues[$attr['attri_id']][$newValue];
                                        } else if(isset($this->attrRef[$attr['attri_id']]) && isset($this->attrValues[$this->attrRef[$attr['attri_id']]][$newValue])) {
                                            $dataValue = $this->attrValues[$this->attrRef[$attr['attri_id']]][$newValue];
                                        }

                                        $dataArray['value'] = $dataValue;
                                        $dataArray['p_id'] = $newValue;
                                    }
                                } else if($attr['data_type'] == 'month') {

                                    if(in_array($newValue, $this->langMonths)) {
                                        $monthValue = array_search($newValue, $this->langMonths);
                                    } else {
                                        $date = $newValue." 01 2000";
                                        $monthValue = date("n", strtotime($date));
                                    }

                                    $monthName = date("F", mktime(0, 0, 0, $monthValue, 10));

                                    $lowerMonth = strtolower($monthName);

                                    $pId = isset($this->mappings['attr'][$attr['attri_id']][$lowerMonth]) ? $this->mappings['attr'][$attr['attri_id']][$lowerMonth] : NULL;

                                    $dataArray['value'] = $monthName;
                                    $dataArray['p_id'] = $pId;
                                } else {
                                    $dataArray['p_id'] = NULL;
                                    $dataArray['value'] = $newValue;
                                } 
                                
                                if($display == 1) print_r($dataArray); 
                                if($display == 2) {
                                    
                                    
                                    $this->surveyModel->setFormData($dataArray); 
                                }
                            }
                        }
                    }
                }

                if(sizeof($appGroups) > 0) {
                    foreach($appGroups as $groupId => $groupData) {

                        if(isset($groupData[$parentId])) {

                            if($display == 1) echo '<h3>'.$groupData[$parentId][0]['group_name'].'</h3>';

                            $i = 1;
                            foreach($groupData[$parentId] as $gd) {

                                $tableName = $gd['app_table'];

                                $gUri = $gd[$typeDetails['type'].'_id'];
                                $seq = $i;

                                $habitKey = (isset($typeDetails['habit-field'])) ? $gd[$typeDetails['habit-field']] : 0;

                                foreach($gd as $gKey => $gValue) {
                                    
                                    if(in_array($gKey, $this->skipFields)) continue;

                                    if(!isset($groupAttributes[$groupId][$gKey])) {
                                        $formErrors[] = $error = '[Group Attribute] Attribute not available in system/group: '.$gKey;
                                        $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                        continue 4;
                                    }

                                    $attr = $groupAttributes[$groupId][$gKey];

                                    $fieldDesc = $attr['name'].'('.$attr['fa_id'].' : '.$attr['attri_id'].' : '.$attr['app_field_rep'].' : '.$gValue.' : '.$attr['ui_element'].' : '.$attr['data_type'].')';
                                        
                                    if($display == 1) echo $fieldDesc;

                                    if(empty($gValue) || is_null($gValue) || $gValue == 'null') {

                                        $dataArray = array(
                                            'fa_id' => $attr['fa_id'],
                                            's_id' => $surveyId,
                                            'g_id' => $groupId,
                                            'seq_id' => $seq,
                                            'ref_table' => 'f',
                                            'value' => $gValue,
                                            'p_id' => NULL
                                        );
                                        if($display == 1) print_r($dataArray); 
                                        if($display == 2) {
                                            
                                            
                                            $this->surveyModel->setFormData($dataArray); 
                                        }
                                            
                                    } else {

                                        $lowerGValue = strtolower($gValue);

                                        if($attr['ui_element'] != 'checkbox') {
                                            $dataArray = array(
                                                'fa_id' => $attr['fa_id'],
                                                's_id' => $surveyId,
                                                'g_id' => $groupId,
                                                'seq_id' => $seq,
                                                'ref_table' => 'f'
                                            );

                                            if(empty($gValue)) {
                                                $dataArray['value'] = $gValue;
                                                $dataArray['p_id'] = NULL;
                                            } else if($attr['ui_element'] == 'select') {

                                                $lowerGValue = in_array($attr['app_field_rep'], array('seedlingtype', 'saplingtype')) ? str_replace(' '.$attr['app_table_rep'], '', $lowerGValue) : $lowerGValue;

                                                if(isset($this->mappings['attr'][$attr['attri_id']][$lowerGValue]) && $attr['data_type'] != 'month') {
                                                    $dataArray['value'] = ucwords($gValue);
                                                    $dataArray['p_id'] = $this->mappings['attr'][$attr['attri_id']][$lowerGValue];
                                                } else if(!isset($this->mappings['attr'][$attr['attri_id']][$lowerGValue]) && $attr['data_type'] != 'month') {

                                                    if(isset($this->mappings['species'][$attr['attri_id']])) {

                                                        if($display == 1) echo $this->mappings['species'][$attr['attri_id']]['values'];
                        
                                                        $speciesData = explode(":", $this->mappings['species'][$attr['attri_id']]['values']);

                                                        if(is_numeric($speciesData[2]) && $speciesData[2] != '{habit_id}') {
                                                            $habitId = $speciesData[2];
                                                        } else {
                                                            if(in_array($tableName, array('seedling', 'sapling'))) {
                                                                $tableName = strstr(strtolower($gd[$tableName.'type']), 'tree') ? 'tree' : 'shrub';
                                                                $habitId = $typeDetails['habit'][$tableName];
                                                            } else if(isset($typeDetails['habit-values'])) {
                                                                $habitId = $typeDetails['habit-values'][$habitKey];
                                                            } else {
                                                                $habitId = $typeDetails['habit'][$tableName];
                                                            }
                                                        }

                                                        if(!isset($habitSpecies[$habitId])) {
                                                            $formErrors[] = $error = '[Group Table] [Species] Value not set for '.$attr['field_name'].': ('.$gValue.')';
                                                            $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                                            $this->surveyModel->rollback();
                                                            continue 4;
                                                        }

                                                        if(!is_numeric($gValue)) {
                                                            $pValue = $this->searchSpecies($habitSpecies[$habitId], $gValue);
                        
                                                            $dataArray['value'] = $pValue['name'];
                                                            $dataArray['p_id'] = $pValue['id'];
                                                        } else {
                                                            $pValue = $this->searchSpecies($habitSpecies[$habitId], $gValue, 'id');

                                                            $dataArray['value'] = $pValue['name'];
                                                            $dataArray['p_id'] = $gValue;
                                                        }
                                                        $dataArray['ref_table'] = 't';
                                                    } else {
                
                                                        if(!is_numeric($gValue)) {
                                                            $formErrors[] = $error = '[Group Table] [Select] Value not set for '.$attr['field_name'].': ('.$gValue.')';
                                                            $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                                            $this->surveyModel->rollback();
                                                            continue 4;
                                                        }

                                                        $dataValue = '';
                                                        if(isset($this->attrValues[$attr['attri_id']][$gValue])) {
                                                            $dataValue = $this->attrValues[$attr['attri_id']][$gValue];
                                                        } else if(isset($this->attrRef[$attr['attri_id']]) && isset($this->attrValues[$this->attrRef[$attr['attri_id']]][$gValue])) {
                                                            $dataValue = $this->attrValues[$this->attrRef[$attr['attri_id']]][$gValue];
                                                        }
                    
                                                        $dataArray['value'] = $dataValue;
                                                        $dataArray['p_id'] = $gValue;
                                                    }
                                                } else if($attr['data_type'] == 'month') {

                                                    if(in_array($gValue, $this->langMonths)) {
                                                        $monthValue = array_search($gValue, $this->langMonths);
                                                    } else {
                                                        $date = $gValue." 01 2000";
                                                        $monthValue = date("n", strtotime($date));
                                                    }

                                                    $monthName = date("F", mktime(0, 0, 0, $monthValue, 10));

                                                    $lowerMonth = strtolower($monthName);

                                                    $pId = isset($this->mappings['attr'][$attr['attri_id']][$lowerMonth]) ? $this->mappings['attr'][$attr['attri_id']][$lowerMonth] : NULL;

                                                    $dataArray['value'] = $monthName;
                                                    $dataArray['p_id'] = $pId;
                                                } else {
                                                    $formErrors[] = $error = 'Species value not available '.$attr['field_name'].': ('.$gValue.')';
                                                    $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                                    $this->surveyModel->rollback();
                                                    continue 4;
                                                }
                                            } else if($attr['ui_element'] == 'radio') {

                                                if(!in_array($lowerGValue, array('yes', 'no'))) $lowerGValue = 'no';
                
                                                if(!isset($typeDetails[$lowerGValue])) {
                                                    $formErrors[] = $error = '[Group Table] [Radio] Value not set for '.$attr['field_name'].': ('.$gValue.')';
                                                    $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                                    $this->surveyModel->rollback();
                                                    continue 4;
                                                }
                
                                                $dataArray['p_id'] = NULL;
                                                $dataArray['value'] = $typeDetails[$lowerGValue];
                                            } else {
                                                $dataArray['p_id'] = NULL;
                                                $dataArray['value'] = $gValue;
                                            } 
                                            
                                            if($display == 1) print_r($dataArray); 
                                            if($display == 2) {
                                                
                                                
                                                $this->surveyModel->setFormData($dataArray);
                                            }
                                        } else if($attr['ui_element'] == 'checkbox') {

                                            $gValueArray = explode(",", $gValue);

                                            foreach($gValueArray as $newGValue) {

                                                $newGValue = trim($newGValue);

                                                $lowerNewGValue = strtolower($newGValue);

                                                $dataArray = array(
                                                    'fa_id' => $attr['fa_id'],
                                                    's_id' => $surveyId,
                                                    'g_id' => $groupId,
                                                    'seq_id' => $seq,
                                                    'ref_table' => 'f'
                                                );

                                                if(isset($this->mappings['attr'][$attr['attri_id']][$lowerNewGValue]) && $attr['data_type'] != 'month') {
                                                    $dataArray['value'] = ucwords($newGValue);
                                                    $dataArray['p_id'] = $this->mappings['attr'][$attr['attri_id']][$lowerNewGValue];
                                                } else if(!isset($this->mappings['attr'][$attr['attri_id']][$lowerNewGValue]) && $attr['data_type'] != 'month') {

                                                    if(isset($this->mappings['species'][$attr['attri_id']])) {
                    
                                                        $speciesData = explode(":", $this->mappings['species'][$attr['attri_id']]['values']);

                                                        if(is_numeric($speciesData[2]) && $speciesData[2] != '{habit_id}') {
                                                            $habitId = $speciesData[2];
                                                        } else {
                                                            $habitId = $typeDetails['habit-values'][$habitKey];
                                                        }
        
                                                        if(!is_numeric($newGValue)) {
                                                            $pValue = $this->searchSpecies($habitSpecies[$habitId], $newGValue);
                    
                                                            $dataArray['value'] = $pValue['name'];
                                                            $dataArray['p_id'] = $pValue['id'];
                                                        } else {
                                                            $pValue = $this->searchSpecies($habitSpecies[$habitId], $newGValue, 'id');
        
                                                            $dataArray['value'] = $pValue['name'];
                                                            $dataArray['p_id'] = $newGValue;
                                                        }
                                                        $dataArray['ref_table'] = 't';
                                                    } else {
                
                                                        if(!is_numeric($newGValue)) {
                                                            $formErrors[] = $error = '[Group Table] [Checkbox] Value not set for '.$attr['field_name'].': ('.$newGValue.')';
                                                            $this->errorLog($t, "Rolled back due to error: ".$error." [ ".$fieldDesc." ]");
                                                            $this->surveyModel->rollback();
                                                            continue 5;
                                                        }

                                                        $dataValue = '';
                                                        if(isset($this->attrValues[$attr['attri_id']][$newGValue])) {
                                                            $dataValue = $this->attrValues[$attr['attri_id']][$newGValue];
                                                        } else if(isset($this->attrRef[$attr['attri_id']]) && isset($this->attrValues[$this->attrRef[$attr['attri_id']]][$newGValue])) {
                                                            $dataValue = $this->attrValues[$this->attrRef[$attr['attri_id']]][$newGValue];
                                                        }
                    
                                                        $dataArray['value'] = $dataValue;
                                                        $dataArray['p_id'] = $newGValue;
                                                    }
                                                } else if($attr['data_type'] == 'month') {

                                                    if(in_array($newGValue, $this->langMonths)) {
                                                        $monthValue = array_search($newGValue, $this->langMonths);
                                                    } else {
                                                        $date = $newGValue." 01 2000";
                                                        $monthValue = date("n", strtotime($date));
                                                    }

                                                    $monthName = date("F", mktime(0, 0, 0, $monthValue, 10));

                                                    $lowerMonth = strtolower($monthName);

                                                    $pId = isset($this->mappings['attr'][$attr['attri_id']][$lowerMonth]) ? $this->mappings['attr'][$attr['attri_id']][$lowerMonth] : NULL;

                                                    $dataArray['value'] = $monthName;
                                                    $dataArray['p_id'] = $pId;
                                                } else {
                                                    $dataArray['p_id'] = NULL;
                                                    $dataArray['value'] = $newGValue;
                                                } 
                                                if($display == 1) print_r($dataArray);
                                                if($display == 2) {
                                                    
                                                    
                                                    $this->surveyModel->setFormData($dataArray);
                                                }
                                            }
                                        }
                                    }
                                }
                                $i++;
                            }
                        }
                    }
                }

                if($this->surveyModel->end() === false) {
                    $formErrors[] = $error = 'DBError: Unable to commit data';
                    $this->errorLog($t, "Rolled back due to error: ".$error);
                } else $syncedRows[] = $fd['id'];
            } else {
                $formErrors[] = $error = 'DBError: Unable to save form: '.$fd['id'];
                $this->errorLog($t, "Rolled back due to error: ".$error);
            }
            if($display == 1) $surveyId++;
        }

        if($display == 1) {
            print_r($formErrors); print_r($syncedRows); exit;
        }

        if(sizeof($syncedRows) > 0) {
            $this->errorLog($t, "Marked as synced rows: ".json_encode($syncedRows));
            $this->model->markSynced($formDetails['app_table'], $syncedRows);
        }

        $params = array(
            'title' => 'Sync Results ('.$formDetails['description'].')',
            'type' => 'AppSync'
        );

        $success = 1;
        if(sizeof($formErrors) > 0) {
            $success = 0;
            $params['errors'] = $formErrors;
        }

        $params['status'] = $success;

        if(isset($syncFilter['type']) && $syncFilter['type'] == 'M') {
            $this->view('odk-sync-success.html',compact('params'));
        } else {
            die(1);
        }
    }
}