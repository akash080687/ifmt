<?php
namespace IFMT\App\Main;
use IFMT\App\Main\AppSync;

/**
* Home Controller
*/
class AppSyncMap extends AppSync
{
    public function __construct() {
        parent::__construct();
    }

    public function syncMap() {
        $params = array('type' => 'AppSyncMap');
		$params['title'] = 'Sync AppSync form data';
		$forms = $this->formModel->getAllForms(); 
        $params['forms'] = $forms;
        $this->view('odk-sync-map.html',compact('params'));
    }

    public function map() {

        $params['type'] = 'AppSyncMap';
        
        if(!$this->request) {
            $this->setFlash(['ERROR! Please select form to sync', 'danger']);
			$this->redirect(array('AppSyncMap','sync'));
        }

        $formDetails = $this->formModel->getFormDetailsById($this->request['form']);

        $typeDetails = $this->tableTypes[$formDetails['app_table']];

        if(isset($typeDetails['skip'])) {
            $this->skipFields = array_merge($this->skipFields, $typeDetails['skip']);
        } 

        $params['skip-fields'] = $this->skipFields;

        if($formDetails === false) {
            $this->setFlash(['ERROR! Provided form is not avaiilable', 'danger']);
			$this->redirect(array('AppSyncMap','sync'));
        }

        $params['title'] = 'Map App Form Columns ('.$formDetails['description'].')';

        $attributes = $this->formModel->getFormAttributes($formDetails['f_id']);

        if($attributes === false) {
            $this->setFlash(['ERROR! No attributes available for selected form', 'danger']);
			$this->redirect(array('AppSyncMap', 'sync'));
        }

        $groupAttributes = $groups = array();
        foreach($attributes as $attr) {

            if(empty($attr['group_id'])) continue;

            $groupId = $attr['group_id'];
            $groupAttributes[$groupId][] = $attr;
            $groups[$groupId] = $attr;
        }

        $params['attributes'] = $attributes;
        
        $this->model->setCoreTable($formDetails['app_table']);
        $formColumns = $this->model->getTableColumns();

        $params['columns'] = $formColumns;
        $params['form_id'] = $formDetails['f_id'];
        $params['groups'] = $groups;
        $params['group_attributes'] = $groupAttributes;

        if(sizeof($groups) > 0) {
            foreach($groups as $groupId => $details) {
                $this->model->setCoreTable($details['app_table_rep']);
                $groupColumns = $this->model->getTableColumns();

                $params['group_'.$groupId] = $groupColumns;
            }
        }

        $params['form_field'] = 'field_name';
        $params['group_field'] = 'app_field_rep';

        $this->view('odk-map.html',compact('params'));
    }

    public function mapOptions() {
        
        if(!$this->request) {
            $this->setFlash(['ERROR! Please select form to sync', 'danger']);
			$this->redirect(array('AppSyncMap','sync'));
        }

        $formDetails = $this->formModel->getFormDetailsById($this->request['form']);

        if($formDetails === false) {
            $this->setFlash(['ERROR! Provided form is not avaiilable', 'danger']);
			$this->redirect(array('AppSyncMap','sync'));
        }

        $params['form_id'] = $formId = $formDetails['f_id'];
        $params['title'] = 'Map Select/Checkbox Field Options ('.$formDetails['description'].')';

        $attributes = $this->formModel->getFormAttributeOptions($formDetails['f_id'], array('select', 'checkbox'));

        if($attributes === false) {
            $this->setFlash(['ERROR! No attributes available for selected form', 'danger']);
			$this->redirect(array('AppSyncMap','sync'));
        }

        $attributeOptions = $attributeDetails = $appFields = $groupFields = array();
        foreach($attributes as $attr) {
            $attrId = $attr['attr_id'];
            $appName = !empty($attr['group_id']) ? $attr['app_field_rep'] : $attr['field_name'];
            $groupTable = $attr['app_table_rep'];
            $attributeOptions[$appName][] = $attr;
            $appFields[$appName] = $attrId;
            $attributeDetails[$appName] = $attr;

            if(!empty($attr['group_id']) && !empty($appName)) {
                $groupFields[$groupTable][$appName][] = $attr;
                continue;
            }

            if($attr['ui_element'] == 'checkbox') {
                $appTables[$appName] = $attrId;
            }
        }

        $params['attributes'] = $attributeDetails;
        $params['values'] = $attributeOptions;
        
        $this->model->setCoreTable($formDetails['app_table']);
        $formData = $this->model->getCoreTable($this->session['user']['user_id']);

        if($formData === false) {
            $this->setFlash(['ERROR! No data available in provided form', 'danger']);
			$this->redirect(array('AppSyncMap','sync'));
        }

        $currentValues = array();
        foreach($formData as $fd) {
            foreach($fd as $key => $value) {
                if(!in_array($key, array_keys($appFields)) || empty($value)) continue;

                $lowerValue = strtolower($value);
                $currentValues[$key][] = $lowerValue;
            }
        }

        if(sizeof($appTables) > 0) {
            foreach($appTables as $fieldName => $attrId) {

                if(empty($fieldName)) continue;

                $fieldValues = array();
                foreach($formData as $fd) {
                    $fieldValues = array_merge($fieldValues, explode(",", $fd[$fieldName]));
                }

                $currentValues[$fieldName] = array_map('strtolower', $fieldValues);
            }
        }

        if(sizeof($groupFields) > 0) {

            foreach($groupFields as $groupTable => $fields) {

                $this->model->setCoreTable($groupTable);
                $gData = $this->model->getCoreTable($this->session['user']['user_id']);

                foreach($gData as $fd) {
                    foreach($fd as $key => $value) {
                        if(!in_array($key, array_keys($fields)) || empty($value)) continue;
        
                        $lowerValue = strtolower($value);
                        $currentValues[$key][] = $lowerValue;
                    }
                }
            }
        }

        $currentValues = array_map('array_unique', $currentValues);

        $params['odk_values'] = $currentValues;

        $params['selected_values'] = (isset($this->mappings['attr'])) ? $this->mappings['attr'] : array();

        $this->view('odk-map-options.html',compact('params'));
    }

    public function mapFields() {
        if($this->request) {
            $response = $this->formModel->setFormAppAttributes($this->request);
            $this->setFlash($response);
            $this->redirect(array('AppSyncMap','map?form='.$this->request['form']));
        } else {
            $this->redirect(array('AppSyncMap','sync'));
        }
    }

    public function mapFieldOptions() {
        if($this->request) {
            if(!isset($this->request['form'])) {
                $this->setFlash(['ERROR! Please select form to map options', 'danger']);
                $this->redirect(array('AppSyncMap','sync'));
            }

            $formId = $this->request['form'];
            echo json_encode($this->request['attr']); exit;

            $this->redirect(array('AppSyncMap', 'mapOptions?form='.$this->request['form']));
        } else {
            $this->redirect(array('AppSyncMap', 'sync'));
        }
    }

    public function searchSpecies($habitSpecies, $speciesName, $field = 'name') {
        foreach($habitSpecies as $sp) {
            if(strtolower($sp[$field]) == trim($speciesName)) {
                return $sp;
            }
        }
    }
    
    public function formData() {

        $stateCode = 910800000000000000;

        if(!$this->request) {
            $this->setFlash(['ERROR! Please select form to sync', 'danger']);
			$this->redirect(array('AppSyncMap', 'sync'));
        }

        $this->utilityModel->setStateCode($stateCode);
        $regions = $this->utilityModel->getForestHierarchy();

        $hierarchyData = array();
        if($regions !== false) {
            if(isset($regions['hierarchy']) && sizeof($regions['hierarchy']) > 0) {
                $regions['hierarchy'] = array_reverse($regions['hierarchy']);

                foreach($regions['hierarchy'] as $type) {

                    $order = $type['h_order'];
                    $name = strtolower($type['name']);

                    $hierarchyData[$name] = array();
                    if(isset($regions['data']) && sizeof($regions['data']) > 0) {
                        foreach($regions['data'] as $data) {
                            $id = $data['h'.$order.'_id'];
                            $regionArray = array(
                                'id' => $id,
                                'name' => $data['h'.$order.'_name'],
                            );

                            if(isset($data['h'.($order-1).'_id'])) {
                                $regionArray['parent_id'] = $data['h'.($order-1).'_id'];
                                $regionArray['parent_name'] = $data['h'.($order-1).'_name'];
                            }
                            
                            if($name == 'circle') {
                                $regionArray['parent_id'] = $stateCode;
                            }

                            $hierarchyData[$name][$id] = $regionArray;
                        }
                    }
                }
            }
        }

        $formId = $this->request['form'];

        $species = $this->formModel->getSpeciesByState($stateCode);

        $habitSpecies = array();
        if($species !== false) { 
            foreach($species as $sp) {
                $habitId = $sp['habit_id'];
                $habitSpecies[$habitId][] = $sp;
            }
        }

        $formDetails = $this->formModel->getFormDetailsById($this->request['form']);

        $typeDetails = $this->tableTypes[$formDetails['app_table']];

        if(isset($typeDetails['skip'])) {
            $this->skipFields = array_merge($this->skipFields, $typeDetails['skip']);
        } 
        
        $this->model->setCoreTable($formDetails['app_table']);
        $formData = $this->model->getCoreTable($this->session['user']['user_id']);

        if($formData === false) {
            $this->setFlash(['ERROR! No data available in selected form', 'danger']);
			$this->redirect(array('AppSyncMap', 'sync'));
        }

        $attributes = $this->formModel->getFormAttributes($formId);

        if($attributes === false) {
            $this->setFlash(['ERROR! No attributes available for selected form', 'danger']);
			$this->redirect(array('AppSyncMap', 'sync'));
        }

        $appAttributes = $groups = $checkBoxes = array();
        foreach($attributes as $attr) {

            $appField = !empty($attr['group_id']) ? $attr['app_field_rep'] : $attr['field_name'];
            $appAttributes[$appField] = $attr;

            if($attr['ui_element'] == 'checkbox' && !empty($appField)) {
                $checkBoxes[$appField] = $attr;
            }

            if(!empty($attr['group_id']) && !empty($attr['app_field_rep'])) {
                $groupId = $attr['group_id'];
                $groups[$groupId] = $attr;
            }
        }

        $formErrors = array();
        
        $decodeFormData = array();
        foreach($formData as $fd) {

            foreach($fd as $key => $value) {

                if(in_array($key, array_keys($hierarchyData)) && !empty($value)) {
                    $fd[$key] = $hierarchyData[$key][$value]['name'];
                    continue;
                }

                if((in_array($key, $this->skipFields) && !in_array($key, array_keys($this->faunaFields))) || empty($value) || in_array($value, array('No Data', 'Select Answer'))) continue;

                if(in_array($key, array_keys($this->faunaFields))) continue;

                if(!isset($appAttributes[$key])) die('Attribute not available in system: '.$key);

                $attr = $appAttributes[$key];

                $lowerValue = strtolower($value);

                if($attr['ui_element'] != 'checkbox') {

                    if($attr['ui_element'] == 'select') {

                        if(isset($this->mappings['attr'][$attr['attri_id']][$lowerValue]) && $attr['data_type'] != 'month') {
                            $fd[$key] = $value;
                        } else if(!isset($this->mappings['attr'][$attr['attri_id']][$lowerValue]) && $attr['data_type'] != 'month') {

                            if(!is_numeric($value)) {
                                $formErrors[] = 'Value not set for '.$attr['field_name'].': ('.$value.')';
                                continue;
                            }

                            $fd[$key] = isset($this->attrValues[$attr['attri_id']][$value]) ? $this->attrValues[$attr['attri_id']][$value] : '';
                        } else if($attr['data_type'] == 'month') {
                            $date = $value." 01 2000";
                            $fd[$key] = date("n", strtotime($date));
                        } else if(isset($this->mappings['species'][$attr['attri_id']])) {

                            $speciesData = explode(":", $this->mappings['species'][$attr['attr_id']]['values']);

                            if(!is_numeric($value)) {
                                $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $value);

                                $fd[$key] = $pValue['name'];
                            } else {
                                $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $value, 'id');

                                $fd[$key] = $pValue['name'];
                            }
                        } else {
                            $formErrors[] = 'Species value not available '.$attr['field_name'].': ('.$value.')';
                            continue;
                        }
                    } else {
                        $fd[$key] = $value;
                    }
                }

                $checkBoxValues = array();
                if($attr['ui_element'] == 'checkbox') {

                    $valueArray = explode(",", $value);

                    foreach($valueArray as $newValue) {

                        $newValue = trim($newValue);

                        $lowerNewValue = strtolower($newValue);

                        if(isset($this->mappings['attr'][$attr['attri_id']][$lowerNewValue]) && $attr['data_type'] != 'month') {
                            $checkBoxValues[] = $newValue;
                        } else if(!isset($this->mappings['attr'][$attr['attri_id']][$lowerNewValue]) && $attr['data_type'] != 'month') {

                            if(isset($this->mappings['species'][$attr['attri_id']])) {

                                $speciesData = explode(":", $this->mappings['species'][$attr['attri_id']]['values']);

                                if(!is_numeric($newValue)) { 
                                    $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $lowerNewValue);

                                    $checkBoxValues[] = $pValue['name'];
                                } else {
                                    $pValue = $this->searchSpecies($habitSpecies[$speciesData[2]], $lowerNewValue, 'id');

                                    $checkBoxValues[] = $pValue['name'];
                                }
                            } else {

                                if(!is_numeric($newValue)) {
                                    $formErrors[] = 'Value not set for '.$attr['field_name'].': ('.$newValue.')';
                                    continue;
                                }

                                $checkBoxValues[] = isset($this->attrValues[$attr['attri_id']][$newValue]) ? $this->attrValues[$attr['attri_id']][$newValue] : '';
                            }
                        } else if($attr['data_type'] == 'month') {
                            $date = $newValue." 01 2000";
                            $checkBoxValues[] = date("n", strtotime($date));
                        } else {
                            $checkBoxValues[] = $newValue;
                        }
                    }

                    $fd[$key] = implode(",", $checkBoxValues);
                }
            }

            $decodeFormData[] = $fd;
        } 

        $params = array(
            'title' => 'AppSync Form Data ('.$formDetails['description'].')',
            'type' => 'AppSyncMap',
            'data' => $decodeFormData,
            'buttons' => false
        );

        $this->view('odk-data.html',compact('params'));
	}
}