<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\DataVerificationModel;
use IFMT\App\Model\UtilityModel;
/**
* Home Controller
*/
class DataVerification extends App
{
	protected $utilityModel;
	protected $verificationModel;
	
	public function __construct()
	{
		parent::__construct();
		$this->utilityModel = new UtilityModel();
		$this->verificationModel = new DataVerificationModel();
	}

	public function verifyAdminBoundaries(){
		$params = array();
		$params['title'] = "Verify Forest Admin Boundaries";
		//$forestAdminHierarchy = json_encode($this->utilityModel->forestAdminHierarchy());
		$url = isset($_SERVER['HTTPS']) && @$_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://'.$_SERVER['SERVER_NAME'];
		$create_by = $_SESSION['user']['user_id'];
		$forestAdminHierarchy = $this->utilityModel->forestAdminHierarchy();
		$params['uploaded'] = 1;
		$params['uploadedText'] = "";
		foreach ($forestAdminHierarchy as $key => $value) {
			$uploadStatus = $this->verificationModel->boundaryUploadStatus($value['table_name']);
			if($uploadStatus[0]['rowcount'] == 0){
				$params['uploaded'] = 0;
				$params['uploadedText'] .= $value['name'].",";
			}
		}
		if($params['uploaded'] == 0){
			$params['uploadedText'] = rtrim($params['uploadedText'], ',');	
		} else{
			$params['verified'] = 0;
			$verificationStatus = $this->verificationModel->boundaryVerificationStatus();
			if($verificationStatus[0]['query'] == 1){
				$params['verified'] = 1;
			}	
		}
		$this->view('verify-admin-boundaries.html',compact('params','url','create_by'));
	}

	public function getLayerList(){
		echo json_encode($this->verificationModel->forestAdminLayerList());
	}

	public function submitVerification(){
		if(isset($this->request['q'])){
			$response = $this->verificationModel->submitVerification($this->request['q']);
			$status = $this->verificationModel->boundaryVerificationStatus();
			$response['verified'] = 0;
			//print_r($status);
			if($status[0]['query'] == 1){
				$updateStatus = $this->verificationModel->updateVerificationStatus();
				$response['verified'] = 1;
			}
			echo json_encode($response);
		} else{
			$response['responseType'] = "-2";
			$response['text'] = "Parameter(s) missing";
			echo json_encode($response);
		}
	}

	public function getGeoJSONData(){
		$forestAdminHierarchy = $this->utilityModel->forestAdminHierarchy();
		//var_dump($forestAdminHierarchy);
		foreach ($forestAdminHierarchy as $key => $value) {
			if(isset($this->request['id']) && @$this->request['id'] == $key){
				$result = $this->verificationModel->convertTableDataToGeoJSON($value['table_name']);
				//echo json_encode($forestAdminHierarchy);
				header('Content-Disposition: attachment; filename="result.geojson"');
				header('Content-Type: text/plain; charset=utf-8');
				header('Content-Length: ' . strlen(implode("\n", $result)));
            	header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
					header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
            	header("Pragma: no-cache");
            	flush();
            	ob_start();
				echo $result;
			}
		}
	}

	public function deleteBoundaries(){
		$forestAdminHierarchy = $this->utilityModel->forestAdminHierarchy();
		$data = array_reverse($forestAdminHierarchy);
		$nameList = "";
		foreach ($data as $key => $value) {
			$status = $this->verificationModel->deleteTableData($value['table_name']);
			if($status['responseType'] === "-1"){
				$response['responseType'] = "-1";
				$nameList .= $value['name'].",";
				echo $status['msg']; exit;
			}
		}
		if($nameList !== ""){
			$response['text'] = rtrim($nameList,',');	
		} else{
			$response['responseType'] = "1";
		}
		echo json_encode($response);
	}
}
	