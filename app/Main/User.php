<?php
namespace IFMT\App\Main;

use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\FormModel;
use IFMT\App\Model\UtilityModel;
use IFMT\App\Model\RareSpeciesModel;
/**
* Home Controller
*/
class User extends App
{
	protected $model;
	protected $query;
	protected $formModel;
	protected $utilityModel;
	protected $userLevels;
	protected $resHierarchy;
	protected $hierarchyAllocation = 'single';

	public function __construct()
	{
		parent::__construct(); 
		$this->model = new UserModel($this->dbHandler);
		$this->formModel = new FormModel($this->dbHandler); 
		$this->utilityModel = new UtilityModel($this->dbHandler);
		$this->rareSpeciesModel = new RareSpeciesModel($this->dbHandler);
		$this->userLevels = array(
			16 => array('level' => array(3), 'state' => isset($this->session['user']) ? $this->session['user']['state_code'] : 0),
			3 => array('level' => array(15))
		);

		$this->resHierarchy = 3;
	}

	public function login() {
		$title = "Login";
		if(!empty($this->request))
		{
			$response = $this->model->checkLoginByUserID($this->request);
			extract($response);
			if($status !== false)
			{
				$this->setFlash($msg);
				$this->redirect(array('Home','index'));
			}else{
				$this->setFlash($msg);
				$this->view('login.html',compact('title'));
			}
		} else {
			$this->view('login.html',compact('title'));
		}
	}
	public function forgot() {
		if(!empty($this->request))
		{
			$title = "Forgot Password";
			$this->model->updatePassword($this->request);
			$this->view('forgot.html',compact('title'));	
		}
		$title = "Forgot Password";
		$this->view('forgot.html',compact('title'));
	}
	public function account() {
		header('Location: /User/getUpdateUser?id='.$this->session['user']['user_id']);
		exit;
	}
// Added Get User for range
	public function appLogin() {

		header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Cache-Control,Content-Language,Expires,Last-Modified,Pragma,Content-Type,X-Requested-With,Accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers");
        header("Access-Control-Allow-Credentials: false");
        header("Access-Control-Allow-Methods: GET,POST,HEAD,OPTIONS,PUT");

		error_log(json_encode($this->request));
		
		if(!empty($this->request))
		{
			$response = $this->model->checkLoginByUserID($this->request);
			extract($response);
			if($status !== false)
			{
				if(empty($this->session['user']['state_code'])) {
					$msg = array('Warning! state is not assigned to user. Please report to DFO.', 'failure');
					$this->setFlash($msg, true);
				}

				$filters = array(
					'where' => array('u.user_id' => $this->session['user']['create_by']),
					'resultset' => 'row'
				);
		
				$userDetails = $this->model->getUsersByFilter($filters);

				array_push($msg, array('user' => $this->session['user']));

				$stateDistrict = $this->utilityModel->getStateDistrict($this->session['user']['state_code']);

				$assignedSD = array();
				if($stateDistrict !== false) {
					foreach($stateDistrict as $sd) {
						$stateCode = $sd['state_code'];
						$assignedSD['state'][$stateCode] = array(
							'id' => $sd['state_code'],
							'name' => $sd['state_name']
						);

						$districtCode = $sd['district_code'];
						$assignedSD['district'][$districtCode] = array(
							'id' => $sd['district_code'],
							'name' => $sd['district_name'],
							'parent_id' => $stateCode
						);
					}
				}

				if(sizeof($assignedSD) > 0) {
					foreach($assignedSD as $type => $data) {
						$msg[2][$type] = array_values($data);
					}
				}

				$userAllocation = $this->model->getUserAllocation($this->session['user']['user_id']);
				$assignedDivision = 0;
				if($userAllocation !== false) {

					$stateId = $this->session['user']['state_code'];

					$circle = $this->utilityModel->getCircleByDFO($userDetails['create_by']);

					$this->utilityModel->setStateCode($stateId);

					$regions = $this->utilityModel->getForestHierarchy($userAllocation['region_type'], explode(",", $userAllocation['region_value']));

					$assignedRegions = array();
					if($regions !== false) {
						if(isset($regions['hierarchy']) && sizeof($regions['hierarchy']) > 0) {
							$regions['hierarchy'] = array_reverse($regions['hierarchy']);

							$hierarchy = array();
							foreach($regions['hierarchy'] as $type) {

								$order = $type['h_order'];

								if($order == 6) continue;
								
								$hArray = array(
									'id' => $type['id'],
									'name' => ($type['name'] == 'Block') ? 'Beat' : $type['name'],
									'h_order' => $type['h_order'],
									'table_name' => ($type['table_name'] == 'block_master') ? 'beat_master' : $type['table_name']
								);
								$hierarchy[] = $hArray;

								$name = strtolower($type['name']);

								$assignedRegions[$name] = array();
								if(isset($regions['data']) && sizeof($regions['data']) > 0) {
									foreach($regions['data'] as $data) {

										$id = $data['h'.$order.'_id'];

										if($name == 'division') $assignedDivision = $id;

										$regionArray = array(
											'id' => $id,
											'name' => $data['h'.$order.'_name'],
											'parent_id' => $data['h'.$order.'_parent']
										);

										if(isset($data['h'.($order-1).'_id'])) {
											$regionArray['parent_name'] = $data['h'.($order-1).'_name'];
										}

										if($order == 2) {
											$regionArray['parent_id'] = ($circle !== false) ? $circle['id'] : 1;
										}

										$assignedRegions[$name][$id] = $regionArray;
									}
								} else {
									$msg = array('Warning! No region data available. Please contact administrator.', 'failure');
									$this->setFlash($msg, true);
								}

								$hArray['parents'] = array_keys($assignedRegions[$name]);
							}

							$msg[2]['hierarchy'] = $hierarchy;

							if($circle !== false) {
								$msg[2]['circle'][] = array(
									'id' => $circle['id'],
									'name' => $circle['name'],
									'parent_id' => $stateCode
								);
							}

							$plots = $this->utilityModel->getAssignedPlots($hArray);

							if($plots !== false) {
								foreach($plots as $p) {
									$id = $p['id'];
									$assignedRegions['plot'][$id] = array(
										'id' => $id,
										'name' => $p['name'],
										'parent_id' => $p['parent_id'],
										'parent_name' => $p['parent_name']
									);
								}
							}
						}
					} else {
						$msg = array('Warning! Forest Hierarchy is missing for this state. Please report to DFO.', 'failure');
						$this->setFlash($msg, true);
					}
					
					if(sizeof($assignedRegions) > 0) {
						foreach($assignedRegions as $type => $data) {
							$type = ($type == 'block') ? 'beat' : $type;
							$msg[2][$type] = array_values($data);
						}
					}
				} else {
					$msg = array('Warning! No region assigned to user. Please report to DFO.', 'failure');
					$this->setFlash($msg, true);
				}
				
				$attributes = $this->formModel->getAllAttributeValues(); 

				$attrValues = array();
				if($attributes !== false) { 
					foreach($attributes as $attr) { 
						if(in_array($attr['attr_id'], array(6, 143))) continue;
						$attrArray = array(
							'attr_id' => $attr['attr_id'],
							'id' => $attr['id'],
							'value' => $attr['values'],
							'regional_value' => $attr['lang_values']
						);
						$attrValues[] = $attrArray;
					}	
				} else {
					$msg = array('Warning! Required data (possible values) is missing. Please report to DFO.', 'failure');
					$this->setFlash($msg, true);
				}
				$msg[2]['values'] = $attrValues;

				$habits = $this->formModel->getHabitTypes();
				if($habits !== false) {
					$msg[2]['habits'] = $habits;

					$speciesArray = array();
					$species = $this->formModel->getSpeciesByUser($this->session['user']['create_by']);
					if($species !== false) {
						foreach($species as $sp) {

							if($sp['sp_id'] == 9999) {
								$sp['name'] = "अन्य - Other";
							}

							$speciesArray[] = $sp;
						}

						$msg[2]['species'] = $speciesArray;
					} else {
						$msg = array('Warning! No species data available', 'failure');
						$this->setFlash($msg, true);
					}
				} else {
					$msg = array('Warning! No habit data available', 'failure');
					$this->setFlash($msg, true);
				}
				$msg[2]['range_users'] = $this->rareSpeciesModel->getUsers($userAllocation['region_value']);
				$data = $this->rareSpeciesModel->getList($userAllocation['region_value']);
				$arr = array();
				foreach ($data as $key => $value) {
					$arr[] = array('min_girth'=>$value['min_girth'],'name'=>$value['name']);
				}
				$msg[2]['rare_species'] = $data;
				$this->setFlash($msg, true);
			} else {
				$this->setFlash($msg, true);
			}
		} else {
			$msg = array('Warning! required parameters are missing', 'failure');
			$this->setFlash($msg, true);
		}
	}

	public function logout(){
		session_destroy();
		$this->setFlash(['You have successfully logged out.','success']);
		$this->redirect(array('Home','index'));
	}

	// User management Stared
	public function getUsers() {

		$filters = array(
			'where' => array('user_id' => $this->session['user']['user_id']),
			'resultset' => 'row'
		);

		$userRole = $this->model->getUserRoles($filters);

		$currentRole = 0;
		if($userRole !== false) {
			$currentRole = $userRole['role_id'];
		}

		$allUserRoles = $this->model->getUserRoles();

		$userRolesArray = array();
		if($allUserRoles !== false) {
			foreach($allUserRoles as $role) {
				$userId = $role['user_id'];
				$userRolesArray[$userId][] = $role['role_name'];
			}
		}

		$filters = array(
			'order' => array('name'),
			'not' => array('u.user_id' => $this->session['user']['user_id']),
			'where' => array('u.create_by' => $this->session['user']['user_id'])
		);

		if(isset($this->userLevels) && sizeof($this->userLevels[$currentRole]['level']) > 0) {
			$filters['in'] = $this->userLevels[$currentRole]['level'];
		}

		if(isset($this->userLevels[$currentRole]['state'])) {
			$filters['where'] = array('u.state_code' => $this->session['user']['state_code']);
		}

		$users = $this->model->getUsersByFilter($filters);

		$params = array(
			'title' => 'System Users',
			'data' => $users,
			'user-roles' => $userRolesArray,
			'roles-available' => isset($this->userLevels[$currentRole]) ? true : false
		);
		$this->view('users.html', compact('params'));
	}

	public function getNewUser() {
		
		$states = $this->utilityModel->getStates();

		$filters = array(
			'where' => array('user_id' => $this->session['user']['user_id']),
			'resultset' => 'row'
		);

		$userRole = $this->model->getUserRoles($filters);

		$currentRole = 0;
		if($userRole !== false) {
			$currentRole = $userRole['role_id'];
		}

		$filters = array(
			'order' => array('role_name')
		);

		if(isset($this->userLevels[$currentRole])) {
			$filters['in'] = $this->userLevels[$currentRole]['level'];
		}

		$roles = $this->model->getRolesByFilter($filters);

		$params = array(
			'title' => 'New System User',
			'roles' => $roles,
			'action' => 'postNewUser',
			'states' => $states,
			'current-role' => $currentRole,
			'state' => $this->session['user']['state_code'],
			'selection' => $this->hierarchyAllocation
		);
		$this->view('user-form.html', compact('params'));
	}

	public function loadStateHierarchy() {
		if(!isset($this->request['id']) || empty($this->request['id'])) {
			$response = array(
				'status' => 'false'
			);
			die(json_encode($response));
		}
		
		$this->utilityModel->setStateCode($this->request['id']);
		$hierarchyData = $this->utilityModel->forestAdminHierarchy('all', $this->resHierarchy);

		if($hierarchyData !== false) {
			$response = array(
				'status' => 'true',
				'data' => $hierarchyData
			);
			die(json_encode($response));
		} else {
			$response = array(
				'status' => 'false'
			);
			die(json_encode($response));
		}
	}

	public function loadRegionEntries() {
		$regionType = isset($this->request['type']) ? $this->request['type'] : '';
		$loadData = $this->request['load'];
		$userId = isset($this->request['user']) ? $this->request['user'] : '';

		$state = $this->request['state'];

		if(!isset($regionType)) {
			die('Please provide region type');
		}

		$id = (isset($this->request['id']) && is_numeric($this->request['id'])) ? $this->request['id'] : 0;

		$this->utilityModel->setStateCode($state);
		$hierarchy = $this->utilityModel->forestAdminHierarchy();

		if($hierarchy === false) {
			return false;
		} 

		$hierarchyData = array();
		foreach($hierarchy as $region) {
			$order = $region['h_order'];
			$hierarchyData[$order] = $region;
		}

		$firstHierarchy = min(array_keys($hierarchyData));

		$regionData = $this->utilityModel->getRegionByType($hierarchyData[$loadData], $id, $userId, $firstHierarchy, array(), $this->session['user']['user_id']);

		if($regionData === false) {
			die('No region available');
		}

		$selected = array();
		if(!empty($userId)) {
			$userAllocation = $this->model->getUserAllocation($userId);
		
			if($userAllocation !== false && !empty($userAllocation['region_value']) && $userAllocation['region_type'] == $regionType) {
				$selected = explode(",", $userAllocation['region_value']);
			}
		}

		$params = array(
			'regions' => $regionData,
			'selected' => $selected,
			'type' => $regionType,
			'load' => $loadData+1,
			'name' => $hierarchyData[$loadData]['name']
		);

		if(isset($this->request['data']) && $this->request['data'] == 'options') {
			$this->view('region-options.html', compact('params'));
        } else if($regionType == $loadData && $this->hierarchyAllocation == 'multiple') {
			$this->view('region-list.html', compact('params'));
		} else if($regionType == $loadData && $this->hierarchyAllocation == 'single') {
			$this->view('region-single.html', compact('params'));
		} else {
			$this->view('region-select.html', compact('params'));
		}
	}

	public function postNewUser() {
		$data = $this->request;
		unset($data['role']);
		unset($data['re_password']);
		unset($data['region_filter']);
		unset($data['region_type']);
		unset($data['region']);
		unset($data['select_region']);
		unset($data['state']);

		$data['mob_number'] = empty($data['mob_number']) ? 0 : $data['mob_number'];
		$data['create_by'] = $this->session['user']['user_id'];
		$data['timestamp'] = date('Y-m-d H:i:s');

		$userInsert = $this->model->setNewUser($data, array('user_id'));

		if($userInsert === false) {
			$this->setFlash(['ERROR! Not able to add this record: '.$this->request['user_id'], 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		if(isset($this->request['role']) && !empty($this->request['role'])) {
			$this->model->setUserRoles($this->request['user_id'], array($this->request['role']));
		}

		if(isset($this->request['region_type'])) {
			$selectedRegions = isset($this->request['select_region']) ? $this->request['select_region'] : array();
			$this->model->setUserAllocation($this->request['user_id'], $this->request['region_type'], $selectedRegions);
		}

		$this->setFlash([$this->request['name'].' has been successfully added', 'success']);
		return $this->redirect(array('User','getUsers'));
	}

	public function getUpdateUser() {
		
		if(!isset($this->request['id']) || empty($this->request['id'])) {
			$this->setFlash(['ERROR! Requested page is not available', 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		$filters = array(
			'where' => array('ur.user_id' => $this->session['user']['user_id']),
			'resultset' => 'row'
		);

		$userRole = $this->model->getUserRoles($filters);

		$currentRole = array();
		if($userRole !== false) {
			$currentRole = $userRole['role_id'];
		}

		$filters = array(
			'where' => array('u.user_id' => $this->request['id']),
			'resultset' => 'row'
		);

		if(isset($this->userLevels[$currentRole]['state'])) {
			$filters['where']['u.state_code'] = $this->session['user']['state_code'];
		} else {
			if($this->session['user']['user_id'] != $this->request['id'])
			{
				$filters['where']['u.create_by'] = $this->session['user']['user_id'];
			}
		}

		$userDetails = $this->model->getUsersByFilter($filters);

		if($userDetails === false) {
			$this->setFlash(['ERROR! Provided record not found: '.$this->request['id'], 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		$roleFilters = array(
			'where' => array('ur.user_id' => $this->request['id']),
			'resultset' => 'resultset'
		);

		$userRoles = $this->model->getUserRoles($roleFilters);

		$userRolesArray = array();
		if($userRoles !== false) {
			foreach($userRoles as $uR) {
				$userRolesArray[] = $uR['role_id'];
			}
		}

		$userAllocation = $this->model->getUserAllocation($userDetails['user_id']);

		$filters = array(
			'order' => array('role_name')
		);

		if(isset($this->userLevels[$currentRole]['level'])) {
			$filters['in'] = $this->userLevels[$currentRole]['level'];
		}

		$roles = $this->model->getRolesByFilter($filters);

		$states = $this->utilityModel->getStates();

		$params = array(
			'user' => $this->request['id'],
			'title' => 'Update System User ('.$userDetails['name'].')',
			'data' => $userDetails,
			'roles' => $roles,
			'user-roles' => $userRolesArray,
			'action' => 'postUpdateUser',
			'states' => $states,
			'selected-state' => $userDetails['state_code'],
			'region-type' => $userAllocation['region_type'],
			'current-role' => $currentRole,
			'state' => $this->session['user']['state_code'],
			'sessionId' => $this->session['user']['user_id'],
		);
		$this->view('user-form.html', compact('params'));
	}

	public function loadUserAllocations() {
		$userId = $this->request['user'];

		if(!isset($userId) || empty($userId)) {
			die('Please provide user to get user allocations');
		}

		$state = $this->request['state'];

		$userAllocation = $this->model->getUserAllocation($userId);
		
		if($userAllocation === false || empty($userAllocation['region_value'])) {
			die('No region allocation available for provided user');
		}

		$this->utilityModel->setStateCode($state);
		$hierarchy = $this->utilityModel->forestAdminHierarchy();

		if($hierarchy === false) {
			return false;
		} 

		$hierarchyData = array();
		foreach($hierarchy as $region) {
			$order = $region['h_order'];
			$hierarchyData[$order] = $region;
		}

		$firstHierarchy = min(array_keys($hierarchyData));

		$regions = (!empty($userAllocation['region_value'])) ? explode(",", $userAllocation['region_value']) : array();

		if($this->hierarchyAllocation == 'multiple') {
			$allocatedRegions = $this->utilityModel->getRegionByType($hierarchyData[$userAllocation['region_type']], 0, $userId, $firstHierarchy, $regions, $this->session['user']['user_id']);
		} else {
			$allocatedRegions = $this->utilityModel->getRegionByType($hierarchyData[$userAllocation['region_type']], 0, $userId, $firstHierarchy, array(), $this->session['user']['user_id']);
		}

		$params = array(
			'regions' => $allocatedRegions,
			'selected' => $regions,
			'type' => $userAllocation['region_type']
		);

		if($this->hierarchyAllocation == 'multiple') {
			$this->view('user-region-list.html', compact('params'));
		} else {
			$this->view('user-region-single.html', compact('params'));
		}
	}

	public function postUpdateUser() {
		
		if(!isset($this->request['user_id']) || empty($this->request['user_id'])) {
			$this->setFlash(['ERROR! Requested page is not available', 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		$data = $this->request;
		unset($data['role']);
		unset($data['re_password']);
		unset($data['user_id']);
		unset($data['region_type']);
		unset($data['region_filter']);
		unset($data['region']);
		unset($data['select_region']);

		if(empty($data['password'])) {
			unset($data['password']);
		}
		$data['mob_number'] = empty($data['mob_number']) ? 0 : $data['mob_number'];
		
		$userUpdate = $this->model->setUpdateUser($data, array('user_id' => $this->request['user_id']));

		if($userUpdate === false) {
			$this->setFlash(['ERROR! Not able to update this record: '.$this->request['user_id'], 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		$roleFilters = array(
			'where' => array('ur.user_id' => $this->request['user_id']),
			'resultset' => 'resultset'
		);

		$userRoles = $this->model->getUserRoles($roleFilters);

		$userRolesArray = array();
		$newRoles = $oldRoles = array();
		if($userRoles !== false) {
			foreach($userRoles as $uR) {
				$roleId = $uR['role_id'];
				$userRolesArray[] = $roleId;

				if(isset($this->request['role']) && !empty($this->request['role'])) {
					if(!in_array($roleId, array($this->request['role']))) $oldRoles[] = $roleId;
				}
			}

			if(isset($this->request['role']) && !empty($this->request['role'])) {
				$role = $this->request['role'];
				if(!in_array($role, $userRolesArray)) $newRoles[] = $role;
			} else {
				$oldRoles = $userRolesArray;
			}
		} else {
			$newRoles[] = $this->request['role'];
		}

		if(sizeof($newRoles) > 0) {
			$this->model->setUserRoles($this->request['user_id'], $newRoles);
		}

		if(sizeof($oldRoles) > 0) {
			$this->model->deleteUserRoles($this->request['user_id'], $oldRoles);
		}

		if(isset($this->request['region_type'])) {
			$selectedRegions = isset($this->request['select_region']) ? $this->request['select_region'] : array();

			$userAllocation = $this->model->getUserAllocation($this->request['user_id']);

			if($userAllocation === false) {
				$this->model->setUserAllocation($this->request['user_id'], $this->request['region_type'], $selectedRegions);
			} else {
				$this->model->setUpdateUserAllocations($this->request['user_id'], $this->request['region_type'], $selectedRegions);
			}
		}

		$this->setFlash([$this->request['name'].' has been successfully updated', 'success']);
		return $this->redirect(array('User','getUsers'));
	}

	/** 
     * Check duplicate email address for provided user details.
     * User details are provided from user registration form in the system.
     */
	function checkUserEmail() {
		$email = $this->request['email'];
		if(!empty($email)) {
			$filters = array(
				'fields' => 'count',
				'resultset' => 'row',
				'where' => array('email' => $email)
			);

			$result = $this->model->getUsersByFilter($filters);
			if($result > 0) {
				$response = array(
					'status' => true,
					'msg' => "<strong>".$email."</strong> is already registered. Please provide different email address"
				);
			} else {
				$response = array(
					'status' => false
				);
			}

		} else {
			$response = array(
				'status' => false,
				'msg' => 'Error! email address is empty'
			);
		}
		die(json_encode($response));
	}
    
    /** 
     * Check duplicate user id for provided user details.
     * User details are provided from user registration form in the system.
     */
	function checkUserId() {
		$user_id = $this->request['id'];
		if(!empty($user_id)) {
			$filters = array(
				'fields' => 'count',
				'resultset' => 'row',
				'where' => array('u.user_id' => $user_id)
			);

			$result = $this->model->getUsersByFilter($filters);
			if($result > 0) {
				$response = array(
					'status' => true,
					'msg' => "<strong>".$user_id."</strong> is already registered. Please provide different user id"
				);
			} else {
				$response = array(
					'status' => false
				);
			}

		} else {
			$response = array(
				'status' => false,
				'msg' => 'Error! user id is empty'
			);
		}
		die(json_encode($response));
	}

	public function getResetPassword() {
		if(!isset($this->request['id']) || empty($this->request['id'])) {
			$this->setFlash(['ERROR! Requested page is not available', 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		$filters = array(
			'where' => array('user_id' => $this->request['id']),
			'resultset' => 'row'
		);

		$userDetails = $this->model->getUsersByFilter($filters);

		if($userDetails === false) {
			$this->setFlash(['ERROR! Provided record not found: '.$this->request['id'], 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		$params = array(
			'user' => $this->request['id'],
			'title' => 'Reset Password ('.$userDetails['name'].')'
		);
		$this->view('reset-password.html', compact('params'));
	}

	public function postResetPassword() {
		if(!isset($this->request['user_id']) || empty($this->request['user_id'])) {
			$this->setFlash(['ERROR! Requested page is not available', 'danger']);
			return $this->redirect(array('User','getUsers'));
		}

		if($this->request['password'] != $this->request['co_password']) {
			$this->setFlash(['ERROR! password and confirm password are not matching', 'danger']);
			return $this->redirect(array('User','getResetPassword'));
		}

		$resetPassword = $this->model->setResetPassword($this->request);

		if($resetPassword === false) {
			$this->setFlash(['System ERROR! Not able to reset password: '.$this->request['user_id'], 'danger']);
		} else {
			$this->setFlash(['User password has been successfully reset: '.$this->request['user_id'], 'success']);
		}

		return $this->redirect(array('User','getUsers'));
	}
	// User management ends

	// Role management starts
	public function getRoles() {

		$filters = array(
			'order' => array('role_name')
		);

		$roles = $this->model->getRolesByFilter($filters);

		$params['title'] = 'System Roles';
		$params['data'] = $roles;
		$this->view('roles.html', compact('params'));
	}

	public function getNewRole() {

		$params = array(
			'title' => 'New System Role',
			'action' => 'postNewRole'
		);
		$this->view('role-form.html', compact('params'));
	}

	public function getPrivileges() {
		$roleId = $this->request['id'];

		if(!isset($this->request['id']) || empty($this->request['id'])) {
			$this->setFlash(['ERROR! Requested page is not available', 'danger']);
			return $this->redirect(array('User','getRoles'));
		}

		$dirs = scandir($_ENV['CLASSURL']);
		array_shift($dirs);
		array_shift($dirs);
		$this->class = array();
		foreach($dirs as $val)
		{
			$this->class[] = str_replace(".php", "", $val);
		}
		
		if($roleId == 16) {
			$this->class[] = 'AppSyncMap';
			array_shift($this->class);
		}
		
		$filters = array(
			'where' => array('role_id' => $this->request['id']),
			'resultset' => 'row'
		);

		$role = $this->model->getRolesByFilter($filters);

		if($role === false) {
			$this->setFlash(['System ERROR! No role available: '.$this->request['id'], 'danger']);
			$this->redirect('User', 'getRoles');
		}
			
		$currentPolicy = $this->model->getRolePrivileges($roleId);
		$roleAccessPolicy = ($currentPolicy !== false) ? explode(",", $currentPolicy['accesspolicy']) :array();
		
		$classMethods = array();
		foreach($this->class as $value)
		{
		   	$absClass = "IFMT\App\Main\\".$value;
		   	$obj = new \ReflectionClass($absClass);
			$all_methods = $obj->getMethods();
			
		   	foreach($all_methods as $var) {
				if(stristr($var->name, '__construct')) continue;
				if(stristr($var->name, 'search')) continue;
				if(!stristr($var->class, $value)) continue;
			   	$classMethods[$value][] = (array) $var;
			}
		}

		$params = array(
			'title' => 'Role Privileges',
			'privileges' => $classMethods,
			'role' => $role,
			'selected' => $roleAccessPolicy
		);
		$this->view('privileges.html', compact('params'));
	}

	public function postPrivileges() {
		$roleId = $this->request['id'];

		if(!isset($this->request['id']) || empty($this->request['id'])) {
			$this->setFlash(['ERROR! Role is not provided', 'danger']);
			$this->redirect(array('User','getRoles'));
		}

		$save = $this->model->setRolePrivileges($roleId, $this->request['access']);

		if($save === false) {
			$this->setFlash(['ERROR! Privileges not saved', 'danger']);
		} else {
			$this->setFlash(['Privileges for selected role has been successfully saved', 'success']);
		}

		return $this->redirect(array('User','getRoles'));
	}

	public static function checkAccess() {

		$userModel = new UserModel();

		$publicPolicy = $userModel->getRolePrivileges(1);

		$roleAccessPolicy = explode(",", $publicPolicy['accesspolicy']);

		$requestUri = explode('?', $_SERVER['REQUEST_URI']);
		$uri = explode('/', $requestUri[0]);
		array_shift($uri);
		$accessMethod = implode(".", $uri); 
		
		if($accessMethod == "Stats.")
		{
			return true;
		}

		if(isset($_SESSION['user'])) {
			$currentPolicy = $userModel->getUserPrivileges($_SESSION['user']['user_id']);

			if($currentPolicy === false) {
				$currentPolicy = $publicPolicy;
			}

			$roleAccessPolicy = explode(",", $currentPolicy['accesspolicy']);
		}

		if(sizeof($roleAccessPolicy) > 0) {
			foreach($roleAccessPolicy as $access) {
				list($module, $method) = explode(".", $access);
				$roleAccessPolicy['modular'][$module][] = $method;
			}
		}
		
		if(!in_array($accessMethod, $roleAccessPolicy) && !empty($accessMethod)) {
			return false;
		}

		$_SESSION['access'] = $roleAccessPolicy;

		return true;
	}
}
