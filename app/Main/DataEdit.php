<?php
namespace IFMT\App\Main;
use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\UtilityModel;
use IFMT\App\Model\SurveyModel;
use IFMT\App\Model\DataEditModel;
use IFMT\App\Model\FormModel;
/**
* DataEdit Controller
*/
class DataEdit extends App
{
	private $userModel;
	private $utilityModel;
	private $surveyModel;
	private $html;
	private $heads;
	private $totalCount;
	private $filterFieldsList;
	private $formModel;

	public function __construct()
	{
		parent::__construct();
		$this->userModel = new UserModel;
		$this->utilityModel = new UtilityModel;
		$this->surveyModel = new SurveyModel;
		$this->dataEditModel = new DataEditModel;

		$this->formModel = new FormModel;

		$this->html = "";
		$this->heads = "";
		$this->filterFieldsList = array('Degradation');
	}

	public function index()
	{
		$userDetails = $this->userModel->getSessionUser();
		if(is_null($userDetails))
		{
			return $this->redirect(array('Home','index'));
		}
		$regionId = $this->dataEditModel->getUserDivision($_SESSION['user']['user_id']);
		$forms = $this->surveyModel->getFormsList();
		$regionHirearchy= $this->utilityModel->getRegions($regionId);
		return $this->view('dataEdit.html',compact('regionHirearchy','order','forms'));
	}

	public function listData() {

		$formId = $this->request['form'];

		$filePath = $this->publicDir."/files/form-fields.json";
		$fh = fopen($filePath, "r");
		$jsonFormat = fread($fh, filesize($filePath));
		$format = json_decode($jsonFormat, true);
		$format = $format[$formId];
		$formDetails = $this->formModel->getFormDetailsById($this->request['form']);

		$attributes = $this->formModel->getAllAttributeValues();
		$attrValues = array();
        if($attributes !== false) { 
            foreach($attributes as $attr) {
                $attrId = $attr['attr_id'];
                $attrValues[$attrId][] = $attr['values'];
            }
        }

		return $this->view('list-data.html', compact('format', 'attrValues', 'formId', 'formDetails'));
	}

	public function loadFormData() {
		// Identify which form 
		$formId = $this->request['form'];
		
		// Get all formats
		$filePath = $this->publicDir."/files/form-fields.json";
		$fh = fopen($filePath, "r");
		$jsonFormat = fread($fh, filesize($filePath));
		$format = json_decode($jsonFormat, true);

		// Table display format
		$format = $format[$formId];

		// Get all the attributes used in the form based on formId
		$attributes = $this->formModel->getFormAttributes($formId);
		
		// Save attributes with there id and name 
		$attrIds = $onlyIds = array();
		if($attributes !== false) { 
            foreach($attributes as $attr) {
				$attrId = $attr['attri_id'];
				$attrName = $attr['name'];
				$attrIds[$attrName] = $attr;
				$attrIds[$attrId] = $attr;
				$onlyIds[] = $attrId;
            }
        }
		
		// Get all the attributes values in the form based on Ids
        $attributes = $this->formModel->getAllAttributeValues($onlyIds);

        // Save attributes values in an array 
        $attrValues = array();
        if($attributes !== false) { 
            foreach($attributes as $attr) {
				$attrId = $attr['attr_id'];
				$valueId = $attr['id'];
				$attrValues[$attrId][$valueId] = $attr['values'];
				$attrIds[] = $attrId;
            }
		}
		
		// Store all fields to be used from format
		// i is set to 3 to skip fixed rows
		$allFields = array();
		$i = 3;
		foreach($format as $fields) {
			foreach($fields as $key => $attr) {
				if($key == 'group') {
					continue;
				}

				$allFields[$i++] = $attr;
			}
		}

		// Identify the fxed columns
		$columns = array( '', 'id', 'main.is_approved');
		// Identify the remaning columns
		$mainColumns = array('create_date', 'state_name', 'division_name', 'range_name', 'block_name', 'compartment_name', 'plot_name');

		/**
		 * When sorting is called
		 */
		$sOrder = array();
		if ( isset( $this->request['order'][0]['column'] ) ) {
			$i = $this->request['order'][0]['column'];
			if($this->request['columns'][$i]['orderable'] == 'true' && isset($columns[$i])) {
				$sOrder[] = "main.".$columns[ $i ]." ".($this->request['order'][0]['dir'] === 'asc' ? 'asc' : 'desc');
			}

			if($this->request['columns'][$i]['orderable'] == 'true' && isset($allFields[$i]) && in_array($allFields[$i], $mainColumns)) {
				$sOrder[] = $allFields[$i]." ".($this->request['order'][0]['dir'] === 'asc' ? 'asc' : 'desc');
			}
		}

		/**
		 * Filters when search based on value is called
		 */
		$likeArray = array();
		if(isset($this->request['columns'])) {
			foreach($this->request['columns'] as $c => $column) {
				if($column['searchable'] == true && !empty($column['search']['value'])) {
					$likeArray["main.".$columns[$c]] =  "%".$column['search']['value']."%";
				}
			}
		}
		/**
		 * Filters getting data count based on user and formId
		 */
		$filters = array(
			'resultset' => 'row',
			'fields' => 'count',
			'where' => array('main.f_id' => $formId, "u.create_by" => $_SESSION['user']['user_id'])
		);

		// if ( isset($this->request['search']['value']) ) {
		// 	$filters['search'] = $this->request['search']['value'];
		// }

		/* Total data set length */
		$formDataCount = $this->surveyModel->getSurveysByFilter($filters);

		$filters['like'] = $likeArray;
			
		/* Data set length after filtering */
		$formDataFiltered = $this->surveyModel->getSurveysByFilter($filters);
		
		/*
		* Output
		*/
		$output = array(
			"sEcho" => intval(@$this->request['sEcho']),
			"iTotalRecords" => $formDataCount,
			"iTotalDisplayRecords" => $formDataFiltered,
			"aaData" => array()
		);

		if($formDataFiltered > 0) {

			$filters = array(
				'resultset' => 'data',
				'where' => array('main.f_id' => $formId, "u.create_by" => $_SESSION['user']['user_id']),
				'like' => $likeArray
			);

			$filters['begin'] = isset($this->request['start']) ? $this->request['start'] : 0;
			$filters['perpage'] = isset($this->request['length']) && $this->request['length'] != -1 ? $this->request['length'] : 'all';

			if(sizeof($sOrder) > 0) {
				$filters['order'] = $sOrder;
			}

			$formData = $this->surveyModel->getSurveysByFilter($filters);

			$surveyIds = array();
			foreach ($formData as $mdata) {
				$surveyIds[] = $mdata['id'];
			}

			$attrFilters = array(
				'resultset' => 'data',
				'in' => array('fd.s_id' => $surveyIds),
			);

			$attrData = $this->surveyModel->getSurveyDataByFilter($attrFilters);

			$formAttrData = $groupData = $attrDetails = $formDataIds = $groupDataIds = $faunaArray = array();
			if($attrData !== false) {
				foreach($attrData as $d) {
					$surveyId = $d['s_id'];
					$attrName = $d['attr_name'];
					$attrId = $d['attr_id'];
					$seq = $d['seq_id'];
					$dValue = $d['value'];
					$formAttrData[$surveyId][$attrName][] = $dValue;
					$formAttrData[$surveyId][$attrId][$seq] = $dValue;

					$formDataIds[$surveyId][$attrName][$dValue] = $d;
					$formDataIds[$surveyId][$attrId][$dValue] = $d;

					$faunaArray[$surveyId][$attrId][$seq][$dValue] = $d;

					if($d['ui_element'] == 'radio') {
						$attrDetails[$attrName] = $attrName;
					}

					if(!empty($d['g_id'])) {
						$groupId = $d['g_id'];
						$groupData[$surveyId][$groupId][$seq][$attrName][] = $dValue;

						$groupDataIds[$surveyId][$groupId][$seq][$attrId][$dValue] = $d;
						$groupDataIds[$surveyId][$groupId][$seq][$attrName][$dValue] = $d;
					}
				}
			}

			$availableGroups = $typeArray = $habitArray = array();
			foreach($format as $fields) {
				foreach($fields as $key => $attr) {
					if($key == 'group') {
						$availableGroups[$attr['id']] = $attr['columns'];
					}

					if(isset($attr['type'])) {
						$name = $attr['name'];
						$typeArray[$name] = $attr;
					}

					if(isset($attr['habit'])) {
						$name = $attr['name'];
						$habitArray[$name] = array_flip($attr['habit']);
					}
				}
			}

			/*
			* SQL queries
			* Get data to display
			*/
			$d = @$this->request['start']+1;

			$nonEditable = array("Surveyor Designation", "Survey Date", "Latitude", "Longitude", "Altitude", "Accuracy", "State", "Division", "Range", "Block");

			foreach ($formData as $mdata) {

				$surveyRows = array();

				$surveyId = $mdata['id'];

				$row = array($d);

				foreach($columns as $column) {
					if(empty($column)) continue;

					$column = str_replace(array('main.', 'u.'), '', $column);

					$value = $mdata[$column];

					if($column == 'is_approved') {
						$value = $mdata[$column] == 't' ? '<div data-attr="'.$surveyId.'" class="discard badge badge-success">Included</div>' : '<div data-attr="'.$surveyId.'" class="discard badge badge-danger">Excluded</div>';
					}

					if($column == 'id') {
						$value ='<span class="survey-id" data-form="'.$formId.'">'.$value.'</span>';
					}

					$row[] = $value;
				}

				foreach($format as $fields) {
					foreach($fields as $key => $attr) {
						if($key == 'fetch_pv') {
							if(in_array($attr['id'], array_keys($attrValues))) {
								$attrId = $attr['id'];
								$attrUIElement = isset($attrIds[$attrId]) ? $attrIds[$attrId]['ui_element'] : '';
								$faId = isset($attrIds[$attrId]) ? $attrIds[$attrId]['fa_id'] : '';
								$groupId = isset($attrIds[$attrId]) ? $attrIds[$attrId]['group_id'] : '';

								foreach($attrValues[$attr['id']] as $valId => $value2) {
									$dataId = isset($formDataIds[$surveyId][$attr['id']][strtolower($value2)]) ? $formDataIds[$surveyId][$attr['id']][strtolower($value2)]['id'] : 0;
									$pValue = isset($formAttrData[$surveyId][$attr['id']]) && in_array(strtolower($value2), $formAttrData[$surveyId][$attr['id']]) ? 'Yes' : '-';
									$row[] = '<span data-attr="'.$attrId.'" data-pid="'.$valId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-id="'.$dataId.'" data-ui="pivot" data-label="'.$value2.'" data-value="'.$pValue.'">'.$pValue.'</span>';
								}
							}
						} else if($key == 'group' || $key == 'combine') {
							if($key == 'combine' && isset($attr['name'])) 
							{
								$row[$attr['name']] = '-';
							}
							if(isset($attr['columns'])) {
								foreach($attr['columns'] as $xKey => $column) {
									if($xKey === 'fetch_pv') {
										if(in_array($column['id'], array_keys($attrValues))) {
											foreach($attrValues[$column['id']] as $cKey => $value) {
												$rKey = $column['id'].'-'.$cKey;
												$pValue = isset($formAttrData[$surveyId][$column['id']]) && in_array(strtolower($value), $formAttrData[$surveyId][$column['id']]) ? 'Yes' : '-';
												$row[$rKey] = $pValue;
											}
										}
									} else {
										$rKey = ($key == 'combine') ? $column : $attr['id'].'-'.$column;
										$row[$rKey] = '-';
									}
								}
							}
						} else if($key == 'fetch_cb') {
							if(isset($attr['id'])) {
								$row[] = isset($formAttrData[$surveyId][$attr['id']]) ? implode(", ", array_values($formAttrData[$surveyId][$attr['id']])) : '-';;
							}
						} else if($key == 'fetch_fauna') {
							if(in_array($attr['id'], array_keys($attrValues))) {
								$seq = 1;
								foreach($attrValues[$attr['id']] as $value) {

									if(!isset($habitArray[$attr['name']][$value])) continue;

									$habitId = $habitArray[$attr['name']][$value];

									$faunaName = $attr['name'];
									$attrUIElement = isset($attrIds[$faunaName]) ? $attrIds[$faunaName]['ui_element'] : '';
									$faId = isset($attrIds[$faunaName]) ? $attrIds[$faunaName]['fa_id'] : '';
									$groupId = isset($attrIds[$faunaName]) ? $attrIds[$faunaName]['group_id'] : '';

									$faunaComments = $attr['comments'];
									$attrUIElement2 = isset($attrIds[$faunaComments]) ? $attrIds[$faunaComments]['ui_element'] : '';
									$faId2 = isset($attrIds[$faunaComments]) ? $attrIds[$faunaComments]['fa_id'] : '';
									$groupId2 = isset($attrIds[$faunaComments]) ? $attrIds[$faunaComments]['group_id'] : '';

									if(isset($formAttrData[$surveyId][$attr['id']]) && in_array($value, $formAttrData[$surveyId][$attr['id']])) {

										$speciesName = '-';
										if(isset($faunaArray[$surveyId][$faunaName][$seq])) {
											if(sizeof(array_keys($faunaArray[$surveyId][$faunaName][$seq])) > 2) {
												$speciesList = array_keys($faunaArray[$surveyId][$faunaName][$seq]);
												$speciesChunks = array_chunk($speciesList, 2);

												$speciesName = implode(", ", $speciesChunks[0]);
											} else {
												$speciesName = implode(", ", array_keys($faunaArray[$surveyId][$faunaName][$seq]));
											}
										}

										$repValue = isset($faunaArray[$surveyId][$faunaName][$seq]) ? implode("|", array_keys($faunaArray[$surveyId][$faunaName][$seq])) : '-';

										$row[] = '<span class="f-'.$surveyId.'-'.$faunaName.'-'.$groupId.'-'.$seq.'-0" data-attr="'.$faunaName.'" data-habit="'.$habitId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$value.'" data-value="'.$repValue.'" data-seq="'.$seq.'">'.$speciesName.'</span>';

										$comments = isset($formAttrData[$surveyId][$faunaComments][$seq]) ? $formAttrData[$surveyId][$faunaComments][$seq] : '-';

										$dataId = isset($faunaArray[$surveyId][$faunaComments][$seq]) ? $faunaArray[$surveyId][$faunaComments][$seq][$comments]['id'] : 0;

										$row[] = '<span class="f-'.$surveyId.'-'.$faunaComments.'-'.$groupId2.''.$seq.'-0" data-attr="'.$faunaComments.'" data-group="'.$groupId2.'" data-fid="'.$faId2.'" data-id="'.$dataId.'" data-ui="'.$attrUIElement2.'" data-label="'.$value.' Comments" data-value="'.$comments.'" data-seq="'.$seq.'">'.$comments.'</span>';
									} else {

										$faunaId = $attr['id'];
										$valuesId = array_flip($attrValues[$faunaId]);
										$faunaPId = $valuesId[$value];

										$ffaId = isset($attrIds[$faunaId]) ? $attrIds[$faunaId]['fa_id'] : '';

										$row[] = '<span class="f-'.$surveyId.'-'.$faunaName.'-'.$groupId.'-'.$seq.'-0" data-attr="'.$faunaName.'" data-habit="'.$habitId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$value.'" data-value="-" data-seq="'.$seq.'" data-fpid="'.$faunaPId.'" data-ffaid="'.$ffaId.'">-</span>';;
										$row[] = '<span class="f-'.$surveyId.'-'.$faunaComments.'-'.$groupId2.'-'.$seq.'-0" data-attr="'.$faunaComments.'" data-group="'.$groupId2.'" data-fid="'.$faId2.'" data-ui="'.$attrUIElement2.'" data-label="'.$value.' Comments" data-value="-" data-seq="'.$seq.'" data-fpid="'.$faunaPId.'" data-ffaid="'.$ffaId.'">-</span>';
									}

									$seq++;
								}
							}
						} else if(!is_array($attr)) {
							$attrId = isset($attrIds[$attr]) ? $attrIds[$attr]['attri_id'] : 0;
							$attrUIElement = isset($attrIds[$attr]) ? $attrIds[$attr]['ui_element'] : '';
							$dataType = isset($attrIds[$attr]) ? $attrIds[$attr]['data_type'] : '';
							$faId = isset($attrIds[$attr]) ? $attrIds[$attr]['fa_id'] : '';
							$groupId = isset($attrIds[$attr]) ? $attrIds[$attr]['group_id'] : '';

							$repValue = '-';
							if(isset($attrDetails) && in_array($attr, array_keys($attrDetails))) {
								$fieldValue = isset($formAttrData[$surveyId][$attr]) && $formAttrData[$surveyId][$attr][0] == 1 ? 'Yes' : 'No';
								$checkValue = isset($formAttrData[$surveyId][$attr]) ? $formAttrData[$surveyId][$attr][0] : 2;
							} else {

								$fieldValue = $checkValue = '-';
								if(isset($formAttrData[$surveyId][$attr])) {
									$totalValues = sizeof($formAttrData[$surveyId][$attr]);
									if($totalValues > 2) {
										$valuesList = array_values($formAttrData[$surveyId][$attr]);
										$dataChunks = array_chunk($valuesList, 2);

										$fieldValue = $checkValue = implode(", ", $dataChunks[0]).' <br /><small><a href="javascript:void(0)" class="more" title="Click to view more"><i class="fa fa-external-link"></i> &nbsp;All ('.$totalValues.')</a></small>';
									} else {
										$fieldValue = $checkValue = implode(", ", array_values($formAttrData[$surveyId][$attr]));
									}
								}

								$repValue = isset($formAttrData[$surveyId][$attr]) ? implode("|", array_values($formAttrData[$surveyId][$attr])) : '-';
								if(isset($mdata[$attr])) {
									$fieldValue = $checkValue = $mdata[$attr];
								}

								if(in_array($attr, array_keys($mdata))) {
									$attrId = $key;
									$pId = 0;
									$attrUIElement = 'master';
								}
							}

							if($attrUIElement != 'checkbox' && !in_array($key, $nonEditable)) { 
								$dataId = isset($formDataIds[$surveyId][$attr][$checkValue]['id']) ? $formDataIds[$surveyId][$attr][$checkValue]['id'] : 0;
								$pId = isset($formDataIds[$surveyId][$attr][$checkValue]['p_id']) ? $formDataIds[$surveyId][$attr][$checkValue]['p_id'] : '0';
								$row[] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.(empty($groupId) ? '0' : $groupId).'-0-'.(empty($pId) ? '0' : $pId).'" data-type="'.$dataType.'" data-attr="'.$attrId.'" data-pid="'.$pId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-id="'.$dataId.'" data-ui="'.$attrUIElement.'" data-label="'.$attr.'" data-value="'.$fieldValue.'">'.$fieldValue.'</span>';
							} else if($attrUIElement == 'checkbox' && !in_array($key, $nonEditable)) {
								$row[] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.(empty($groupId) ? '0' : $groupId).'-0-0" data-attr="'.$attrId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$attr.'" data-value="'.$repValue.'">'.$fieldValue.'</span>';
							} else {
								$row[] = '<span data-attr="">'.$fieldValue.'</span>';
							}
						} else {
							$row[] = '-';
						}
					}
				}

				$surveyRows[] = $row;
				$row = null;
				unset($row);

				$groupAvailable = false;
				foreach($availableGroups as $groupId => $gColumns) {

					if(isset($groupData[$surveyId][$groupId])) {

						$currentRows = $surveyRows;
						$surveyRows = null;
						unset($surveyRows);
						$surveyRows = array();

						foreach($groupData[$surveyId][$groupId] as $gSeq => $gData) { 

							foreach($currentRows as $i => $row) {

								$row[0] = $d;

								foreach($gColumns as $column) {

									$attrId = isset($attrIds[$column]) ? $attrIds[$column]['attri_id'] : 0;
									$attrUIElement = isset($attrIds[$column]) ? $attrIds[$column]['ui_element'] : '';
									$dataType = isset($attrIds[$column]) ? $attrIds[$column]['data_type'] : '';
									$faId = isset($attrIds[$column]) ? $attrIds[$column]['fa_id'] : '';

									$repValue = '-';
									if(isset($gData[$column])) {
										if(isset($attrDetails) && in_array($column, array_keys($attrDetails))) {
											$gValue = isset($formAttrData[$surveyId][$column]) && $formAttrData[$surveyId][$column][0] == 1 ? 'Yes' : 'No';
											$checkValue = isset($formAttrData[$surveyId][$column]) ? $formAttrData[$surveyId][$column][0] : 2;
										} else {

											$gValue = $checkValue = '-';
											if(isset($gData[$column])) {
												$totalValues = sizeof($gData[$column]);
												if($totalValues > 2) {
													$valuesList = $gData[$column];
													$dataChunks = array_chunk($valuesList, 2);

													$gValue = $checkValue = implode(", ", $dataChunks[0]).' <br /><small><a href="javascript:void(0)" class="more" title="Click to view more"><i class="fa fa-external-link"></i> &nbsp;All ('.$totalValues.')</a></small>';
												} else {
													$gValue = $checkValue = implode(", ", $gData[$column]);
												}
											}
											$repValue = implode("|", $gData[$column]);
										}

										if($attrUIElement != 'checkbox') {
											$dataId = isset($formDataIds[$surveyId][$column]) ? $formDataIds[$surveyId][$column][$checkValue]['id'] : 0;
											$pId = isset($formDataIds[$surveyId][$column]) ? $formDataIds[$surveyId][$column][$checkValue]['p_id'] : 0;
											$row[$groupId.'-'.$column] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-'.(empty($pId) ? '0' : $pId).'" data-type="'.$dataType.'" data-attr="'.$attrId.'" data-pid="'.$pId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-id="'.$dataId.'" data-ui="'.$attrUIElement.'" data-label="'.$column.'" data-value="'.$gValue.'" data-seq="'.$gSeq.'">'.$gValue.'</span>';
										} else {
											$row[$groupId.'-'.$column] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-0" data-attr="'.$attrId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$column.'" data-value="'.$repValue.'" data-seq="'.$gSeq.'">'.$gValue.'</span>';
										}
									} else {
										$row[$groupId.'-'.$column] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-0" data-type="'.$dataType.'" data-attr="'.$attrId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$column.'" data-seq="'.$gSeq.'">-</span>';
									}
								}

								$surveyRows[] = $row;
								$row = null;
								unset($row);

								if($i != 0) $d++;
								if($i > 0) $groupAvailable = true;
							}
							
						}

						$currentRows = null;
						unset($currentRows);
					}
				}

				$combineAvailable = false;
				if(sizeof($typeArray) > 0) {
					$currentRows = $surveyRows;
					$surveyRows = null;
					unset($surveyRows);
					$surveyRows = array();

					$i = 0;
					foreach($typeArray as $name => $typeData) {

						foreach($typeData['type'] as $groupId => $groupName) {

							if(isset($groupData[$surveyId][$groupId])) {

								foreach($groupData[$surveyId][$groupId] as $gSeq => $gData) {

									foreach($currentRows as $row) {

										$row[0] = $d;
										
										$row[$name] = '<span data-attr="">'.$groupName.'</span>';

										$habitId = isset($habitArray[$name]) ? $habitArray[$name][$groupName] : 0;

										if(in_array($groupName, array('Seedling', 'Sapling'))) {
											$habitId = $habitArray[$name]['Tree'];
										}
										
										foreach($typeData['columns'] as $tKey => $column) {

											if((!is_array($column) && in_array($column, $typeData[$groupName])) || in_array($tKey, $typeData[$groupName])) {

												if($tKey == 'fetch_pv') {
													if(isset($column['id']) && in_array($column['id'], array_keys($attrValues))) {

														$attrId = $column['id'];

														$faId = isset($attrIds[$attrId]) ? $attrIds[$attrId]['fa_id'] : '';

														foreach($attrValues[$attrId] as $cKey => $value) {
															$rKey = $attrId.'-'.$cKey;
															$dataId = isset($groupDataIds[$surveyId][$groupId][$gSeq][$attrId][strtolower($value)]) ? $groupDataIds[$surveyId][$groupId][$gSeq][$attrId][strtolower($value)]['id'] : 0;
															$pValue = isset($gData[$column['name']]) && in_array(strtolower($value), $gData[$column['name']]) ? 'Yes' : '-';
															$row[$rKey] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-'.$cKey.'" data-attr="'.$attrId.'" data-pid="'.$cKey.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-id="'.$dataId.'" data-ui="pivot" data-label="'.$value.'" data-value="'.$pValue.'" data-seq="'.$gSeq.'">'.$pValue.'</span>';
														}
													}
												} else if(isset($gData[$column])) {

													$attrId = isset($attrIds[$column]) ? $attrIds[$column]['attri_id'] : 0;
													$attrUIElement = isset($attrIds[$column]) ? $attrIds[$column]['ui_element'] : '';
													$dataType = isset($attrIds[$column]) ? $attrIds[$column]['data_type'] : '';
													$faId = isset($attrIds[$column]) ? $attrIds[$column]['fa_id'] : '';

													$repValue = '-';
													if(isset($attrDetails) && in_array($column, array_keys($attrDetails))) {
														$gValue = isset($formAttrData[$surveyId][$column]) && $formAttrData[$surveyId][$column][0] == 1 ? 'Yes' : 'No';
														$checkValue = isset($formAttrData[$surveyId][$column]) ? $formAttrData[$surveyId][$column][0] : 2;
													} else {
														$gValue = $checkValue = implode(", ", $gData[$column]);
														$repValue = implode("|", array_values($gData[$column]));
													}

													if($attrUIElement != 'checkbox') {
														$dataId = isset($groupDataIds[$surveyId][$groupId][$gSeq][$column]) ? $groupDataIds[$surveyId][$groupId][$gSeq][$column][$checkValue]['id'] : 0;
														$pId = isset($groupDataIds[$surveyId][$groupId][$gSeq][$column]) ? $groupDataIds[$surveyId][$groupId][$gSeq][$column][$checkValue]['p_id'] : 0;
														$row[$column] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-'.(empty($pId) ? '0' : $pId).'" data-type="'.$dataType.'" data-attr="'.$attrId.'" data-habit="'.$habitId.'" data-pid="'.$pId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-id="'.$dataId.'" data-ui="'.$attrUIElement.'" data-label="'.$column.'" data-value="'.$gValue.'" data-seq="'.$gSeq.'">'.$gValue.'</span>';
													} else {
														$row[$column] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-0" data-attr="'.$attrId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$column.'" data-value="'.$repValue.'" data-seq="'.$gSeq.'">'.$gValue.'</span>';
													}
													
												} else {

													$attrId = isset($attrIds[$column]) ? $attrIds[$column]['attri_id'] : 0;
													$attrUIElement = isset($attrIds[$column]) ? $attrIds[$column]['ui_element'] : '';
													$dataType = isset($attrIds[$column]) ? $attrIds[$column]['data_type'] : '';
													$faId = isset($attrIds[$column]) ? $attrIds[$column]['fa_id'] : '';

													$row[$column] = '<span class="f-'.$surveyId.'-'.$attrId.'-'.$groupId.'-'.$gSeq.'-0" data-type="'.$dataType.'" data-attr="'.$attrId.'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$attrUIElement.'" data-label="'.$column.'" data-seq="'.$gSeq.'">-</span>';
												}
											} else {
												if($tKey == 'fetch_pv') {
													if(isset($column['id']) && in_array($column['id'], array_keys($attrValues))) {

														$attrId = $column['id'];

														foreach($attrValues[$attrId] as $cKey => $value) {
															$rKey = $attrId.'-'.$cKey;
															$row[$rKey] = '<span data-attr="" title="Not editable">x</span>';
														}
													}
												} else {
													$row[$column] = '<span data-attr="" title="Not editable">x</span>';
												}
											}
										}

										$surveyRows[] = $row;
										$row = null;
										unset($row);

										if($i != 0) $d++;
										if($i > 0) $combineAvailable = true;

										$i++;
									}
									
								}
							}
						}
					}
				}

				if(!$groupAvailable && !$combineAvailable) {
					$d++;
				}

				foreach($surveyRows as $row) {
					$output['aaData'][] = array_values($row);
				}

				unset($surveyRows);
			}
		}

		die(json_encode( $output ));
	}

	public function updateFieldData() {

		$groupId = isset($this->request['group_id']) && !empty($this->request['group_id']) ? $this->request['group_id'] : NULL;
		$groupIdClass = isset($this->request['group_id']) && !empty($this->request['group_id']) ? $this->request['group_id'] : 0;
		$seqId = isset($this->request['seq']) && !empty($this->request['seq']) ? $this->request['seq'] : 0;
		$faId = $this->request['fa_id'];
		$surveyId = $this->request['survey_id'];

		$dataId = isset($this->request['id']) ? $this->request['id'] : 0;

		$refTable = 'f';

		$pId = $value = NULL;
		if($this->request['type'] == 'select' || $this->request['type'] == 'pivot') {
			$pId = $this->request['dataVal'];

			if(!empty($pId) && !isset($this->request['habit'])) {
				$valueData = $this->formModel->getPossibleValue($pId);
				$value = ($valueData === false) ? '' : strtolower($valueData['values']);
			} else if(isset($this->request['habit'])) {
				$species = $this->formModel->getSpeciesByUser($_SESSION['user']['user_id'], $this->request['habit'], $pId);
				$value = ($species === false) ? '' : $species[0]['local_name'].' - '.$species[0]['sc_name'];
			}
		} else {
			$value = $this->request['dataVal'];
		}

		$repValue = $value;

		if(isset($this->request['fpid'])) {
			$dataArray = array(
				'fa_id' => $this->request['ffaid'],
				's_id' => $surveyId,
				'g_id' => $groupId,
				'seq_id' => $seqId,
				'ref_table' => 'f',
				'p_id' => $this->request['fpid'],
				'value' => $this->request['label']
			);
			$result = $this->surveyModel->setFormData($dataArray);
		}

		if($this->request['action'] == 'add') {

			if($this->request['type'] == 'checkbox') {
				$data = array(
					's_id' => $surveyId, 
					'fa_id' => $faId
				);

				if(isset($this->request['group_id']) && !empty($this->request['group_id'])) {
					$data['g_id'] = $this->request['group_id'];
				}

				if(isset($this->request['seq']) && !empty($this->request['seq'])) {
					$data['seq_id'] = $this->request['seq'];
				}
				$this->surveyModel->deleteFormData($data);

				$options = array();
				if(isset($this->request['habit'])) {
					$species = $this->formModel->getSpeciesByUser($_SESSION['user']['user_id'], $this->request['habit']);

					if($species !== false) { 
						foreach($species as $sp) {
							$spId = $sp['id'];
							$options[$spId] = $sp['name'];
						}
					}
					$refTable = 't';
				} else {
					$possibleValues = $this->surveyModel->getPossibleValues($this->request['attr']);

					if($possibleValues !== false) { 
						foreach($possibleValues as $pv) {
							$pvId = $pv['id'];
							$options[$pvId] = $pv['values'];
						}
					}
				}

				$valueArray = array();
				if(isset($this->request['dataVal'])) {
					foreach($this->request['dataVal'] as $pId) {
						$dataArray = array(
							'fa_id' => $faId,
							's_id' => $surveyId,
							'g_id' => $groupId,
							'seq_id' => $seqId,
							'ref_table' => $refTable,
							'p_id' => $pId,
							'value' => isset($options[$pId]) ? $options[$pId] : ''
						);
						$result = $this->surveyModel->setFormData($dataArray);

						$valueArray[] = isset($options[$pId]) ? $options[$pId] : '';
					}
					$repValue = implode("|", $valueArray);
					$value = implode(", ", $valueArray);
				}


			} else {

				$dataArray = array(
					'fa_id' => $faId,
					's_id' => $surveyId,
					'g_id' => $groupId,
					'seq_id' => $seqId,
					'ref_table' => $refTable,
					'p_id' => $pId,
					'value' => $value
				);
				$result = $this->surveyModel->setFormData($dataArray);

				$dataId = $result;

				$value = $this->request['type'] == 'pivot' ? 'Yes' : $value;
				$repValue = $this->request['type'] == 'pivot' ? 'Yes' : $value;
			}
		} else {

			if($this->request['type'] == 'pivot') {
				$data = array('id' => $dataId);
				$result = $this->surveyModel->deleteFormData($data);
				$value = '-';
				$repValue = 'No';
				$dataId = 0;
			} else {
				$dataArray = array(
					':newValue' => $value,
					':id' => $dataId
				);

				if(!empty($pId)) {
					$dataArray[':pid'] = $pId;
				}

				if(isset($this->request['seq']) && !empty($this->request['seq'])) {
					$dataArray[':seq'] = $this->request['seq'];
				}

				$result = $this->surveyModel->updateSingleData($dataArray);
			}
		}

		if($result === false) {
			die('0');
		} else {

			$pIdClass = !empty($pId) ? $pId : 0;

			$classArray = array($surveyId, $this->request['attr'], $groupIdClass, $seqId, $pIdClass);

			$htmlAdd = !empty($pId) ? ' data-pid="'.$pId.'"' : '';
			$htmlAdd .= $dataId != 0 ? ' data-id="'.$dataId.'"' : '';
			$htmlAdd .= isset($this->request['seq']) ? ' data-seq="'.$this->request['seq'].'"' : '';
			$htmlAdd .= isset($this->request['habit']) ? ' data-habit="'.$this->request['habit'].'"' : '';

			$newValue = $value;
			if($refTable == 't') {
				$totalValues = sizeof(explode("|", $repValue));
				if($totalValues > 2) {
					$valuesList = explode("|", $repValue);
					$dataChunks = array_chunk($valuesList, 2);

					$newValue = implode(", ", $dataChunks[0]).' <br /><small><a href="javascript:void(0)" class="more" title="Click to view more"><i class="fa fa-external-link"></i> &nbsp;All ('.$totalValues.')</a></small>';
				}
			}

			$html = '<span class="f-'.implode("-", $classArray).'"'.$htmlAdd.' data-attr="'.$this->request['attr'].'" data-group="'.$groupId.'" data-fid="'.$faId.'" data-ui="'.$this->request['type'].'" data-label="'.$this->request['label'].'" data-value="'.$repValue.'">'.$newValue.'</span>';
			die($html);
		}
	}

	public function editField() {

		$attr = $this->request['attr'];
		$sId = $this->request['sId'];
		$fId = $this->request['fid'];
		$seqId = isset($this->request['seq']) ? $this->request['seq'] : 0;
		$gId = $this->request['group'];
		$pId = isset($this->request['pid']) ? $this->request['pid'] : 0;
		$type = $this->request['ui'];
		$label = $this->request['label'];

		$value = isset($this->request['value']) ? $this->request['value'] : '';

		$id = isset($this->request['id']) ? $this->request['id'] : 0;

		$action = (!empty($attr) && is_numeric($attr) && empty($id)) ? 'add' : 'edit';

		$html = "<form class='form-horizontal' id='saveEditOption'>";

		if(isset($this->request['fpid'])) {
			$html .= "<input type='hidden' name='fpid' value='".$this->request['fpid']."'/>";	
			$html .= "<input type='hidden' name='ffaid' value='".$this->request['ffaid']."'/>";	
		}

		$html .= "<input type='hidden' name='id' value='".$id."'/>";
		$html .= "<input type='hidden' name='survey_id' value='".$sId."'/>";
		$html .= "<input type='hidden' name='fa_id' value='".$fId."'/>";
		$html .= "<input type='hidden' name='p_id' value='".$pId."'/>";
		$html .= "<input type='hidden' name='group_id' value='".$gId."'/>";
		$html .= "<input type='hidden' name='action' value='".$action."'/>";
		$html .= "<input type='hidden' name='type' value='".$type."'/>";
		$html .= "<input type='hidden' name='attr' value='".$attr."'/>";
		$html .= "<input type='hidden' name='label' value='".$label."'/>";
		$html .= "<input type='hidden' name='seq' value='".$seqId."'/>";
		$html .= "<label>".$label."</label>";
		$name = "dataVal";
		if($type == 'select') {
			$attrValues = $this->formModel->getAttrValues($attr);

			if(sizeof($attrValues) == 1) {
				$speciesData = explode(":", $attrValues[0]['values']);

				if(is_numeric($speciesData[2]) && $speciesData[2] != '{habit_id}') {
					$habitId = $speciesData[2];
				} else {
					$habitId = $this->request['habit'];
				}

				$html .= "<input type='hidden' name='habit' value='".$habitId."'/>";

				$species = $this->formModel->getSpeciesByUser($_SESSION['user']['user_id'], $habitId);

				$options = array();
				if($species !== false) { 
					foreach($species as $sp) {
						$options[] = array(
							'id' => $sp['id'],
							'values' => $sp['name']
						);
					}
				}
			} else {
				$options = $attrValues;
			}

			$html .= "<select class='form-control' id='editOption' name='$name' required>";
			$html .= "<option value='' selected> [ Select any one ] </option>";
			foreach ($options as $value) {
				$selected = $pId == $value['id'] ? " selected" : "";
				$html .= "<option value='".$value['id']."' $selected>".$value['values']."</option>";
			}
			$html .= "</select>";
			$html .= '<div class="invalid-feedback">
					Please select any option
				</div>';
		} else if($type == 'checkbox') {

			$attrValues = $this->formModel->getAttrValues($attr);

			if(sizeof($attrValues) == 1) {
				$speciesData = explode(":", $attrValues[0]['values']);

				if(is_numeric($speciesData[2]) && $speciesData[2] != '{habit_id}') {
					$habitId = $speciesData[2];
				} else {
					$habitId = $this->request['habit'];
				}

				$html .= "<input type='hidden' name='habit' value='".$habitId."'/>";

				$species = $this->formModel->getSpeciesByUser($_SESSION['user']['user_id'], $habitId);

				$options = array();
				if($species !== false) { 
					foreach($species as $sp) {
						$options[] = array(
							'id' => $sp['id'],
							'values' => $sp['name']
						);
					}
				}
			} else {
				$options = $attrValues;
			}

			$selectedValues = explode("|", $value);

			$html .= "<select class='form-control' multiple id='editOption' name='dataVal[]' required>";
			foreach ($options as $key => $value) {
				$selected = in_array($value['values'], $selectedValues) || in_array(strtolower($value['values']), $selectedValues)  ? " selected" : "";
				$html .= "<option value='".$value['id']."' $selected>".$value['values']."</option>";
			}
			$html .= "</select>";
			$html .= '<div class="invalid-feedback">
					Please select atleast 1 option.
				</div>';
		} else if($type == 'text field') {

			$type = isset($this->request['type']) && $this->request['type'] == 'number' ? 'number' : 'text';

			$html .= '<div class="form-group">';
			$html .= "<input type='".$type."' class='form-control' name='dataVal' value='".$value."' required>";
			$html .= '<div class="invalid-feedback">
					This field is required.
				</div>';
			$html .= '</div>';
		} else if($type == 'radio') {
			$html .= "<select class='form-control' id='editOption' name='dataVal' required>";
			$sel2 = $sel1 = "";
			if($value == 1)
			{
				$sel1 = "selected";
			} else {
				$sel2 = "selected";
			}
			$html .= "<option value='1' $sel1>Yes</option>";
			$html .= "<option value='2' $sel2>No</option>";
			$html .= "</select>";
		}  else if($type == 'pivot') {
			$html .= "<select class='form-control' id='editOption' name='dataVal'>";
			$sel2 = $sel1 = "";
			if($value == 'Yes')
			{
				$sel1 = "selected";
			} else {
				$sel2 = "selected";
			}
			$html .= "<option value='".$pId."' $sel1>Yes</option>";
			$html .= "<option value='' $sel2>No</option>";
			$html .= "</select>";
		} else {
			$html = "<div>$type define type</div>";
		}
		$html .= "</form>";
		echo $html;
		exit;
	}

	public function getData()
	{
		$formId = $_POST['formId'];
		$ids = $_POST['ids'];
		$survey = $this->surveyModel->getFormDetails($ids, $formId);
		
		if($survey == null)
		{
			$null = true;
			$msg = "<thead><th>No Data Found!!</th></thead><tbody><td class='disbale-dbl'>Survey data is null.</td></tbody>";
			$tabs = "";
			return json_encode(array("html"=>$msg, "null"=>$null, "tabs" => ''));
		}
		$surveySorted = $this->cleanData($survey, $formId);
		$arr = $this->renderHtmlAlternative($surveySorted,$survey,$formId);
		return json_encode($arr);
	}

	public function discardData()
	{
		$sId = $_POST['surveyId'];
		$assign = $_POST['assign'];
		return $this->surveyModel->updateData($sId, $assign);
	}


	private function cleanData($survey, $formId)
	{
		$reOrder = array();
		// getFileformat Details
		$filePath = $this->publicDir."/files/format.json";
		$fh = fopen($filePath, "r");
		$jsonFormat = fread($fh, filesize($filePath));
		$format = json_decode($jsonFormat, true);
		$format = $format[$formId];
		$this->setHeads($format);
		if(!empty($survey))
		{
			foreach ($survey as $sId => $sData) 
			{
				foreach ($format as $mainHead => $subHead) 
				{
					$reOrder[$sId]['is_approved'] = $sData['is_approved'];
					if(is_array($subHead) && !empty($subHead))
					{
						foreach ($subHead as $key=>$value) 
						{
							$k = array_keys($value);
							$k = $k[0];
							$v = $value[$k];
							if($k == 'fetch_pv')
							{
								$fieldName = $v[0];
								$attrVal = $v[1];
								$valField = $v[2];
								$groupField = isset($v[3]) ? $v[3] : "";
								$getPossibleValues = $this->surveyModel->getPossibleValues($attrVal, false);
								$getPossibleValues = array_map(function($item){return $item['values'];}, $getPossibleValues);
								foreach ($getPossibleValues as $value) 
								{
									$reOrder[$sId][$mainHead][$fieldName][$value] = "--";
									if($groupField == "")
									{
										if(isset($sData['form'][$fieldName]) && !empty($sKey = preg_grep("/".strtolower($value)."/", $sData['form'][$fieldName])))
										{
											$sKey = array_values($sKey)[0];
											$sKey = explode("::", $sKey);
											array_shift($sKey);
											$sKey = implode("::", $sKey);
											$reOrder[$sId][$mainHead][$fieldName][$value] = "Yes"."::".$sKey;
										}else{
											$reOrder[$sId][$mainHead][$fieldName][$value] = "--";
										}
									} else {
										if(isset($sData['form'][$fieldName])):
											$sKey = $this->findTextMatch($value, $sData['form'][$fieldName], $groupField);
											if($sKey !== false)
											{
												$reOrder[$sId][$mainHead][$fieldName][$value] = $sData['form'][$fieldName][$sKey][$valField][0];
											}
										endif;
									}
								}								
							}else{
								if(is_array($v) && !empty($v))
								{
									$subKey = array_keys($v);
									$subKey = $subKey[0];
									if(isset($sData['form'][$subKey]))
									{
										foreach ($sData['form'][$subKey] as $cnt => $subData)
										{
											foreach ($v[$subKey] as $field) 
											{
												$toSearch = is_array($field) ? array_values($field)[0] : $field;
												$reOrder[$sId][$mainHead][$k][$cnt][$toSearch] = isset($subData[$toSearch]) ? $subData[$toSearch] : "";
											}
										}
									}else{
										$reOrder[$sId][$mainHead][$k] = "";
									}
								}else{
									if(isset($sData[$v]))
									{
										$reOrder[$sId][$mainHead][$k] = $sData[$v];
									} else if(isset($sData['form'][$v])) {
										$reOrder[$sId][$mainHead][$k] = count($sData['form'][$v]) == 1 ? isset($sData['form'][$v][0]) ? $sData['form'][$v][0] : $sData['form'][$v] : array_values($sData['form'][$v]);
									}else{
										$reOrder[$sId][$mainHead][$k] = null;
									}
								}
							}	
						}
					}else{
						$reOrder[$sId][$mainHead] = "";
					}	
				}
			}
		}
		return $reOrder;
	}

	private function setHeads($format)
	{
		$this->heads = "<thead>";		
		// Heads
		$this->heads .= "<tr>";
		$this->heads .= "<th></th>";
		$this->heads .= "<th></th>";
		$this->heads .= "<th></th>";
		$altI = 0;
		foreach ($format as $key => $value) 
		{
			$altI++;
			$cnt = count($value);
			if(count($value) != 1)
			{
				$cnt = count($value);
			}else{
				foreach ($value as $displayData) {
					foreach ($displayData as $displayName => $dbName) {
						if($displayName == 'fetch_pv')
						{
							$attrVal = $dbName[1];
							$val = $this->surveyModel->getPossibleValues($attrVal, true);
							$cnt = $val[0]['count'];
						}else{
							if(is_array($dbName))
							{
								$cnt = max(array_map('count', $dbName));	
							}else{
								$cnt = 1;
							}
						}
					}
				}
			}
			$altCls = ($altI%2 != 0) ? "alt" : "";
			$this->heads .= "<th colspan=".$cnt." class='$altCls' >$key</th>";
		}
		$this->heads .= "</tr>";
		//Sub Heads
		$this->heads .= "<tr>";
		$this->heads .= "<th>Sr. No.</th>";
		$this->heads .= "<th>Survey ID</th>";
		$this->heads .= "<th>Status</th>";
		$this->totalCount = 3;
		$altI = 0;
		foreach ($format as $mainKey => $subData) 
		{
			$altI++;
			$altCls = ($altI%2 != 0) ? "alt" : "";
			foreach ($subData as $subKey => $value) 
			{
				foreach ($value as $displayName => $dbName) 
				{
					$cls = strtolower(str_replace(" ", "-", $mainKey));
					if($displayName == 'fetch_pv')
					{
						$attrVal = $dbName[1];
						$getPossibleValues = $this->surveyModel->getPossibleValues($attrVal, false);
						foreach ($getPossibleValues as $value) {
							$this->heads .= "<th class='$cls $altCls'>".$value['values']."</th>";
							$this->totalCount++;
						}
					}else{
						if(!is_array($dbName))
						{
							$this->heads .= "<th class='$cls $altCls'>$displayName</th>";
							$this->totalCount++;
						}else{
							foreach ($dbName as $columns) 
							{
								foreach ($columns as $name) {
									$dispNm = is_array($name) ? array_keys($name)[0] : $name;
									$this->heads .= "<th class='$cls $altCls'>$dispNm</th>";
									$this->totalCount++;
								}
							}
						}
					}
				}
			}
		}
		//Sub Heads
		$this->heads .= "</tr>";
		$this->heads .= "</thead>";
	}

	private function renderHtmlAlternative($survey,$surveyOld,$formId)
	{
		$cnt = 1;
		$this->html = "";
		//------------------------------------------------
		$filePath = $this->publicDir."/files/format.json";
		$fh = fopen($filePath, "r");
		$jsonFormat = fread($fh, filesize($filePath));
		$format = json_decode($jsonFormat, true);
		$format = $format[$formId];
		$attrArray = $this->utilityModel->getAttrIdsFromName();
		$tabs = array();
		$filterFields = function($array){
			foreach ($this->filterFieldsList as $value) {
				if(in_array($value, $array))
				{
					unset($array[$value]);
				}
			}
			return $array;
		};
		if($survey != null)
		{
			$null = false;
			$this->html = $this->heads;
			foreach ($survey as $sId => $sData) 
			{
				$tmp = $surveyOld[$sId]['form'];
				foreach ($this->filterFieldsList as $value) {
					unset($tmp[$value]);
				}
				$maxRows = max(array_map('count', $tmp));
				for($i=0; $i<$maxRows; $i++)
				{
					$this->html .= "<tr> <td>$cnt</td>";
					$this->html .= "<td class='sId' data-fId='$formId'>$sId</td>";
					$text = 'Included';
					$cls = 'success';
					if($sData['is_approved'] == '')
					{
						$text = 'Excluded';
						$cls = 'danger';
					}
					$this->html .= "<td><div data-attr=$sId class='discard badge-$cls badge'>$text</div></td>";
					$altI = 0;
					foreach ($format as $mainKey => $subData) 
					{
						$altI++;
						$altCls = ($altI%2 != 0) ? "alt" : "";
						$tabs[] = $mainKey;
						foreach ($subData as $subKey => $value) 
						{
							foreach ($value as $displayName => $dbName) 
							{
								if($displayName == 'fetch_pv')
								{
									$fieldName = $dbName[0];
									$attrVal = $dbName[1];
									$valField = $dbName[2];
									$attr_id = "";
									$groupField = isset($dbName[3]) ? $dbName[3] : "";
									$getPossibleValues = $this->surveyModel->getPossibleValues($attrVal, false);
									$getPossibleValues = array_map(function($item){return $item['values'];}, $getPossibleValues);
									foreach ($getPossibleValues as $value) {
										if($sData[$mainKey][$fieldName][$value] != "")
										{
											$attr_val = explode("::", $sData[$mainKey][$fieldName][$value]);
											$attr_id = isset($attr_val[1]) ? $attr_val[1] : "";
											$seq_id = isset($attr_val[2]) ? $attr_val[2] : "";
											$g_id = isset($attr_val[3]) ? $attr_val[3] : "";
											$type = isset($attr_val[4]) ? $attr_val[4] : "";
											$pId = isset($attr_val[5]) ? $attr_val[5] : "";
											if($type == 'radio')
											{
												$txt = trim($attr_val[0])==1 ? "Yes" : "No";
												$this->html .= "<td class='$altCls' data-repeat=true data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>$txt</td>";	
											}else{
												$this->html .= "<td class='$altCls' data-repeat=true data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>".$attr_val[0]."</td>";		
											}
										}else{
											$this->html .= "<td class='$altCls' data-attr='".$attr_id."'>--</td>";
										}
									}
								}else{
									if(!is_array($dbName))
									{
										if(isset($sData[$mainKey][$displayName]) && !empty(isset($sData[$mainKey][$displayName])))
										{
											if(is_array($sData[$mainKey][$displayName]))
											{
												if(isset($sData[$mainKey][$displayName][$i]))
												{
													$attr_val = explode("::", $sData[$mainKey][$displayName][$i]);
													$attr_id = isset($attr_val[1]) ? $attr_val[1] : "";
													$seq_id = isset($attr_val[2]) ? $attr_val[2] : "";
													$g_id = isset($attr_val[3]) ? $attr_val[3] : "";
													$type = isset($attr_val[4]) ? $attr_val[4] : "";
													$pId = isset($attr_val[5]) ? $attr_val[5] : "";
													if($type == 'radio')
													{
														$txt = trim($attr_val[0])==1 ? "Yes" : "No";
														$this->html .= "<td class='$altCls' data-repeat=true data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>$txt</td>";
													}else if($type == 'checkbox') {
														$checkbox_values = array();
														foreach($sData[$mainKey][$displayName] as $row) {
															$attr_val = explode("::", $row);
															$checkbox_values[] = $attr_val[0];
														}

														$this->html .= "<td class='$altCls' data-repeat=false data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>".implode(", ", $checkbox_values)."</td>";
													} else {
														$this->html .= "<td class='$altCls' data-repeat=false data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>".$attr_val[0]."</td>";
													}
												}else{
													$this->html .= "<td class='$altCls' >N/A</td>";
												}
											}else{
												$attr_val = explode("::", $sData[$mainKey][$displayName]);
												$attr_id = isset($attr_val[1]) ? $attr_val[1] : "";
												$seq_id = isset($attr_val[2]) ? $attr_val[2] : "";
												$g_id = isset($attr_val[3]) ? $attr_val[3] : "";
												$type = isset($attr_val[4]) ? $attr_val[4] : "";
												$pId = isset($attr_val[5]) ? $attr_val[5] : "";
												if($type == 'radio')
												{
													$txt = trim($attr_val[0])==1 ? "Yes" : "No";
													$this->html .= "<td class='$altCls' data-repeat=true data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>$txt</td>";
												}else{
													$this->html .= "<td class='$altCls' data-repeat=false data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>".$attr_val[0]."</td>";
												}
											}	
										}else{
											$attr_id = array_search($format[$mainKey][$subKey][$displayName], $attrArray);
											$this->html .= "<td class='$altCls' data-repeat=false data-attr='$attr_id' data-seq_id='0' data-g_id=''>No Value</td>";
										}
									}else{
										foreach ($dbName as $columns) 
										{
											foreach ($columns as $colName) {
												$name = is_array($colName) ? array_values($colName)[0] : $colName;
												if(isset($sData[$mainKey][$displayName][$i][$name][0]) && count($sData[$mainKey][$displayName][$i][$name]) == 1 )
												{
													$attr_val = explode("::", $sData[$mainKey][$displayName][$i][$name][0]);
													$attr_id = isset($attr_val[1]) ? $attr_val[1] : "";
													$seq_id = isset($attr_val[2]) ? $attr_val[2] : "";
													$g_id = isset($attr_val[3]) ? $attr_val[3] : "";
													$type = isset($attr_val[4]) ? $attr_val[4] : "";
													$pId = isset($attr_val[5]) ? $attr_val[5] : "";
													if($type == 'radio')
													{
														$txt = trim($attr_val[0])==1 ? "Yes" : "No";
														$this->html .= "<td class='$altCls' data-repeat=true data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>$txt</td>";
													}else{
														$this->html .= "<td class='$altCls' data-repeat=false data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>".$attr_val[0]."</td>";
													}
												} else if (isset($sData[$mainKey][$displayName][$i][$name][0]) && count($sData[$mainKey][$displayName][$i][$name]) > 1 ) {
													// $this->html .= "<td class='$altCls'>Error data..</td>";
													$tmp = "";
													foreach ($sData[$mainKey][$displayName][$i][$name] as $multiValue) {
														$attr_val = explode("::", $multiValue);
														$attr_id = isset($attr_val[1]) ? $attr_val[1] : "";
														$seq_id = isset($attr_val[2]) ? $attr_val[2] : "";
														$g_id = isset($attr_val[3]) ? $attr_val[3] : "";
														$type = isset($attr_val[4]) ? $attr_val[4] : "";
														$pId = isset($attr_val[5]) ? $attr_val[5] : "";
														$tmp[] = $attr_val[0];
													}
													$this->html .= "<td class='$altCls' data-repeat=false data-attr='$attr_id' data-seq_id='$seq_id' data-g_id='$g_id' data-p_id='$pId'>".implode(",", $tmp)."</td>";
												} else {
													$this->html .='<td class="'.$altCls.' disbale-dbl" data-repeat=false data-attr="'.$attr_id.'">-</td>';
												}
											}
										}
									}
								}
							}
						}
					}
					$cnt++;
				}
			}
		}else{
			$null = true;
			$this->html .= "<thead><th>Error</th></thead><tbody><td>No Data Found</td></tbody>";
		}
		
		$tabs = array_unique($tabs);

		$tabsHtml = '<span class="badge badge-info">Categories</span>';
		$tabsHtml .= '<select multiple="" id="catId">';
		$tabsHtml .= "<option value='all'>All</option>";
		foreach ($tabs as $value) {
			$tabsHtml .= "<option value='".$value."'>".$value."</option>";
		}
		$tabsHtml .= '</select>';
		return array("html"=>$this->html, "null"=>$null, "tabs" => $tabsHtml);
	}

	public function editOptions()
	{
		$attr = $_POST['attr'];
		$sId = $_POST['sId'];
		$fId = $_POST['fId'];
		$seqId = $_POST['seqId'];
		$gId = $_POST['gId'];
		$pId = $_POST['pId'];
		list($options, $success) = $this->surveyModel->getOptions($attr, $sId, $fId);
		if($success)
		{
			list($currentValues, $success, $type, $faId) = $this->surveyModel->currentValues($attr, $sId, $fId, $seqId, $gId);
			list($newOptions, $newType, $isSpecies) = $this->surveyModel->checkRefTableOptions($faId, $sId, $pId);
			$options = is_null($newOptions) ? $options : $newOptions;
			$type = is_null($newType) ? $type : $newType;
			if($success)
			{
				$html = "<form class='form' id='saveEditOption' action='/DataEdit/updateData'>";
				$html .= "<input type='hidden' name='ids' value='$attr::$sId::$fId::$seqId::$gId::$faId'/>";
				$name = "dataVal";
				if($type == 'checkbox' || $type == 'select')
				{
					usort($options, function ($a, $b) {
						if(in_array(strtolower($a['values']), array('januray','february','march','april','may','june','july','august','september','october','november','december')))
						{
							$monthA = date_parse($a['values']);
						    $monthB = date_parse($b['values']);
						    return $monthA["month"] - $monthB["month"];	
						}else{
							return 1;
						}
					});
					$multiple = "";
					if($type == 'checkbox')
					{
						$multiple = "multiple=''";
						$name = "dataVal[]";
					}
					$html .= "<select class='form-control' $multiple id='editOption' name='$name'>";
					foreach ($options as $key => $value) {
						$selected = "";
						$tofind = $isSpecies ? $value['values'] : strtolower($value['values']);
						if($this->recursive_array_search($tofind, $currentValues) !== false)
						{
							$selected = "selected";
						}
						$html .= "<option value='".$value['id']."' $selected>".$value['values']."</option>";
					}
					$html .= "</select>";
				} else if($type == 'text field') {
					$html .= '<div class="form-group">';
 					$html .= "<input type='text' class='form-control' name='dataVal' value='".$currentValues[0]['value']."'>";
 					$html .= '</div>';
				} else if($type == 'radio') {
					$html .= "<select class='form-control' id='editOption' name='dataVal'>";
					$sel2 = $sel1 = "";
					if($currentValues[0]['value'] ==1)
					{
						$sel1 = "selected";
					}else{
						$sel2 = "selected";
					}
					$html .= "<option value='1' $sel1>Yes</option>";
					$html .= "<option value='2' $sel2>No</option>";
					$html .= "</select>";
				} else {
					$html = "<div>$type define type</div>";
				}
				$html .= "</form>";
				echo $html;
			}else{
				echo "<div class='alert-danger'>Internal error Occured.</div>";	
			}
		}else{
			echo "<div class='alert-danger'>Internal error Occured.</div>";
		}
		exit;
	}

	private function recursive_array_search($needle,$haystack) 
	{
		$needle = strtolower($needle);
		foreach($haystack as $key=>$value) 
	    {
	    	$value = !is_array($value) ? strtolower($value) : $value;
	        $current_key=$key;
	        if($needle===$value OR (is_array($value) && $this->recursive_array_search($needle,$value) !== false))
	        {
	            return $current_key;
	        }
	    }
	    return false;
	}

	private function findTextMatch($needle, $haystack, $groupField, $curr = null)
	{
		foreach ($haystack as $key => $value) 
		{
			if(strpos($value[$groupField][0], $needle) !== false)
			{
				return $key;
			}
		}
		return false;
	}

	public function updateData()
	{
		// check & filter $_POST
		$this->surveyModel->updateEditData($_POST);
	}

	public function downloadRaw()
	{
		$this->dataEditModel->downloadRawData();
	}
}