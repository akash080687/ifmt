<?php
namespace IFMT\App\Core;
/**
* Config class
* Standard File
*/
class Config
{
    public static function init()
    {
        return [

            'theme' => 'vali',

            'debug' => true,

            'database' => [

                'in_use' => 'pgsql_local',

                'mysql' => [
                    'driver' => 'mysql',
                    'host' => '127.0.0.1',
                    'port' => '3306',
                    'database' => 'forge',
                    'username' => 'forge',
                    'password' => '',
                    'unix_socket' => '',
                    'charset' => 'utf8mb4',
                    'collation' => 'utf8mb4_unicode_ci',
                    'prefix' => '',
                    'strict' => true,
                    'engine' => null,
                ],
                'pgsql_cloud' => [
                    'driver' => 'pgsql',
                    'host' => $_ENV['PG_HOST'],
                    'port' => $_ENV['PG_PORT'],
                    'database' => $_ENV['PG_DATABASE'],
                    'username' => $_ENV['PG_USERNAME'],
                    'password' => $_ENV['PG_PASSWORD'],
                    'charset' => 'utf8',
                    'prefix' => '',
                    'schema' => 'public',
                    'sslmode' => 'prefer',
                ],
                'pgsql_local' => [
                   'driver' => 'pgsql',
                    'host' => $_ENV['PGL_HOST'],
                    'port' => $_ENV['PGL_PORT'],
                    'database' => $_ENV['PGL_DATABASE'],
                    'username' => $_ENV['PGL_USERNAME'],
                    'password' => $_ENV['PGL_PASSWORD'],
                    'charset' => 'utf8',
                    'prefix' => '',
                    'schema' => 'public',
                    'sslmode' => 'prefer',
                ]
            ]

        ];
    }
}