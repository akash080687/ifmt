<?php
namespace IFMT\App\Core;
use IFMT\App\Core\App;
use IFMT\App\Model\UserModel;
/**
* Auth component for user/api authentication
* Standard File
*/
class Auth extends App
{
	private $token;
	private $user;

	public function __construct()
	{
		$this->token = null;
		$this->session = $_SESSION;
	}

	public function userExists(){
		if(isset($this->session['user']))
		{
			return true;
		}else{
			return false;
		}
	}

	public function authenticateApi(){
		// update $this->auth authenticateApi
	}

	public function checkTokenAccess(){
		// update $this->auth authenticateApi
	}

	public function checkAccess()
	{
		if(!$this->userExists() && $_SERVER['REQUEST_URI'] != '/Home/index')
		{
			$this->redirect(array('Home','index'));
		}
	}

	static public function destroyFlash()
	{
		unset($this->session['alerts']);	
	}

}