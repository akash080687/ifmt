<?php
namespace IFMT\App\Core;
use IFMT\App\Core\Config;
use IFMT\App\Core\Auth;
/**
* Standard File
*/
class App
{

	public $theme;
	
	protected $viewDir;

	public $session;

	protected $params;

	protected $controller;

	protected $method;

	protected $publicDir;

	public $debug;

	protected $uri;

	protected $protocol;

	protected $database;

	protected $dbHandler;

	protected $twig;

	protected $auth;

	public $request;

	protected $namespace = "IFMT\App\Main\\";

	public function __construct()
	{
		$this->publicDir = dirname(dirname(__DIR__))."/public";
		$this->viewDir = dirname(__DIR__)."/Views";
		$this->initConfig();

		ini_set('error_reporting', E_ALL);
        error_reporting(E_ALL);
        ini_set('log_errors', TRUE);
        ini_set('html_errors', FALSE);
        ini_set('error_log', $this->publicDir.'/errors/error.log');
        ini_set('display_errors', TRUE);
	}

	protected function initConfig()
	{
		$dotenv = new \Dotenv\Dotenv(dirname(dirname(__DIR__))."/public");
		$dotenv->load();
		$config = Config::init();
		$this->debug = $config['debug'];
		$this->theme = $config['theme'];
		$this->request = $_REQUEST;
		unset($this->request['path']);
		$this->database = $config['database'][$config['database']['in_use']];
		$this->database['in_use'] = $config['database']['in_use'];
		$loader = new \Twig_Loader_Filesystem($this->viewDir."/".$this->theme);
		$this->twig = new \Twig_Environment($loader);
		$filter = new \Twig_SimpleFunction('filemtime', 'filemtime');
		$filter1 = new \Twig_SimpleFunction('date', 'date');
		$this->twig->addFunction($filter);
		$this->twig->addFunction($filter1);
		$this->setConfig();
		$this->auth = new Auth();
		$this->registerTwigFunctions();
	}

	public function setConfig()
	{
		$this->setDebug();
		$uri = explode('/', $_SERVER['REQUEST_URI']);
		array_shift($uri);
		$this->uri = $uri;
		$this->setController();
		$this->setMethod();
		$this->setProtocol();
		$this->session = &$_SESSION;
	}

	public function registerTwigFunctions(){
		$this->twig->addFunction(new \Twig_SimpleFunction('json_encode_twig', function ($data) {
		    return json_encode($data);
		}));
	}

	public function setDebug()
	{
		if($this->debug)
		{
			error_reporting(E_ALL);
			ini_set('display_errors',1);
		}
	}

	public function setController()
	{
		if(empty(array_filter($this->uri)))
		{
			$this->controller = 'Home';
		}else{
			$this->controller = ucfirst($this->uri[0]);
		}
	}
		
	public function setMethod()
	{
		if(empty(array_filter($this->uri)) || (isset($this->uri[1]) && $this->uri[1] == ""))
		{
			$this->method = 'index';
		} else if (isset($this->uri[1])) {
			if(strpos($this->uri[1], "?") === false )
			{
					$this->method = $this->uri[1];
			}else{
					$this->method = substr($this->uri[1], 0, strpos($this->uri[1], "?"));
					$this->params = substr($this->uri[1], strpos($this->uri[1], "?"));
			}
		} else {
			$this->method = 'index';
		}
	}

	public function setProtocol()
	{
		// To-do
	}

	public function call()
	{
		if(
			class_exists('\IFMT\App\Main\User') && method_exists('\IFMT\App\Main\User', 'checkAccess')
			&& $this->controller != 'Stats'
		) {
			$access = \IFMT\App\Main\User::checkAccess();
			if($access === false) {
				$this->setFlash(['ERROR! Unauthorized Access', 'danger']);
				$this->redirect(array('Home','index'));
			}
		}
		
		$this->controller =  $this->namespace . $this->controller;
		$method = (string) $this->method;
		$obj = new $this->controller();
		echo $obj->$method();
	}

	public function view($viewFile, $param=array())
	{
		$viewFile = $this->controller."/".$viewFile;
		$param['auth'] = false;
		extract($param);
		
		// Set alerts messages for views
		if(isset($this->session['alerts']))
		{
			$param['alerts'] = $this->session['alerts'];
		}

		// Set user information for views
		if($this->auth->userExists())
		{
			$param['auth'] = true;
			$param['user'] = $this->session['user'];

			if(sizeof($this->session['access']) > 0) {
				$param['access'] = $this->session['access'];
			}
			if(class_exists('\IFMT\App\Model\FormModel') && method_exists('\IFMT\App\Model\FormModel', 'getAllForms')) {
				$formModel = new \IFMT\App\Model\FormModel;
				$forms = $formModel->getAllForms();
	
				if($forms !== false) {
					$param['forms'] = $forms;
				}
			}

			if(class_exists('\IFMT\App\Model\ReportModel') && method_exists('\IFMT\App\Model\ReportModel', 'getRanges')) {
				$reportModel = new \IFMT\App\Model\ReportModel;
				$ranges = $reportModel->getRanges();
				if($ranges != null) {
					$param['ranges'] = $ranges;
				}
			}
		}
		
		if(file_exists($this->viewDir."/vali/".$viewFile))
		{
			echo $this->twig->render($viewFile,$param);
			unset($this->session['alerts']);
		}else{
			echo $this->twig->render("404.html");
		}
	}
	
	public function url($path)
	{
		return $this->publicDir . "/" . $path;
	}
	
	public function redirect($params=array())
	{
		$host = $_SERVER['HTTP_HOST'];
		if(!empty($params))
		{
			$controller = $params[0];
			$view = $params[1];
		}else{
			$controller = 'Home';
			$view = 'index';
		}
		
		header("Location: http://$host/$controller/$view");
		exit;
	}

	public function setFlash($params, $json=false)
	{
		if($json)
		{
			header('Content-Type: application/json; charset=utf-8');
			$response = array(
				'msg' => $params[0],
				'status' => $params[1]
			);

			if(isset($params[2])) {
				$response['data'] = $params[2];
			}

			die(json_encode($response));
		} else{
			$this->session['alerts'][$params[1]]=$params[0];
		}
	}

	public function __destruct()
	{
		// remove alerst from session
		// unset($_SESSION['alerts']);
	}
}