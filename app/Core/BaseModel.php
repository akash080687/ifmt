<?php
namespace IFMT\App\Core;
use IFMT\App\Core\App;
use IFMT\App\Core\Database;
/**
* BaseModel class
* Standard File
*/
class BaseModel extends App
{
	protected $tableName;
	protected $primaryId;
	protected $orderBy;
	protected $dbHandler;

	function __construct()
	{
		parent::__construct();
	}

	public function runSql($sql)
	{
		$result = null;
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		
		return count($result) >= 1 ? $result : null;
	}
}