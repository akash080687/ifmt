<?php
namespace IFMT\App\Model;
use IFMT\App\Core\Config;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;

use IFMT\App\Model\UserModel;
/**
* User Model 
* Standard File
*/
class AppSyncModel extends BaseModel
{
	protected $coreTable;

	protected $types = array(
        'village' => array('table' => 'village_details', 'f_id' => 4, 'sub-table' => array('ntfp'), 'Yes' => 1, 'No' => 2, 'skip' => array('name')),
        'description' => array('table' => 'plot_description', 'f_id' => 2, 'sub-table' => array('plantation'), 'group' => array('id' => 3, 'fauna' => 58, 'description' => 123)),
        'enumeration' => array('table' => 'plot_enumeration', 'f_id' => 3, 'sub-table' => array('bamboo', 'sapling', 'seedling', 'tree', 'shrub', 'herb'), 'habit' => array('bamboo' => 6, 'sapling' => 3, 'seedling' => 4, 'tree' => 7, 'shrub' => 2, 'herb' => 1)),
        'approach' => array('table' => 'plot_approach', 'f_id' => 5, 'sub-table' => array('plant_species'), 'group' => array('id' => 5, 'fauna' => 20, 'description' => 93), 'Yes' => 1, 'No' => 2),
        'household' => array('table' => 'household', 'f_id' => 6, 'sub-table' => array('crop_damage', 'human_damage', 'livestock_damage', 'property_damage', 'forest_lands', 'private_lands', 'livestock_hh'), 'Yes' => 1, 'No' => 2)
    );

	public function __construct() {
		parent::__construct();
	}

	public function getMaxFormCounts() {

		$this->dbHandler = Database::connection($this->database);

		$userObj = new UserModel;

		$tableCount = array();
		foreach($this->types as $type) {
			$tableCount[] = "select um.create_by, ".$type['f_id']." as f_id, '".$type['table']."' as form_type, s.state, count(s.id) as total_rows 
							 from public.static_".$type['table']." as s 
							 inner join ".$userObj->tableName." as um 
								on um.user_id = s.user_id
							 where s.version = '2' and s.synced = 'f' 
							 group by um.create_by, s.state";
		}

		$query = "select x.* 
				from (".implode(" union ", $tableCount).") as x 
				order by x.total_rows desc limit 1";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getMaxFormCounts: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getSyncRecordCount($tableName, $date_created, $device_id) {
		$this->dbHandler = Database::connection($this->database);
		$query = "select count(id) as total_rows from static_".$tableName." where date_created = ? and device_id = ?";
        try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->bindParam(1, $date_created, \PDO::PARAM_STR);
			$stmt->bindParam(2, $device_id, \PDO::PARAM_STR);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			return $result['total_rows'];
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getSyncRecordCount: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}
	
	public function updateSyncRecord($tableName, $updateData, $whereArray, $return = '') {
		if(sizeof($updateData) == 0 || sizeof($whereArray) == 0) {
			return false;
		}

		$queryAdd = $updateValues = array();
		foreach($updateData as $key => $value) {
			$newKey = ':'.$key;
			$queryAdd[] = $key.' = '.$newKey;
			$updateValues[$newKey] = $value;
		}

		$whereAdd = array();
		foreach($whereArray as $key => $value) {
			$newKey = ':'.$key;
			$whereAdd[] = $key.' = '.$newKey;
			$updateValues[$newKey] = $value;
		}

		$returning = !empty($return) ? " returning ".$return : '';

		$query = "update static_".$tableName." set ".implode(", ", $queryAdd)." where ".implode(" and ", $whereAdd).$returning;
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($updateValues);

			if(!empty($return)) {
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
				$result = $stmt->fetch();
				return $result;
			}
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('updateSyncRecord: '.$error);
			return false;
		}
	}

	public function insertSyncRecord($tableName, $insertData, $return = '') {
		if(sizeof($insertData) == 0) {
			return false;
		}

		$queryAdd = $insertValues = array();
		foreach($insertData as $key => $value) {
			$newKey = ':'.$key;
			$queryAdd[$key] = $newKey;
			$insertValues[$newKey] = $value;
		}

		$returning = !empty($return) ? " returning ".$return : '';

		$query = "insert into static_".$tableName." (".implode(", ", array_keys($queryAdd)).") values (".implode(", ", array_values($queryAdd)).")".$returning;
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($insertValues);

			if(!empty($return)) {
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
				$result = $stmt->fetch();
				return $result;
			}
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('insertSyncRecord: '.$error);
			return false;
		}
	}

	public function deleteSyncRecords($tableName, $whereArray) {

		if(sizeof($whereArray) == 0) {
			return false;
		}

		$this->dbHandler = Database::connection($this->database);

		$whereAdd = $updateValues = array();
		foreach($whereArray as $key => $value) {
			$newKey = ':'.$key;
			$whereAdd[] = $key.' = '.$newKey;
			$updateValues[$newKey] = $value;
		}

		$query = "update static_".$tableName." set deleted = 't', delete_date = '".date('Y-m-d H:i:s')."' where ".implode(", ", $whereAdd);
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($updateValues);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('deleteSyncRecords: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}
	
	public function markSynced($tableName, $syncedRows) {

		$this->dbHandler = Database::connection($this->database);

		$bindArray = array();
		foreach($syncedRows as $key => $value) {
			$newKey = ':row'.$key;
			$bindArray[$newKey] = $value;
		}

		$query = "update static_".$tableName." set synced = 't', sync_date = '".date('Y-m-d H:i:s')."' where synced = 'f' and id in (".implode(", ", array_keys($bindArray)).")";
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('markSynced: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
    }

	public function setCoreTable($coreTable) {
		$this->coreTable = $coreTable;
	}

	public function getTableColumns() {

		$this->dbHandler = Database::connection($this->database); 

		$query = "select column_name from information_schema.columns where table_name = 'static_".$this->coreTable."' and table_schema = '".$this->database['schema']."' order by column_name";
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getTableColumns: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getCoreTable($userId, $selected = array(), $limit = 0) {

		$this->dbHandler = Database::connection($this->database);

		$userModel = new UserModel();

		$bindArray = array(':user' => $userId);
		$whereArray = array();
		if(isset($selected) && sizeof($selected) > 0) {
			foreach($selected as $key => $value) {
				$newKey = ':form'.$key;
				$bindArray[$newKey] = $value;
				$whereArray[] = $newKey;
			}
		}

		$whereStr = sizeof($whereArray) > 0 ? " and st.id in (".implode(", ", $whereArray).")" : '';

		$limitStr = ($limit != 0 && is_numeric($limit)) ? " limit ".$limit : "";

		$query = "select st.* 
				  from static_".$this->coreTable." as st  
				  inner join ".$userModel->tableName." as u 
				  	on u.user_id = st.user_id and u.create_by = :user 
				  where st.synced = 'f' and st.version::integer = 2".$whereStr.$limitStr;

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getCoreTable: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getGroupTable($type, $selected = array()) {

		$this->dbHandler = Database::connection($this->database);

		$bindArray = array();
		$whereArray = array();
		if(isset($selected) && sizeof($selected) > 0) {
			foreach($selected as $key => $value) {
				$newKey = ':form'.$key;
				$bindArray[$newKey] = $value;
				$whereArray[] = $newKey;
			}
		}

		$whereStr = sizeof($whereArray) > 0 ? " and main.id in (".implode(", ", $whereArray).")" : '';

		$typeTable = $this->types[$type]['table'];

		$query = "select g.* 
				  from static_".$this->coreTable." as g 
				  inner join static_".$typeTable." as main 
					  on g.".$type."_id = main.id and main.synced = 'f'
				  where g.deleted = 'f'".$whereStr;
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getGroupTable: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function begin() {
		$this->dbHandler = Database::connection($this->database);

		$query = 'begin;';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("begin: ".$error);
			return false;
		}
	}

	public function end() {
		$query = 'commit;';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$this->dbHandler = null;
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("end: ".$error);
			$this->rollback();
			return false;
		}
	}

	public function rollback() {
		$query = 'rollback;';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$this->dbHandler = null;
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("rollback: ".$error);
			return false;
		}
	}
}