<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* User Model 
* Standard File
*/
class DataVerificationModel extends BaseModel
{

	public function __construct()
	{
		parent::__construct();
		$this->stateCode = $_SESSION['user']['state_code'];
	}

	function forestAdminLayerList(){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select a.id, a.name, a.table_name as layerName, b.is_verified from forest_boundary_hierarchy a left join forest_boundary_verification b on b.id = a.id and b.create_by = '".$_SESSION['user']['user_id']."' where state_code = ". $this->stateCode . " order by h_order");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}

	function convertTableDataToGeoJSON($tableName){
		$this->dbHandler = Database::connection($this->database);
		//$statement = $this->dbHandler->prepare("select id, parent_id, name, ST_AsGeoJSON(geom) from $tableName where create_by = '".$_SESSION['user']['user_id']."'");
		$statement = $this->dbHandler->prepare("select 'FeatureCollection' as type, array_to_json(array_agg(f)) as features from (SELECT 'Feature' as type, ST_AsGeoJSON((lg.geom),15,0)::json as geometry, row_to_json((id, name)) as properties from $tableName as lg where create_by = '".$_SESSION['user']['user_id']."') as f ");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}

	function submitVerification($id){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("insert into forest_boundary_verification (id, create_by, is_verified) values( ?, ?, ?)");
		try{
			$statement->execute([$id, $_SESSION['user']['user_id'], true]);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result['responseType'] = '1';
			$result['id'] = $id;
		}catch(\PDOException $e){
			//$error['msg'] = $e->getMessage();
			$error['responseType'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);	
	}

	function boundaryUploadStatus($tableName){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select count(id) as rowcount from ".$tableName." where create_by = '".$_SESSION['user']['user_id']."'");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			//$error['msg'] = $e->getMessage();
			$error['responseType'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}

	function boundaryVerificationStatus(){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select count(a.sr_no) = count(b.id) as query from forest_boundary_hierarchy b left join forest_boundary_verification a on a.id = b.id and a.create_by = '".$_SESSION['user']['user_id']."' where b.state_code = ".$this->stateCode);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			//$error['msg'] = $e->getMessage();
			$error['responseType'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}

	function updateVerificationStatus(){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("update user_master set data_verified = true where user_id = '".$_SESSION['user']['user_id']."'");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result['responseType'] = '1';
		}catch(\PDOException $e){
			//$error['msg'] = $e->getMessage();
			$error['responseType'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);	
	}

	function deleteTableData($tableName){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("delete from ".$tableName." where create_by = '".$_SESSION['user']['user_id']."'");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			$result['responseType'] = "1";
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['responseType'] = "-1";
		}
		$deleteAllVerifications = $this->dbHandler->prepare("delete from forest_boundary_verification "." where create_by = '".$_SESSION['user']['user_id']."'");
		try{
			$deleteAllVerifications->execute();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['responseType'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);		
	}
}