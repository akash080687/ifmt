<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
use IFMT\App\Model\UtilityModel;
use IFMT\App\Model\UserModel;
use IFMT\App\Model\FormModel;
/**
* Survey Model 
*/
class SurveyModel extends BaseModel
{
	public $tableName;
	public $dataTable;

	private $userModel;
	private $utilityModel;
	private $formModel;	

	public function __construct()
	{
		parent::__construct();
		$this->tableName = 'survey_master';
		$this->dataTable = 'form_data';

		$this->utilityModel = new UtilityModel();
		$this->userModel = new UserModel();
		$this->formModel = new FormModel();
	}

	public function getSurveysByFilter($filters = array()) {
		$this->dbHandler = Database::connection($this->database);
		$bindArray = $whereArray = $inArray = array();
		if(isset($filters['where']) && is_array($filters['where'])) {
			foreach($filters['where'] as $key => $value) {
				$newKey = ':'.str_replace(array('main.', 'u.'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' = '.$newKey;
			}
		}

		if(isset($filters['not']) && is_array($filters['not'])) {
			foreach($filters['not'] as $key => $value) {
				$newKey = ':'.str_replace(array('main.', 'u.'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' != '.$newKey;
			}
		}

		if(isset($filters['in']) && is_array($filters['in'])) {
			foreach($filters['in'] as $key => $value) {
				$newKey = ':role'.$key;
				$bindArray[$newKey] = $value;
				$inArray[] = $newKey;
			}
		}

		$limitStr = (isset($filters['begin']) && isset($filters['perpage'])) ? " limit ".$filters['perpage']." offset ".$filters['begin'] : '';

		$inString = sizeof($inArray) > 0 ? " and u.role_id in (".implode(",", $inArray).")" : "";

		$whereStr = sizeof($whereArray) > 0 ? " and ".implode(" and ", $whereArray) : '';

		$orderBy = isset($filters['order']) ? " order by ".implode(", ", $filters['order']) : '';

		$fields = isset($filters['fields']) && $filters['fields'] == 'count' ? 
				'count(main.id) as total_rows' : 'main.is_approved, main.f_id, main.id, main.create_date, u.name, u.designation, 
				 u.mob_number, p.name as plot_name, c.name as compartment_name, b.name as block_name, r.name as range_name, d.name as division_name, s.name as state_name';

		$dataJoin = '';
		if(isset($filters['search']) && !empty($filters['search'])) {
			$dataJoin = "inner join (select s_id from form_data where lower(value) like :search group by s_id) as x 
			on x.s_id = main.id";
			$bindArray[':search'] = "%".$filters['search']."%";
		}

		$query = "select ".$fields." 
				  from ".$this->tableName." as main  
				  inner join ".$this->userModel->tableName." as u 
					  on u.user_id = main.create_by ".$dataJoin."
				  left join ".$this->utilityModel->plotTable." as p
						on p.id = main.plot_id 
				  left join ".$this->utilityModel->compartmentTable." as c
						on c.id = main.comp_id 
				  left join ".$this->utilityModel->blockTable." as b
						on b.id = main.b_id 
				  left join ".$this->utilityModel->rangeTable." as r 
						on r.id = b.parent_id  
				  left join ".$this->utilityModel->divisionTable." as d  
						on d.id = r.parent_id  
				  left join ".$this->utilityModel->stateTable." as s 
				  	 	on s.mc = d.parent_id 
				  where true ".$whereStr.$orderBy.$limitStr;
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);

			if(isset($filters['resultset']) && $filters['resultset'] == 'row') {
				$result = $stmt->fetch();
				if(isset($filters['fields']) && $filters['fields'] == 'count') {
					return $result['total_rows'];
				}
			} else {
				$result = $stmt->fetchAll();
			}
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getSurveysByFilter: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getSurveyDataByFilter($filters = array()) {
		$this->dbHandler = Database::connection($this->database);

		$bindArray = $whereArray = $inArray = array();
		if(isset($filters['where']) && is_array($filters['where'])) {
			foreach($filters['where'] as $key => $value) {
				$newKey = ':'.str_replace(array('main.', 'fd.'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' = '.$newKey;
			}
		}

		if(isset($filters['not']) && is_array($filters['not'])) {
			foreach($filters['not'] as $key => $value) {
				$newKey = ':'.str_replace(array('main.', 'fd.'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' != '.$newKey;
			}
		}

		if(isset($filters['in']) && is_array($filters['in'])) {
			foreach($filters['in'] as $field => $values) {
				$valuesArray = array();
				foreach($values as $key => $value) {
					$newKey = ':'.str_replace(array('main.', 'fd.'), '', $field).$key;
					$bindArray[$newKey] = $value;
					$valuesArray[] = $newKey;
				}
				$whereArray[] = $field." in (".implode(", ", $valuesArray).")";
			}
		}

		$whereStr = sizeof($whereArray) > 0 ? " and ".implode(" and ", $whereArray) : '';

		$orderBy = isset($filters['order']) ? " order by ".implode(", ", $filters['order']) : '';

		$fields = isset($filters['fields']) && $filters['fields'] == 'count' ? 'count(main.id) as total_rows' : 'fd.*, a.id as attr_id, a.name as attr_name, a.data_type, a.ui_element';

		$query = "select ".$fields." 
				  from ".$this->dataTable." as fd  
				  inner join ".$this->tableName." as main
					  on main.id = fd.s_id 
				  inner join ".$this->formModel->formAttrTable." as fa 
						on fa.fa_id = fd.fa_id 
				  inner join ".$this->formModel->attrTable." as a 
				  	on a.id = fa.attri_id 
				  where true ".$whereStr.$orderBy;
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);

			if(isset($filters['resultset']) && $filters['resultset'] == 'row') {
				$result = $stmt->fetch();
				if(isset($filters['fields']) && $filters['fields'] == 'count') {
					return $result['total_rows'];
				}
			} else {
				$result = $stmt->fetchAll();
			}
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getUsersByFilter: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getAllSurveyByFormId($formId, $dataFrom = 'odk') {
		$this->dbHandler = Database::connection($this->database);

		$refField = ($dataFrom == 'app') ? 'app_ref_id' : 'odk_uri_id';

		$query = "select * from ".$this->tableName." where f_id = ? and ".$refField." is not null";
		$stmt = $this->dbHandler->prepare($query);
		$stmt->bindParam(1, $formId, \PDO::PARAM_INT);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setSurveyODKForm($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("insert into ".$this->tableName." (f_id, device_id, odk_uri_id, create_by, create_date, b_id, comp_id, plot_id) VALUES (?,?,?,?,?,?,?,?)");
		$statement->bindParam(1, $data['f_id'], \PDO::PARAM_INT);
		$statement->bindParam(2, $data['device_id'], \PDO::PARAM_INT);
		$statement->bindParam(3, $data['odk_uri_id'], \PDO::PARAM_STR);
		$statement->bindParam(4, $data['create_by'], \PDO::PARAM_STR);
		$statement->bindParam(5, $data['create_date'], \PDO::PARAM_STR);
		$statement->bindParam(6, $data['b_id'], \PDO::PARAM_INT);
		$statement->bindParam(7, $data['comp_id'], \PDO::PARAM_INT);
		$statement->bindParam(8, $data['plot_id'], \PDO::PARAM_INT);
		try{
			$statement->execute();
			$surveyFormId = $this->dbHandler->lastInsertId();
			return $surveyFormId;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('setSurveyODKForm: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setSurveyAppSyncForm($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("insert into ".$this->tableName." (f_id, device_id, app_ref_id, create_by, create_date, b_id, comp_id, plot_id, timestamp) VALUES (?,?,?,?,?,?,?,?,?)");
		$statement->bindParam(1, $data['f_id'], \PDO::PARAM_INT);
		$statement->bindParam(2, $data['device_id'], \PDO::PARAM_INT);
		$statement->bindParam(3, $data['app_ref_id'], \PDO::PARAM_STR);
		$statement->bindParam(4, $data['create_by'], \PDO::PARAM_STR);
		$statement->bindParam(5, $data['create_date'], \PDO::PARAM_STR);
		$statement->bindParam(6, $data['b_id'], \PDO::PARAM_INT);
		$statement->bindParam(7, $data['comp_id'], \PDO::PARAM_INT);
		$statement->bindParam(8, $data['plot_id'], \PDO::PARAM_INT);
		$statement->bindParam(9, $data['timestamp'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$surveyFormId = $this->dbHandler->lastInsertId();
			return $surveyFormId;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('setSurveyAppSyncForm: '.$error);
			return false;
		}
	}

	public function setFormData($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("insert into ".$this->dataTable." (s_id, fa_id, value, g_id, seq_id, p_id, ref_table) VALUES (?,?,?,?,?,?, ?)");
		$statement->bindParam(1, $data['s_id'], \PDO::PARAM_INT);
		$statement->bindParam(2, $data['fa_id'], \PDO::PARAM_INT);
		$statement->bindParam(3, $data['value'], \PDO::PARAM_STR);
		$statement->bindParam(4, $data['g_id'], \PDO::PARAM_INT);
		$statement->bindParam(5, $data['seq_id'], \PDO::PARAM_INT);
		$statement->bindParam(6, $data['p_id'], \PDO::PARAM_INT);
		$statement->bindParam(7, $data['ref_table'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$formDataId = $this->dbHandler->lastInsertId();
			return $formDataId;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('setFormData: '.$error);
			return false;
		}
	}

	public function setCompartment($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("insert into ".$this->utilityModel->compartmentTable." (c_id, d_id, r_id, b_id, name, start_date, create_by) VALUES (?,?,?,?,?,?,?)");
		$statement->bindParam(1, $data['c_id'], \PDO::PARAM_INT);
		$statement->bindParam(2, $data['d_id'], \PDO::PARAM_INT);
		$statement->bindParam(3, $data['r_id'], \PDO::PARAM_INT);
		$statement->bindParam(4, $data['b_id'], \PDO::PARAM_INT);
		$statement->bindParam(5, $data['name'], \PDO::PARAM_STR);
		$statement->bindParam(6, $data['start_date'], \PDO::PARAM_STR);
		$statement->bindParam(7, $data['create_by'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$compartmentId = $this->dbHandler->lastInsertId();
			return $compartmentId;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('setCompartment: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setPlot($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("insert into ".$this->utilityModel->plotTable." (parent_id, name, create_by) VALUES (?,?,?)");
		$statement->bindParam(1, $data['parent_id'], \PDO::PARAM_INT);
		$statement->bindParam(2, $data['name'], \PDO::PARAM_STR);
		$statement->bindParam(3, $data['create_by'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$plotId = $this->dbHandler->lastInsertId();
			return $plotId;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('setPlot: '.$error);
			return false;
		}
	}

	public function getRanges() {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->utilityModel->rangeTable;
		$stmt = $this->dbHandler->prepare($query);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getRanges: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getBeatsByRange($rangeCode = 0) {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->utilityModel->beatTable;
		$stmt = $this->dbHandler->prepare($query);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getBeatsByRange: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getCompartmentsByRange($rangeCode = 0) {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->utilityModel->compartmentTable;
		$stmt = $this->dbHandler->prepare($query);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getCompartmentsByRange: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getPlotsByRange($rangeCode = 0) {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->utilityModel->plotTable;
		$stmt = $this->dbHandler->prepare($query);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getPlotsByRange: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getRangeByName() {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->utilityModel->rangeTable;
		$stmt = $this->dbHandler->prepare($query);
		$stmt->bindParam(1, $rangeName, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getRangeByName: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getFormDetails($ids, $formId)
	{
		$columnName = $this->getColumToSearchInSurvey($formId);
		$regionIds = $this->getAllSurveyRegions($ids, $formId);
		if($regionIds != null)
		{
			$regionIds = implode(",", $regionIds);
			$surveyList = $this->getSurveyIds($regionIds, $formId, $columnName);
			if($surveyList != null)
			{
				$sIds = implode(",", array_column($surveyList, 'id'));
				$survey = $this->getSurveyDetails($sIds, $formId);
				$surveyDetails = array();
				foreach ($survey as $key=>$value) 
				{
					// Basic Survey Information
					$surveyDetails[$value['s_id']]['name'] = $value['uname'];
					$surveyDetails[$value['s_id']]['mob_number'] = $value['mob_number'];
					$surveyDetails[$value['s_id']]['designation'] = $value['designation'];
					$surveyDetails[$value['s_id']]['plot'] = isset($value['plot']) ? $value['plot'] : "";
					$surveyDetails[$value['s_id']]['compartment'] = isset($value['compartment']) ? $value['compartment'] : "";
					$surveyDetails[$value['s_id']]['create_by'] = $value['create_by'];
					$surveyDetails[$value['s_id']]['create_date'] = $value['create_date'];
					// Basic Survey Information

					// Location
					$surveyDetails[$value['s_id']]['state'] = $value['state'];
					$surveyDetails[$value['s_id']]['district'] = $value['district'];
					// $surveyDetails[$value['s_id']]['circle'] = $value['circle'];
					$surveyDetails[$value['s_id']]['division'] = isset($value['division']) ? $value['division'] : "";
					$surveyDetails[$value['s_id']]['range'] = isset($value['range']) ? $value['range'] : "";
					$surveyDetails[$value['s_id']]['beat'] = isset($value['beat']) ? $value['beat'] : "";
					$surveyDetails[$value['s_id']]['block'] = isset($value['block']) ? $value['block'] : "";
					// Location ends

					// Form Specific Data
					$surveyDetails[$value['s_id']]['is_approved'] = $value['is_approved'] == "" ? false : true ;
					if($value['seq_id'] == 0) 
					{
						$surveyDetails[$value['s_id']]['form'][$value['name']][] = $value['value']."::".$value['attr_id']."::".$value['seq_id']."::".$value['g_id']."::".$value['type']."::".$value['p_id'];
					} else {
						$groupId = ($value['g_id'] == "") ?  $value['name'] : $value['group_name'];
						$seq = $value['seq_id'];
		 				$surveyDetails[$value['s_id']]['form'][$groupId][$seq-1][$value['name']][] = $value['value']."::".$value['attr_id']."::".$value['seq_id']."::".$value['g_id']."::".$value['type']."::".$value['p_id'];
					}
					// Form Specific Data
				}
				return $surveyDetails;
			}
			return null;
		}
		return null;
	}

	public function getSurveyDetails($sId, $formId)
	{
		$orders = $this->utilityModel->getOrders();
		$tabs = "plot_master as p, compartment_master as c,";
		$tabCondition = "s.comp_id = c.id and s.plot_id = p.id and";
		$tabColumns = 'c."name" as compartment, p."name" as plot,';
		$tabSubCondition = 'and s.comp_id = co.id';
		if(in_array($formId, array('6','4')))
		{
			unset($orders[array_search('plot', $orders)]);
			unset($orders[array_search('compartment', $orders)]);
			$tabs = "";
			$tabCondition = "";
			$tabColumns = "";
			$tabSubCondition = 'and s.b_id = bl.id';
		}
		if($formId == '5')
		{
			unset($orders[array_search('plot', $orders)]);
			$tabs = "compartment_master as c,";
			$tabCondition = "s.comp_id = c.id and";
			$tabColumns = 'c."name" as compartment,';
		}
		$sqlAlias = "";
		$sqlTables = "";
		$condition = "";
		foreach ($orders as $k=>$value) {
			$alias = $value[0].$value[1];
			$sqlAlias .=  $alias.'."name" as '.$value.', ';
			$sqlTables .= $value."_master as ".$alias.",";
			if(isset($orders[$k+1]))
			{
				$condition .= $alias.".id = ".$orders[$k+1][0].$orders[$k+1][1].".parent_id and ";
			}
		}
		$sqlAlias = rtrim($sqlAlias,", ");
		$sql = 'select query1.*, query2.* from
		(
			select base.*, grp.group_name from 
				(
					select s.id as sId, fd.p_id, u."name" as uname, fa.fa_id, u.mob_number ,u.designation,s.create_by, s.create_date, fa.field_name, '.$tabColumns.' fd.s_id, fd.g_id, s.is_approved, attr.id as attr_id, attr."name",  attr.ui_element as type, fd.value, fd.seq_id from form_data as fd, form_attri_relation as fa, attributes_master as attr, survey_master as s, '.$tabs.' user_master as u where fd.s_id in ('.$sId.') and fd.s_id = s.id and fa.fa_id = fd.fa_id and fa.attri_id = attr.id and '.$tabCondition.' s.create_by = u.user_id 
				) as base left join group_master as grp on base.g_id = grp.group_id
		) as query1 left join
		(
			select s.id as sId, st."name" as state, dt."name" as district, '.$sqlAlias.' from survey_master as s, '.$sqlTables.' states_master as st, district_master as dt	where s.id in ('.$sId.') '.$tabSubCondition.' and '.$condition.' dt.state_code = st.mc and st.mc= \''.$_SESSION['user']['state_code'].'\'
		) as query2 on query1.sId = query2.sId';
		return $this->runSql($sql);
	}

	public function getSurveyIds($regionIds, $formId, $columnName)
	{
		$sql = "select * from survey_master where $columnName in ($regionIds) and f_id = $formId order by id desc limit 10";
		return $this->runSql($sql);
	}

	public function getFormsList()
	{
		$sql = "select f_id,description from ".$this->formModel->tableName;
		$dataSet = $this->runSql($sql);
		return $dataSet;
	}
	public function getColumToSearchInSurvey($formId)
	{
		switch ($formId) {
			case '5':
				return 'comp_id';
				break;
			case '6':
				return 'b_id';
				break;
			case '4':
				return 'b_id';
				break;
			case '2':
				return 'plot_id';
				break;
			case '3':
				return 'plot_id';
				break;
			default:
				return false;
				break;
		}
	}
	/**
	 * @param  ids integer id of the region can be group of plots, compartment or blocks
	 * @param  formId the form to be selected , based on these the plots, compartment or blocks will be returned
	 * @return array
	 */
	public function getAllSurveyRegions($ids, $formId)
	{
		$userSelection = explode(",", $ids);
		$userInput = "";
		foreach ($userSelection as $val) {
			$det = explode(":", $val);
			if($det[0] == -1)
			{
				continue;
			}
			$userInput = $det;
		}

		if($userInput != ""):
			$sql = "";
			$cnt = 0;
			$order = $this->utilityModel->getOrders();
			
			if(in_array($formId, array('6','4')))
			{
				unset($order[array_search('plot', $order)]);
				unset($order[array_search('compartment', $order)]);
			}
			if($formId == '5')
			{
				unset($order[array_search('plot', $order)]);
			}
			$order = array_reverse($order);
			
			foreach ($order as $h) {
				$cnt++;
				if($h == $userInput[1])
				{
					$sql .= "select id from ".$h."_master where id = $userInput[0]";
					break;
				}else{
					$sql .= "select id from ".$h."_master where parent_id in (";
				}
			}
			$sql = $sql.str_repeat(")", $cnt-1);
			
			$regionIds = $this->runSql($sql);
			$regionIds = array_column($regionIds, 'id');
			return $regionIds;
		else:
			return false;
		endif;
	}

	public function updateData($sId, $assign)
	{
		$status = $assign == "Excluded" ? 'true' : 'false';
		$sql = "Update $this->tableName set is_approved = ".$status." where id = $sId";
		$this->runSql($sql);
		return true;
	}

	public function begin() {
		$this->dbHandler = Database::connection($this->database);

		$query = 'begin;';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("begin: ".$error);
			return false;
		}
	}

	public function end() {
		$this->dbHandler = Database::connection($this->database);
		$query = 'commit;';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$this->dbHandler = null;
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("end: ".$error);
			$this->rollback();
			return false;
		}
	}

	public function rollback() {
		$this->dbHandler = Database::connection($this->database);
		$query = 'rollback;';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$this->dbHandler = null;
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("rollback: ".$error);
			return false;
		}
	}

	public function getOptions($attr, $sId, $fId)
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select id,values from ".$this->formModel->valuesTable." where attr_id = $attr";
		$stmt = $this->dbHandler->prepare($query);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return array($result, true);
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return array($error, false);
		} finally {
			$this->dbHandler = null;
		}
	}

	public function currentValues($attrId, $sId, $fId, $seqId, $gId)
	{
		$this->dbHandler = Database::connection($this->database);
		$formAttrId = "select fa_id from ".$this->formModel->formAttrTable." where attri_id = ? and f_id = ?";
		$stmt = $this->dbHandler->prepare($formAttrId);	
		$stmt->bindParam(1, $attrId, \PDO::PARAM_INT);
		$stmt->bindParam(2, $fId, \PDO::PARAM_INT);
		try
		{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			$faId = $result[0]['fa_id'];
			if($gId == "")
			{
				$selectedValues = "select s_id, fa_id, value from ".$this->dataTable." where fa_id = ? and s_id = ?";
				$stmt = $this->dbHandler->prepare($selectedValues);	
				$stmt->bindParam(1, $faId, \PDO::PARAM_INT);
				$stmt->bindParam(2, $sId, \PDO::PARAM_INT);
			}else{
				$selectedValues = "select s_id, fa_id, value from ".$this->dataTable." where fa_id = ? and s_id = ? and seq_id = ? and g_id = ?";
				$stmt = $this->dbHandler->prepare($selectedValues);	
				$stmt->bindParam(1, $faId, \PDO::PARAM_INT);
				$stmt->bindParam(2, $sId, \PDO::PARAM_INT);
				$stmt->bindParam(3, $seqId, \PDO::PARAM_INT);
				$stmt->bindParam(4, $gId, \PDO::PARAM_INT);
			}
			try
			{
				$stmt->execute();
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
				$result = $stmt->fetchAll();
				$type = $this->runSql("select ui_element from ".$this->formModel->attrTable." where id = ".$attrId);
				$type = isset($type[0]['ui_element']) ? $type[0]['ui_element'] : null; 
				return array($result,true, $type, $faId);
			} catch(\PDOException $e) {
				$error = $e->getMessage();
				return array($error,false,false);
			}
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return array($error,false,false);
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getPossibleValues($attrVal, $count=false)
	{
		$this->dbHandler = Database::connection($this->database);
		$fields = $count ? 'count(*)' : 'id,values';
		$query = "select $fields from ".$this->formModel->valuesTable." where attr_id = ? order by weight";
		$stmt = $this->dbHandler->prepare($query);	
		$stmt->bindParam(1, $attrVal, \PDO::PARAM_INT);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function updateSingleData($data) {
		$this->dbHandler = Database::connection($this->database);

		$setAdd = isset($data[':pid']) ? ', p_id = :pid' : '';
		$setAdd .= isset($data[':seq']) ? ', seq_id = :seq' : '';

		$query = "update ".$this->dataTable." set value = :newValue".$setAdd." where id = :id";

		//error_log($query);
		
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute($data);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('updateSingleData: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function deleteFormData($data) {
		$this->dbHandler = Database::connection($this->database);

		if(!is_array($data) || sizeof($data) == 0) {
			error_log('deleteFormData: No data provided');
			return false;
		}

		$whereArray = $bindArray = array();
		foreach($data as $key => $value) {
			$newKey = ':'.$key;
			$bindArray[$newKey] = $value;
			$whereArray[] = $key.' = '.$newKey;
		}

		$whereStr = implode(" and ", $whereArray);

		$query = "delete from ".$this->dataTable." where ".$whereStr;

		error_log($query);
		
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('deleteFormData: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function updateEditData($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$ids = $data['ids'];
		$dataVal = $data['dataVal'];
		list($attrId, $sId, $fId, $seqId, $gId, $faId, $pId) = explode("::", $ids);
		$checkRef = $this->checkRefTableOptions($faId, $sId,$pId, true);
		$tableNm = $checkRef['ref_table'] ? "species_master" : $this->formModel->valuesTable;
		$column = $checkRef['ref_table'] ? "local_name||' - '||sc_name" : "values";
		$error = 0;
		if(is_array($dataVal))
		{
			$this->runSql("begin;");
			$this->runSql("delete from $this->dataTable where fa_id = $faId and s_id =$sId and p_id = $pId");
			foreach ($dataVal as $value) {
				$pv = $this->runSql("select $column as values from $tableNm where id = ".$value);
				$pv = $checkRef['ref_table'] ? $pv[0]['values'] : strtolower($pv[0]['values']);
				$sql = "insert into $this->dataTable (s_id, p_id, seq_id, g_id, value, fa_id) values ($sId, $value, $seqId, NULLIF('$gId','')::integer , '".$pv."', $faId)";
				$stmt = $this->dbHandler->prepare($sql);
				try{
					$stmt->execute();
				} catch(\PDOException $e) {
					$error = 1;
				}
			}
			if($error)
			{
				$this->runSql("rollback;");
				echo 0;
			}else{
				$this->runSql("commit;");
				echo 1;
			}
		}else{
			$this->runSql("begin;");
			if($gId != "") {
				$this->runSql("delete from $this->dataTable where fa_id = ".$faId." and s_id =".$sId." and seq_id = ".$seqId." and g_id =".$gId);	
			} else {
				$this->runSql("delete from $this->dataTable where fa_id = ".$faId." and s_id =".$sId);
			}
			$pv = $this->runSql("select id,values from ".$this->formModel->valuesTable." where attr_id = ".$attrId);
			$possibleValId = "";
			$possibleValue = $dataVal;
			if(!empty($pv))
			{
				$pvList = array_combine(array_column($pv, 'id'), array_column($pv, 'values'));
				$possibleValId = $dataVal;
				$possibleValue = strtolower($pvList[$dataVal]);
			}
			$sql = "insert into $this->dataTable (s_id, p_id, seq_id, g_id, value, fa_id) values ($sId, NULLIF('$possibleValId','')::integer, $seqId, NULLIF('$gId','')::integer ,NULLIF('$possibleValue','')::text, $faId)";
			$stmt = $this->dbHandler->prepare($sql);
			try{
				$stmt->execute();
			} catch(\PDOException $e) {
				$error = 1;
			}
			if($error)
			{
				$this->runSql("rollback;");
				echo 0;
			}else{
				$this->runSql("commit;");
				echo 1;
			}
		}
	}

	public function checkRefTableOptions($faId, $sId, $pId, $justCheck=false)
	{
		$this->dbHandler = Database::connection($this->database);
		$condition = '';
		if($pId != '')
		{
			$condition = 'and p_id = ?';
		}
		$query = "select ref_table, p_id from ".$this->dataTable." where fa_id = ? and s_id = ? $condition";
		$stmt = $this->dbHandler->prepare($query);	
		$stmt->bindParam(1, $faId, \PDO::PARAM_INT);
		$stmt->bindParam(2, $sId, \PDO::PARAM_INT);
		if($pId != '')
		{
			$stmt->bindParam(3, $pId, \PDO::PARAM_INT);
		}
		try
		{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			if($justCheck){
				return $result;
			}
			if($result['ref_table']):
				$query = "select id, local_name||' - '||sc_name as values from species_master where habit_id in (
							select habit_id from species_master where id = ? and create_by = '".$_SESSION['user']['user_id']."'
					) and create_by = '".$_SESSION['user']['user_id']."'";
				$stmt = $this->dbHandler->prepare($query);	
				$stmt->bindParam(1, $result['p_id'], \PDO::PARAM_INT);
				$stmt->execute();
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
				$result = $stmt->fetchAll();
				return array($result, 'checkbox', true);
			else:
			 	return array(null, null, false);
			endif;
		} catch(\PDOException $e) {
			echo $e;exit;
		}
	}
}