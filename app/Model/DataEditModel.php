<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
use IFMT\App\Model\UtilityModel;
/**
* Data Edit Model 
* Standard File
*/
class DataEditModel extends BaseModel
{
	private $divisionMaster;

	public function __construct()
	{
		parent::__construct();
		$this->divisionMaster = "division_master ";
		$this->utilityModel = new UtilityModel();
	}

	public function getUserDivision($userId)
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select id from ".$this->divisionMaster." where create_by = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $userId, \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return array_column($result, 'id');
		} catch(\PDOException $e) {
			$e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function downloadRawData()
	{
		$pa = "select * from static_plot_approach as a,static_plant_species as b where a.user_id = ".
				$this->session['user']['user_id']." and a.id = b.approach_id";
		$pd = "select * from static_plot_description as a,static_plantation as b where a.user_id = ".
				$this->session['user']['user_id']." and a.id = b.description_id";
		$pe = "select * from static_plot_enumeration as a,
				static_bamboo as b,static_climber as c,
				static_grass as d,static_herb as e,static_sapling as f,static_seedling as g,
				static_shrub as h,static_tree as i
				where a.id in (select id from static_plot_enumeration where 
				user_id in (select user_id from user_master where create_by = 'udaipur@fes')) ".
				"and a.id = b.enumeration_id and a.id = c.enumeration_id and a.id = d.enumeration_id
				and a.id = e.enumeration_id and a.id = f.enumeration_id and a.id = g.enumeration_id
				and a.id = h.enumeration_id and a.id = i.enumeration_id";
		echo $pe;exit;
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($pe);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		} catch(\PDOException $e) {
			echo $e->getMessage();
			return false;
		}
		echo '<pre>';print_r($result);exit;
	}
}