<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
 * 	Reporting Model
 */
class ReportModel extends BaseModel
{
	protected $dbHandler;
	private $speciesAttributes;
	function __construct()
	{
		parent::__construct();
		$this->speciesAttributes = array('324');
	}

	public function getRanges()
	{
		$divId = $this->getUserDiv();
		$sql = "select id,name from range_master where parent_id = '$divId'";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		if(!empty($result))
		{
			return array_combine(array_column($result, 'id'), array_column($result, 'name'));	
		}else{
			return array();
		}
		
	}

	public function getCh1Report()
	{
		$formId = 2;
		$divId = $this->getUserDiv();
		$sql = 'select s.comp_id, p."name" as plot, c."name" as compartment, b."name" as block, r."name" as range, d."name" as division, fd.s_id, fd.fa_id, a."name",fd.value, case when (fd.ref_table is true) then (select ht.name from species_master sp, habit_type ht where sp.id = fd.p_id and sp.habit_id = ht.id) else null end as habit 
			from form_data fd, form_attri_relation fa, attributes_master a, survey_master s, compartment_master c, plot_master p, block_master b, range_master r, division_master d where s.id = fd.s_id and fd.fa_id=fa.fa_id and fa.attri_id = a.id and s.comp_id = c.id and s.plot_id = p.id and s.b_id = b.id and b.parent_id = r.id and r.parent_id = d.id and fd.s_id in (		select s.id from survey_master s where s.f_id = '.$formId.' and s.b_id in ( select id from block_master where parent_id in ( select id from range_master where parent_id = '.$divId.')))';
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		$formatedResultSet = array();
		foreach ($result as $key => $value) {
			$compId = $value['comp_id'];
			$sId = $value['s_id'];
			unset($value['s_id']);
			unset($value['comp_id']);
			$formatedResultSet[$compId]['division'] = $value['division'];
			$formatedResultSet[$compId]['range'] = $value['range'];
			$formatedResultSet[$compId]['block'] = $value['block'];
			$formatedResultSet[$compId]['comp'] = $value['compartment'];
			$formatedResultSet[$compId]['plot'] = $value['plot'];
			if(in_array($value['fa_id'], $this->speciesAttributes))
			{
				$habit = $value['habit'] == "" ? 'species' : $value['habit'];
				$formatedResultSet[$compId][$sId][$habit][] = $value['name'].":".$value['value'];
			}else{
				$formatedResultSet[$compId][$sId][$value['fa_id']][] = $value['name'].":".$value['value'];
			}
			
		}
		return $formatedResultSet;
	}

	public function getCh2aReport()
	{
		$formId = 3;
		$divId = $this->getUserDiv();
		$sql = "select DISTINCT(p.name) AS PLOT, d.name,r.name as range,b.name as block,c.name as compartment,'none' as Sub_compartment,st_area(c.geom)/10000 as TotaL_area,'0.1' as Area_enumerated ,'none' as Sampling_method_if_partial, extract(year from s.create_date)::text from form_data fd, form_attri_relation fa, attributes_master a,survey_master s, compartment_master c, plot_master p, block_master b, range_master r,division_master d where s.id = fd.s_id and fd.fa_id=fa.fa_id and fa.attri_id = a.id and s.comp_id = c.id and s.plot_id = p.id and s.b_id = b.id and b.parent_id = r.id and r.parent_id = d.id and fd.s_id in ( select s.id from survey_master s where s.f_id = $formId and s.b_id in ( select id from block_master where parent_id in ( select id from range_master where parent_id = $divId) )) ORDER BY range, block, compartment, plot";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}
		return $result;
	}

	public function getCh2bReport()
	{
		$formId = 3;
		$divId = $this->getUserDiv();
		$sql = "select a.value, sp.sc_name, ht.name, a.s_id, a.fa_id, a.g_id from (select fd.value, a.name, fa.fa_id, fd.s_id, fd.p_id, fd.g_id from form_data fd, attributes_master a, form_attri_relation fa where fd.s_id in ( select s.id from survey_master s where s.f_id = $formId and s.b_id in ( select id from block_master where parent_id in ( select id from range_master where parent_id = $divId ))) and fd.fa_id = fa.fa_id and fa.attri_id = a.id and fd.fa_id in (4,126)) a left join species_master as sp on a.p_id = sp.id left join habit_type as ht on sp.habit_id = ht.id";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo $error;
		}
		$tmp = array();
		foreach ($result as $value) {
			$tmp[$value['s_id']][$value['g_id']][$value['fa_id']] = $value['value'];
			if(!isset($tmp[$value['s_id']][$value['g_id']]['type']))
			{
				$tmp[$value['s_id']][$value['g_id']]['type'] = $value['name'];
			} 
		}
		$tmp = $this->filterResult($tmp, 3);
		return $this->reformatCh2b($tmp);
	}

	public function getCh3Report($rangeId)
	{
		$formId = 3;
		$divId = $this->getUserDiv();
		$plotCount = $this->getplotCounts($rangeId);
		$specieSql = "select fd.value, fd.fa_id, fd.g_id, fd.s_id,s.comp_id, case when (fd.ref_table is true)  then ( select ht.name from species_master sp, habit_type ht where sp.id = fd.p_id and sp.habit_id = ht.id ) else null end as habit  from form_data fd, survey_master s where fd.s_id = s.id and fd.s_id in  (  select id from survey_master where b_id in  (  select id from block_master where parent_id = $rangeId ) ) and fd.fa_id in(4,16)";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($specieSql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}
		$cleanResult = $this->fetchResultWithGirthValues($result);
		$cleanResult = array_filter($this->filterHabit($cleanResult));
		$finalResult = array();
		foreach ($cleanResult as $value)
		{
			foreach ($value as $k => $v) {
				foreach ($v[4] as $kk=>$vv) {
					$finalResult[$vv]['habit'] = $v['habit'];
					$finalResult[$vv]['girth'][] = $v[16][$kk];
					$finalResult[$vv]['comp_id'][] = $v['comp_id'];
					if(isset($finalResult[$vv]['cnt']))
					{
						$finalResult[$vv]['cnt'] = $finalResult[$vv]['cnt']+1;
					}else{
						$finalResult[$vv]['cnt'] = 1;
					}
				}
			}
		}
		$tmp = array();
		foreach ($finalResult as $key => $value) {
			$tmp[$key]['habit'] = $value['habit'];
			$tmp[$key]['spName'] = $key;
			$compList = $this->compInRange($rangeId);
			$density = $value['cnt'] / $compList;
			$tmp[$key]['density'] = $density;
			$frequecy = (count(array_unique($value['comp_id'])))*100/$compList;
			$tmp[$key]['frequecy'] = $frequecy;
			$netBasalArea = 0;
			foreach ($value['girth'] as $girthVal) {
				$basalArea = ((3.14 * ($girthVal/3.14) * 2)) / (4 *10000);
				$netBasalArea += $basalArea;
			}
			$netBasalArea = $netBasalArea/count($value['girth']);
			$tmp[$key]['basal_area'] = $netBasalArea;
		}
		$totalDensity = 0;
		$totalFreq = 0;
		$totalBasal = 0;
		foreach ($tmp as $value) {
			$totalDensity += $value['density'];
			$totalFreq += $value['frequecy'];
			$totalBasal += $value['basal_area'];
		}
		foreach ($tmp as $key=>$value) {
			$tmp[$key]['rel_den'] = 100*$value['density']/$totalDensity;
			$tmp[$key]['rel_freq'] = 100*$value['frequecy']/$totalFreq;
			$tmp[$key]['rel_basal'] = 100*$value['basal_area']/$totalBasal;
			$tmp[$key]['ivi'] = ($value['density']*100/$totalDensity)+($value['frequecy']*100/$totalFreq)+($value['basal_area']*100/$totalBasal);
		}
		return $tmp;
	}

	public function compInRange($rangeId)
	{
		$sql = "select count(*) as cnt from compartment_master where parent_id in (select id from block_master where parent_id = $rangeId)";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}
		return $result[0]['cnt'];
	}

	public function findArea($compIds)
	{
		$compIds = array_unique($compIds);
		$sql = "select (sum(st_area(geom))/10000)::float as sum from compartment_master where id in (".implode(",", $compIds).")";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}

		return $result[0]['sum'];
	}

	public function fetchResultWithGirthValues($result)
	{ 
		$tmp = array();
		foreach ($result as $value) {
			if(!isset($tmp[$value['s_id']][$value['g_id']]['habit']))
			{
				if($value['habit'] != "")
				{
					$tmp[$value['s_id']][$value['g_id']]['habit'] = $value['habit'];	
				}
			}
			if(!isset($tmp[$value['s_id']][$value['g_id']]['comp_id']))
			{
				$tmp[$value['s_id']][$value['g_id']]['comp_id'] = $value['comp_id'];
			}
			$tmp[$value['s_id']][$value['g_id']][$value['fa_id']][] = $value['value'];
		}
		return $this->filterResult($tmp, 4);
	}

	public function filterHabit($result)
	{
		foreach ($result as $key => $value) {
			foreach ($value as $k => $v) {
				if(!in_array($v['habit'], array('Tree','Herb','Shrub')))
				{
					unset($result[$key][$k]);
				}
			}
		}
		return $result;
	}

	public function getplotCounts($divId)
	{
		$sql = "select count(*) as cnt from plot_master where parent_id in ( select id from compartment_master where parent_id in ( select id from block_master where parent_id in ( select id from range_master where parent_id = $divId ) ) )";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}
		return $result[0]['cnt'];
	}


	public function getCh5Report()
	{
		$formId = 3;
		$divId = 93;
		$sql = "select a.comp, sp.sc_name, sp.local_name, ht.name, a.s_id, a.fa_id, a.g_id, a.value from ( select co.name as comp, fd.value, a.name, fa.fa_id, fd.s_id, fd.p_id, fd.g_id from form_data fd, attributes_master a, form_attri_relation fa, survey_master s, compartment_master co where fd.s_id in ( select s.id from survey_master s where s.f_id = $formId and s.b_id in ( select id from block_master where parent_id in ( select id from range_master where parent_id = $divId ))) and fd.s_id = s.id and s.comp_id = co.id and fd.fa_id = fa.fa_id and fa.attri_id = a.id and fd.fa_id in (2,4,14)) a left join species_master as sp on a.p_id = sp.id left join habit_type as ht on sp.habit_id = ht.id";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo $error;
		}
		$tmp = array();
		foreach ($result as $value) {
			if(!isset($tmp[$value['s_id']][$value['g_id']]['sc_name']))
			{
				$tmp[$value['s_id']][$value['g_id']]['sc_name'] = $value['sc_name'];
			}
			if(!isset($tmp[$value['s_id']][$value['g_id']]['local_name']))
			{
				$tmp[$value['s_id']][$value['g_id']]['local_name'] = $value['local_name'];
			}
			if(!isset($tmp[$value['s_id']][$value['g_id']]['comp']))
			{
				$tmp[$value['s_id']][$value['g_id']]['comp'] = $value['comp'];
			}
			if(in_array($value['fa_id'], array('2','14')))
			{
				$tmp[$value['s_id']][$value['g_id']][$value['fa_id']][] = $value['value'];
			}else{
				$tmp[$value['s_id']][$value['g_id']][$value['fa_id']] = $value['value'];
			}
			if(!isset($tmp[$value['s_id']][$value['g_id']]['type']))
			{
				$tmp[$value['s_id']][$value['g_id']]['type'] = $value['name'];
			} 
		}
		$tmp = $this->filterResult($tmp, 7);
		$tmp = $this->reformatCh5($tmp);
		return $tmp;
	}

	public function filterResult($resultSet, $matchCase)
	{
		foreach ($resultSet as $key => $value) {
			foreach ($value as $k=>$v) {
				if(count($v) != $matchCase)
				{
					unset($resultSet[$key][$k]);
				}
			}
		}
		return array_filter($resultSet);
	}

	public function reformatCh2b($result)
	{
		$temp = array();
		foreach ($result as $value) {
			foreach ($value as $v) {
				$temp[$v[4]]['name'] = $v[4];
				$temp[$v[4]]['habit'] = $v['type'];
				$temp[$v[4]]['count'] = isset($temp[$v[4]]['count']) ? $temp[$v[4]]['count']+1 : 1;
				switch ($v[126]) {
					case (0 <= intval($v[126]) && intval($v[126]) <= 10):
						$temp[$v[4]][0] = isset($temp[$v[4]][0]) ? $temp[$v[4]][0]+1 : 1;
						break;
					case (10 <= intval($v[126]) && intval($v[126]) <= 20):
						$temp[$v[4]][1] = isset($temp[$v[4]][1]) ? $temp[$v[4]][1]+1 : 1;
						break;
					case (20 <= intval($v[126]) && intval($v[126]) <= 30):
						$temp[$v[4]][2] = isset($temp[$v[4]][2]) ? $temp[$v[4]][2]+1 : 1;
						break;
					case (30 <= intval($v[126]) && intval($v[126]) <= 40):
						$temp[$v[4]][3] = isset($temp[$v[4]][3]) ? $temp[$v[4]][3]+1 : 1;
						break;
					case (40 <= intval($v[126]) && intval($v[126]) <= 50):
						$temp[$v[4]][4] = isset($temp[$v[4]][4]) ? $temp[$v[4]][4]+1 : 1;
						break;
					case (50 <= intval($v[126]) && intval($v[126]) <= 60):
						$temp[$v[4]][5] = isset($temp[$v[4]][5]) ? $temp[$v[4]][5]+1 : 1;
						break;
					case (60 <= intval($v[126]) && intval($v[126]) <= 70):
						$temp[$v[4]][6] = isset($temp[$v[4]][6]) ? $temp[$v[4]][6]+1 : 1;
						break;
					case (70 <= intval($v[126]) && intval($v[126]) <= 80):
						$temp[$v[4]][7] = isset($temp[$v[4]][7]) ? $temp[$v[4]][7]+1 : 1;
						break;
					case (80 <= intval($v[126]) && intval($v[126]) <= 90):
						$temp[$v[4]][8] = isset($temp[$v[4]][8]) ? $temp[$v[4]][8]+1 : 1;
						break;
					case (90 <= intval($v[126])):
						$temp[$v[4]][9] = isset($temp[$v[4]][9]) ? $temp[$v[4]][9]+1 : 1;
						break;
				}
			}
		}
		return $temp;
	}

	public function reformatCh5($result)
	{
		$temp = array();
		$cnt = 0;
		foreach ($result as $value) {
			foreach ($value as $k => $v) {
				$temp[$cnt]['sc_name'] = $v['sc_name'];
				$temp[$cnt]['local_name'] = $v['local_name'];
				$temp[$cnt]['habit'] = $v['type'];
				$temp[$cnt]['parts'] = implode(",", array_filter($v[2]));
				$temp[$cnt]['area'] = $v['comp'];
				/**
				 * total_wt (in gms) to be converted to kgs i.e. tot_wt/1000
				 * multiply the output with 10 to get Potential harvesting quantity per ha
				 */
				$temp[$cnt]['potential'] = array_sum($v[14])*10/1000; 
				$cnt++;
			}
		}

		return $temp;
	}

	public function getUserDiv()
	{
		$userName = $_SESSION['user']['user_id'];
		$sql = "select id from division_master where create_by = '$userName'";
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}
		return isset($result[0]['id']) ? $result[0]['id'] : null;
	}
}