<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* User Model 
* Standard File
*/
class GisDashboardModel extends BaseModel
{
	private $pointTable;
	public function __construct()
	{
		parent::__construct();
		$this->stateCode = $_SESSION['user']['state_code'];
		$this->pointTable = "user_point_data";
		$this->vectorTable = "user_vector_data";
		$this->rasterTable = "user_raster_data";
		ini_set("error_log", "/errors/gisDashboard-error.log");
	}

	function getPointTable()
	{
		return $this->pointTable;
	}

	function getVectorTable()
	{
		return $this->vectorTable;
	}
	
	function getRasterTable()
	{
		return $this->rasterTable;
	}

	function getAllLayers(){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("(select id, name, table_name as layerName, null as url, null as workspace, h_order  from forest_boundary_hierarchy a where state_code = ".$this->stateCode.") union (select id, layer_displayname as name, layer_name as layername, layer_url as url, workspace, '999' as  h_order from layer_info where create_by = '".$_SESSION['user']['user_id']."') order by h_order");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}

	function boundaryVerificationStatus(){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select count(user_id) as status from user_master where data_verified is true and user_id = '".$_SESSION['user']['user_id']."'");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)? $result : $error);		
	}

	function getUploadedPointLayers()
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select id,nice_name,isVerified from $this->pointTable where user_id = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $this->session['user']['user_id'], \PDO::PARAM_STR);
		try
		{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			$this->dbHandler = null;
			return $result;
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			if($this->debug)
			{
				error_log($error['msg']);
			}
		}
		$this->dbHandler = null;
		return false;
	}

	function getUploadedRasterLayers()
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select id,nice_name,layer_name,isVerified from $this->rasterTable where user_id = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $this->session['user']['user_id'], \PDO::PARAM_STR);
		try
		{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			$this->dbHandler = null;
			return $result;
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			if($this->debug)
			{
				error_log($error['msg']);
			}
		}
		$this->dbHandler = null;
		return false;
	}

	function getUploadedVectorLayers()
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select id,nice_name,file_name,isVerified from $this->vectorTable where user_id = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $this->session['user']['user_id'], \PDO::PARAM_STR);
		try
		{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			$this->dbHandler = null;
			return $result;
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			if($this->debug)
			{
				error_log($error['msg']);
			}
		}
		$this->dbHandler = null;
		return false;
	}

	function addUserData($mapType, $attrs)
	{
		if($mapType == "point")
		{
			return $this->savePointData($attrs);
		}elseif($mapType == 'vector'){
			return $this->saveVectorData($attrs);
		}elseif($mapType == 'raster'){
			return $this->saveRasterData($attrs);
		}
	}

	function savePointData($attrs)
	{
		extract($attrs);
		$this->dbHandler = Database::connection($this->database);
		$query = "insert into $this->pointTable (user_id,location_info,nice_name,layer_name,geoserver_url) values (?,?,?,?,?)";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $user_id, \PDO::PARAM_STR);
		$statement->bindParam(2, $location_info, \PDO::PARAM_STR);
		$statement->bindParam(3, $nice_name, \PDO::PARAM_STR);
		$statement->bindParam(4, $layer_name, \PDO::PARAM_STR);
		$statement->bindParam(5, $geoserver_url, \PDO::PARAM_STR);
		try{
			$statement->execute();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			if($this->debug)
			{
				echo $error['msg'];exit;
				error_log($error['msg']);
			}
			return false;
		}
		$this->dbHandler = null;
		return true;
	}

	function saveVectorData($attrs)
	{
		extract($attrs);
		$this->dbHandler = Database::connection($this->database);
		$query = "insert into $this->vectorTable (user_id,file_name,nice_name,layer_name,geoserver_url) values (?,?,?,?,?)";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $user_id, \PDO::PARAM_STR);
		$statement->bindParam(2, $file_name, \PDO::PARAM_STR);
		$statement->bindParam(3, $nice_name, \PDO::PARAM_STR);
		$statement->bindParam(4, $layer_name, \PDO::PARAM_STR);
		$statement->bindParam(5, $geoserver_url, \PDO::PARAM_STR);
		try{
			$statement->execute();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			var_dump($error);exit;
			if($this->debug)
			{
				error_log($error['msg']);
			}
			return false;
		}
		$this->dbHandler = null;
		return true;
	}

	function getLocationData($id)
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select location_info from $this->pointTable where id = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $id, \PDO::PARAM_INT);
		try
		{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			$this->dbHandler = null;
			return $result;
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			if($this->debug)
			{
				error_log($error['msg']);
			}
		}
		$this->dbHandler = null;
		return false;
	}

	function updateVerifyPointVectorLayerUpload($table, $id, $val)
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "update $table set isverified = ? where id = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(2, $id, \PDO::PARAM_INT);
		$statement->bindParam(1, $val, \PDO::PARAM_BOOL);
		try
		{
			$statement->execute();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			if($this->debug)
			{
				error_log($error['msg']);
			}
		}
		$this->dbHandler = null;
		return true;
	}

	function saveRasterData($attrs)
	{
		$dir = $this->publicDir."/files/temp/";
		$jsonFileName = $dir."import".uniqid().".json";
		extract($attrs);
		$this->dbHandler = Database::connection($this->database);
		$query = "insert into $this->rasterTable (user_id,file_name,nice_name,layer_name,geoserver_url) values (?,?,?,?,?)";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $user_id, \PDO::PARAM_STR);
		$statement->bindParam(2, $file_name, \PDO::PARAM_STR);
		$statement->bindParam(3, $nice_name, \PDO::PARAM_STR);
		$statement->bindParam(4, $layer_name, \PDO::PARAM_STR);
		$statement->bindParam(5, $geoserver_url, \PDO::PARAM_STR);
		try{
			$statement->execute();
		}catch(\PDOException $e){
			$error['msg'] = $e->getMessage();
			$error['error'] = "-1";
			var_dump($error);exit;
			if($this->debug)
			{
				error_log($error['msg']);
			}
			return false;
		}
		$this->dbHandler = null;
		/**
		 * Create Json to upload to geoserver
		 */
		$arr = array(
			"import"=>array(
				"targetWorkspace"=>array(
					"workspace"=>array(
						"name"=>"ifmt"
					)
				),
				"data"=>array(
					"type"=>"file",
					"file"=> $file_name
				)
			)
		);
		/**
		 * Write file to path
		 */
		$fp = fopen($jsonFileName, 'w');
		fwrite($fp, json_encode($arr));
		fclose($fp);
		/**
		 * Post Layer
		 */
		$cmd = "curl -u admin:geoserver -XPOST -H 'Content-type: application/json' -d @".$jsonFileName." ".$_ENV['IMPORTURL'];
		exec($cmd);
		/**
		 * Get Layer reference
		 */
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $_ENV['IMPORTURL']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_USERPWD, 'admin' . ':' . 'geoserver');
		$result = curl_exec($ch);
		$response = json_decode($result);
		$lastElem = end($response->imports);
		$id=$lastElem->id;
		/**
		 * Sync Layer
		 */
		exec("curl -u admin:geoserver -XPOST ".$_ENV['IMPORTURL']."/".$id);
		unlink($jsonFileName);
		return true;
	}
}