<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* User Model 
* Standard File
*/
class FormModel extends BaseModel
{
	public $tableName;
	public $attrTable;
	public $valuesTable;
	public $formAttrTable;
	public $groupTable;
	public $groupAttrTable;
	public $speciesTable;
	public $habitTable;
	public $langValuesTable;

	public function __construct()
	{
		parent::__construct();
		$this->tableName = 'form_master';
		$this->attrTable = 'attributes_master';
		$this->valuesTable = 'possible_value';
		$this->formAttrTable = 'form_attri_relation';
		$this->groupTable = 'group_master';
		$this->groupAttrTable = 'group_attri_relation';
		$this->speciesTable = 'species_master';
		$this->habitTable = 'habit_type';
		$this->langValuesTable = 'poss_val_lang_relation';
	}

	public function getAllValues($idArray = array()){
		$this->dbHandler = Database::connection($this->database);

		$whereStr = (sizeof($idArray) > 0) ? " and id in (".implode(", ", $idArray).")" : '';

		$query = 'select * from '.$this->valuesTable.' where true'.$whereStr.' order by values';
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getAllValues: ".$error);
			return false;
		}
		$this->dbHandler = null;
		return $result;
	}

	public function getAllForms(){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare('select * from '.$this->tableName.' order by "description"');
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getAllForms: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getAttrValues($attrId) {
		$this->dbHandler = Database::connection($this->database);

		$query = "select v.*, lv.values as lang_values   
				  from ".$this->valuesTable." as v 
				  left join ".$this->langValuesTable." as lv 
						on lv.id = v.id 
				  where v.attr_id = :attr
				  order by v.weight";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute(array(':attr' => $attrId));
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getAttrValues: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getPossibleValue($pId) {
		$this->dbHandler = Database::connection($this->database);

		$query = "select v.*, lv.values as lang_values   
				  from ".$this->valuesTable." as v 
				  left join ".$this->langValuesTable." as lv 
						on lv.id = v.id 
				  where v.id = :pid
				  order by v.weight";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute(array(':pid' => $pId));
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getPossibleValue: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getAllAttributeValues($selected = array()) {

		$this->dbHandler = Database::connection($this->database);

		$bindArray = array();
		$whereStr = '';
		if(is_array($selected) && sizeof($selected) > 0) {
			foreach($selected as $key => $attr) {
				$newKey = ':attr'.$key;
				$bindArray[$newKey] = $attr;
			}

			$whereStr = " and v.attr_id in (".implode(", ", array_keys($bindArray)).")";
		}

		$query = "select v.*, a.name, lv.values as lang_values   
				  from ".$this->valuesTable." as v 
				  inner join ".$this->attrTable." as a
					  on a.id = v.attr_id and a.ui_element in ('select', 'checkbox')
				  left join ".$this->langValuesTable." as lv 
					  on lv.id = v.id 
				  where true ".$whereStr."
				  order by v.attr_id, v.weight";
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log('getAllAttributeValues: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getHabitTypes() {
		$this->dbHandler = Database::connection($this->database);

		$query = "select h.id, h.name
				  from ".$this->habitTable." as h 
				  where h.type = 'flora' 
				  order by h.id";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getHabitTypes: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getSpeciesByUser($userId, $habitId = 0, $pId = 0) {
		$this->dbHandler = Database::connection($this->database);

		$bindArray = array(':user' => $userId);

		$whereStr = '';
		if($habitId != 0) {
			$bindArray[':habit'] = $habitId;
			$whereStr .= " and sp.habit_id = :habit";
		}

		if($pId != 0) {
			$bindArray[':id'] = $pId;
			$whereStr .= " and sp.id = :id";
		}

		$query = "select sp.habit_id, h.name as habit, sp.id, sp.sc_name, concat(sp.local_name, ' - ', sp.sc_name) as name, sp.local_name, sp.sp_id    
				  from ".$this->speciesTable." as sp 
				  inner join ".$this->habitTable." as h 
				  	on h.id = sp.habit_id
				  where sp.create_by = :user".$whereStr."
				  order by sp.habit_id, sp.sp_id";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getSpeciesByState: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getFormAttributes($formId, $uiElement = '') {
		$this->dbHandler = Database::connection($this->database);

		$whereStr = !empty($uiElement) ? " and a.ui_element = ?" : '';

		$query = "select fa.*, a.name, a.ui_element, a.data_type, ga.group_id, ga.id as group_attr_id, ga.odk_field_rep, ga.app_field_rep,  
					g.group_name, g.odk_table_rep, g.app_table_rep 
				  from ".$this->formAttrTable." as fa 
				  inner join ".$this->attrTable." as a
					  on a.id = fa.attri_id 
				  left join ".$this->groupAttrTable." as ga 
					  on ga.fa_id = fa.fa_id 
				  left join ".$this->groupTable." as g
				  	  on g.group_id = ga.group_id
				  where fa.f_id = ? ".$whereStr."
				  order by a.name";
		$stmt = $this->dbHandler->prepare($query);
		$stmt->bindParam(1, $formId, \PDO::PARAM_INT);

		if(!empty($uiElement)) {
			$stmt->bindParam(2, $uiElement, \PDO::PARAM_STR);
		}

		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getFormAttributes: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getFormAttributeOptions($formId, $uiElement = array()) {
		$this->dbHandler = Database::connection($this->database);

		$bindKey = array();
		if(sizeof($uiElement) > 0) {
			foreach($uiElement as $ui) {
				$newKey = ':'.$ui;
				$bindKey[$newKey] = $ui;
			}
		}

		$whereStr = sizeof($uiElement) > 0 ? " and a.ui_element in (".implode(", ", array_keys($bindKey)).")" : '';

		$bindKey[':form_id'] = $formId;

		$query = "select v.*, a.name, a.ui_element, fa.odk_field_master, fa.fa_id, fa.field_name, 
					ga.group_id, ga.odk_field_rep, ga.app_field_rep, 
					g.odk_table_rep, g.app_table_rep
				  from ".$this->valuesTable." as v
				  inner join ".$this->formAttrTable." as fa 
				  	  on fa.attri_id = v.attr_id and fa.f_id = :form_id
				  inner join ".$this->attrTable." as a
					  on a.id = fa.attri_id and a.data_type != 'month'".$whereStr." 
				  left join ".$this->groupAttrTable." as ga 
					  on ga.fa_id = fa.fa_id 
				  left join ".$this->groupTable." as g
				  	  on g.group_id = ga.group_id
				  order by a.name";
		$stmt = $this->dbHandler->prepare($query);

		try{
			$stmt->execute($bindKey);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getFormAttributeOptions: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getFormDetails($appTable) {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->tableName." where app_table = ?";
		$stmt = $this->dbHandler->prepare($query);
		$stmt->bindParam(1, $appTable, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getFormDetailsById($id) {
		$this->dbHandler = Database::connection($this->database);
		$query = "select * from ".$this->tableName." where f_id = ?";
		$stmt = $this->dbHandler->prepare($query);
		$stmt->bindParam(1, $id, \PDO::PARAM_INT);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setFormODKAttributes($data) {

		if(!isset($data['attr']) || sizeof($data['attr']) == 0) {
			$msg = ['ERROR! attributes are not selected', 'danger'];
			return $msg;
		}

		$this->dbHandler = Database::connection($this->database);

		$errors = array();
		foreach($data['attr'] as $odkField => $formAttrId) {
			
			if($formAttrId == 0) continue;

			$statement = $this->dbHandler->prepare("update ".$this->formAttrTable." set odk_field_master = ? where id = ?");
			$statement->bindParam(1, $odkField, \PDO::PARAM_STR);
			$statement->bindParam(2, $formAttrId, \PDO::PARAM_INT);
			try{
				$statement->execute();
				$this->dbHandler->commit();
			} catch(\PDOException $e) {
				$errors[] = $e->getMessage();
			}
		}

		if(isset($data['group_attr']) && sizeof($data['group_attr']) > 0) {
			foreach($data['group_attr'] as $odkField => $groupAttrId) {
			
				if($groupAttrId == 0) continue;
	
				$statement = $this->dbHandler->prepare("update ".$this->groupAttrTable." set odk_field_rep = ? where id = ?");
				$statement->bindParam(1, $odkField, \PDO::PARAM_STR);
				$statement->bindParam(2, $groupAttrId, \PDO::PARAM_INT);
				try{
					$statement->execute();
					$this->dbHandler->commit();
				} catch(\PDOException $e) {
					$errors[] = $e->getMessage();
				}
			}
		}

		if(sizeof($errors) > 0) {
			$msg = [implode("<br />", $errors), 'danger'];
		} else {
			$msg = ['Selected fields have been successfully mapped', 'success'];
		}
		$this->dbHandler = null;
		return $msg;
	}

	public function setFormAppAttributes($data) {

		if(!isset($data['attr']) || sizeof($data['attr']) == 0) {
			$msg = ['ERROR! attributes are not selected', 'danger'];
			return $msg;
		}

		$this->dbHandler = Database::connection($this->database);

		$errors = array();
		foreach($data['attr'] as $appField => $formAttrId) {
			
			if($formAttrId == 0) continue;

			$statement = $this->dbHandler->prepare("update ".$this->formAttrTable." set field_name = ? where fa_id = ?");
			$statement->bindParam(1, $appField, \PDO::PARAM_STR);
			$statement->bindParam(2, $formAttrId, \PDO::PARAM_INT);
			try{
				$statement->execute();
			} catch(\PDOException $e) {
				$error = $e->getMessage();
				error_log('setFormAppAttributes: Updating attributes - '. $error);
				return false;
			}
		}

		if(isset($data['group_attr']) && sizeof($data['group_attr']) > 0) {
			foreach($data['group_attr'] as $groupId => $groupAttrs) { //$appField => $groupAttrId) {

				$updateFields = array();
				if(sizeof($groupAttrs) > 0) {
					foreach($groupAttrs as $appField => $groupAttrId) {
						if($groupAttrId == 0) continue;
	
						$statement = $this->dbHandler->prepare("update ".$this->groupAttrTable." set app_field_rep = ? where id = ?");
						$statement->bindParam(1, $appField, \PDO::PARAM_STR);
						$statement->bindParam(2, $groupAttrId, \PDO::PARAM_INT);
						try{
							$statement->execute();
							//$this->dbHandler->commit();
						} catch(\PDOException $e) {
							$error = $e->getMessage();
							error_log('setFormAppAttributes: Updating group attributes - '. $error);
							return false;
						}
					}
				}
			}
		}

		$msg = ['Selected fields have been successfully mapped', 'success'];
		$this->dbHandler = null;
		return $msg;
	}
}