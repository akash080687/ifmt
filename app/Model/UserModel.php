<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
use IFMT\App\Model\UtilityModel;
/**
* User Model 
* Standard File
*/
class UserModel extends BaseModel
{
	public $tableName;
	public $regionAllocation;
	public $roleTable;
	public $userRolesTable;

	public $roleAccessTable;

	public $utilityModel;

	public function __construct()
	{
		parent::__construct();
		$this->tableName = 'user_master';
		$this->roleTable = 'roles_master';
		$this->userRolesTable = 'user_roles_relation';

		$this->roleAccessTable = 'access_policy';

		$this->utilityModel = new UtilityModel($this->dbHandler);
		$this->regionAllocation = 'region_allocation';
	}

	public function setUser($data)
	{
		$this->dbHandler = Database::connection($this->database);
		if($this->checkEmailExists($data['email']))
		{
			$statement = $this->dbHandler->prepare("insert into user_master (name,email,address,password,mob_number) VALUES (?,?,?,?,?)");
			$statement->bindParam(1, $data['name'], \PDO::PARAM_STR);
			$statement->bindParam(2, $data['email'], \PDO::PARAM_STR);
			$statement->bindParam(3, $data['address'], \PDO::PARAM_STR);
			$statement->bindParam(4, $data['password'], \PDO::PARAM_STR);
			$statement->bindParam(5, $data['mob_number'], \PDO::PARAM_STR);
			try{
	    		$statement->execute();
	    		$this->dbHandler->commit();
	    		$msg = ['You have been registered successfully.','success'];
	    	} catch(\PDOException $e) {
	    		$msg = [$sql . "<br>" . $e->getMessage(),'danger'];
	    	}	
		}else{
			$msg = ['The email id you provided already exists.','danger'];
		}
		$this->dbHandler = null;
		return $msg;
	}

	public function setODKUser($data)
	{
		$this->dbHandler = Database::connection($this->database);
		
		$statement = $this->dbHandler->prepare("insert into user_master (user_id,name,designation,mob_number) VALUES (?,?,?,?)");
		$statement->bindParam(1, $data['user_id'], \PDO::PARAM_STR);
		$statement->bindParam(2, $data['name'], \PDO::PARAM_STR);
		$statement->bindParam(3, $data['designation'], \PDO::PARAM_STR);
		$statement->bindParam(4, $data['mob_number'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$userId = $this->dbHandler->lastInsertId();
			$this->dbHandler->commit();
			return $userId;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function checkEmailExists($email, $info=false)
	{
		$condition = "email = '$email'";
		$sql = "SELECT * from ".$this->tableName." where ".$condition;
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		
		if($info)
		{
			return $result;
		} else {
			return (count($result) == 0 ? true : false);
		}
		
	}

	public function checkLogin($data) {

		$this->dbHandler = Database::connection($this->database);

		$statement = $this->dbHandler->prepare("select * from ".$this->tableName." where email = ? and password = ?");
		$statement->bindParam(1, $data['email'], \PDO::PARAM_STR);
		$statement->bindParam(2, $data['password'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();

			if(count($result) != 0)
			{
				$status = true;
				$_SESSION['user'] = $result;
				$msg = ["Logged in successfully.",'success'];
			}else{
				$status = false;
				$msg = ["Error in login. Please verify the credentials provided.",'danger'];
			}
			return compact('msg','status');
		}catch(\PDOException $e){
			$error = $e->getMessage();
		} finally {
			$this->dbHandler = null;
		}		
	}

	public function checkLoginByUserID($data) {

		$this->dbHandler = Database::connection($this->database);

		if(isset($data['username']))
		{
			$prepareArray = array(
				':user' => $data['username'],
				':password' => $data['password']
			);	
		}else{
			$prepareArray = array(
				':user' => $data['email'],
				':password' => $data['password']
			);
		}
		
		$statement = $this->dbHandler->prepare("select * from ".$this->tableName." where user_id = :user and password = :password");
		try{
			$statement->execute($prepareArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();
			
			if($statement->rowCount() > 0) {
				$status = true;
				$_SESSION['user'] = $result;
				$msg = ["Logged in successfully.",'success'];
			} else {
				$status = false;
				$msg = ["Error in login. Please verify the credentials provided.",'danger'];
			}
		} catch (\PDOException $e) {
			$error = $e->getMessage();
			$status = false;
			$msg = ["Error in login. Please contact system administrator.",'danger'];
		} finally {
			$this->dbHandler = null;
		}
		return compact('msg','status');
	}
	
	public function checkCredentials($data, $info=false)
	{
		$statement = $this->dbHandler->prepare("select * from ".$this->tableName." where email = ? and password = ?");
		$statement->bindParam(1, $data['email'], \PDO::PARAM_STR);
		$statement->bindParam(2, $data['password'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		
		if($info)
		{
			return $result;
		} else {
			return (count($result) == 0 ? true : false);
		}
		
	}

	public function getUser($data)
	{
		$this->dbHandler = Database::connection($this->database);

		$user = $this->checkCredentials($data, true);
		if(count($user) != 0)
		{
			$status = true;
			$_SESSION['user'] = $user[0];
			$msg = ["Logged in successfully.",'success'];
		}else{
			$status = false;
			$msg = ["Error in login. Please verify the credentials provided.",'danger'];
		}
		$this->dbHandler = null;
		return compact('msg','status');
	}

	public function getRolePrivileges($roleId) {

		$this->dbHandler = Database::connection($this->database);

		$query = "select accesspolicy
				  from ".$this->roleAccessTable." as ra
				  where ra.role_id = :role";
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute(array(':role' => $roleId));
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log('getRolePrivileges: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getUserPrivileges($userId) {

		$this->dbHandler = Database::connection($this->database);

		$query = "select ra.accesspolicy
				  from ".$this->roleAccessTable." as ra 
				  inner join ".$this->userRolesTable." as ur 
				  	on ur.role_id = ra.role_id and ur.user_id = :user and ur.start_date <= '".date('Y-m-d H:i:s')."' 
				  	and (case when ur.end_date is null or ur.end_date >= '".date('Y-m-d H:i:s')."' then true else false end)";
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute(array(':user' => $userId));
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log('getUserPrivileges: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getUserDefaultRegion($divisionId) {

		$this->dbHandler = Database::connection($this->database);

		$query = "select d.id as division_id, d.name as division_name, c.name as circle_name, c.parent_id as state_code 
				  from ".$this->utilityModel->divisionTable." as d  
				  inner join ".$this->utilityModel->circleTable." as c
				  	on c.id = d.parent_id  
				  where d.id = :division";
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->bindParam(':division', $divisionId);
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log('getUserDefaultRegion: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getUserAllocation($userId) {

		$this->dbHandler = Database::connection($this->database);

		$query = "select * from ".$this->regionAllocation." where user_id = :user";
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->bindParam(':user', $userId);
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log('getUserAllocation: '.$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getUsersByFilter($filters = array()) {
		$this->dbHandler = Database::connection($this->database);

		$bindArray = $whereArray = $inArray = array();
		if(isset($filters['where']) && is_array($filters['where'])) {
			foreach($filters['where'] as $key => $value) {
				$newKey = ':'.str_replace(array('u.', 'ur.'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' = '.$newKey;
			}
		}

		if(isset($filters['not']) && is_array($filters['not'])) {
			foreach($filters['not'] as $key => $value) {
				$newKey = ':'.str_replace(array('u.', 'ur.'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' != '.$newKey;
			}
		}

		if(isset($filters['in']) && is_array($filters['in'])) {
			foreach($filters['in'] as $key => $value) {
				$newKey = ':role'.$key;
				$bindArray[$newKey] = $value;
				$inArray[] = $newKey;
			}
		}

		$inString = sizeof($inArray) > 0 ? " and ur.role_id in (".implode(",", $inArray).")" : "";

		$whereStr = sizeof($whereArray) > 0 ? " and ".implode(" and ", $whereArray) : '';

		$orderBy = isset($filters['order']) ? " order by ".implode(", ", $filters['order']) : '';

		$fields = isset($filters['fields']) && $filters['fields'] == 'count' ? 'count(u.user_id) as total_rows' : 'u.*';

		$query = "select ".$fields." 
				  from ".$this->tableName." as u 
				  inner join ".$this->userRolesTable." as ur 
					  on ur.user_id = u.user_id 
					  and ur.start_date <= '".date('Y-m-d H:i:s')."' 
				  	  and (case when ur.end_date is null or ur.end_date >= '".date('Y-m-d H:i:s')."' then true else false end)".$inString."
				  where true ".$whereStr.$orderBy;
		
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);

			if(isset($filters['resultset']) && $filters['resultset'] == 'row') {
				$result = $stmt->fetch();
				if(isset($filters['fields']) && $filters['fields'] == 'count') {
					return $result['total_rows'];
				}
			} else {
				$result = $stmt->fetchAll();
			}
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getUsersByFilter: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getSessionUser()
	{
		return isset($_SESSION['user']) ? $_SESSION['user'] : null;
	}

	public function regionAllocated()
	{
		$userId = $_SESSION['user']['user_id'];
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select region_value, region_type from ".$this->regionAllocation." where user_id = ?");
		$statement->bindParam(1,$userId, \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		if(isset($result[0]))
		{
			$result = $result[0];
			unset($result[0]);
		}
		return $result;
	}

	public function getRoles() {
		$this->dbHandler = Database::connection($this->database);

		$statement = $this->dbHandler->prepare("select role_id, role_name, role_desc from ".$this->roleTable." where true order by role_name");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getRoles: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getUserRoles($filters = array()) {
		$this->dbHandler = Database::connection($this->database);

		$bindArray = $whereArray = array();
		if(isset($filters['where']) && is_array($filters['where'])) {
			foreach($filters['where'] as $key => $value) {
				$newKey = ':'.str_replace('.', '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' = '.$newKey;
			}
		}

		$whereStr = sizeof($whereArray) > 0 ? " and ".implode(" and ", $whereArray) : '';

		$query = "select ur.user_id, ur.role_id, r.role_name 
				  from ".$this->userRolesTable." as ur 
				  inner join ".$this->roleTable." as r 
				  	on r.role_id = ur.role_id  
				  where ur.start_date <= '".date('Y-m-d H:i:s')."' 
				  	and (case when ur.end_date is null or ur.end_date >= '".date('Y-m-d H:i:s')."' then true else false end)".$whereStr;

		$statement = $this->dbHandler->prepare($query);
		try{
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			if(isset($filters['resultset']) && $filters['resultset'] == 'row') {
				$result = $statement->fetch();
			} else {
				$result = $statement->fetchAll();
			}
			return $result;
		} catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getUserRoles: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setNewUser($data, $return = array()) {

		if(sizeof($data) == 0) {
			return false;
		}

		$this->dbHandler = Database::connection($this->database);

		$bindArray = $keyArray = array();
		foreach($data as $key => $value) {
			$newKey = ':'.$key;
			$bindArray[$newKey] = $value;
			$keyArray[] = $key;
		}

		$returnStr = sizeof($return) > 0 ? " returning ".implode(", ", $return) : '';

		$query = "insert into ".$this->tableName."(".implode(", ", $keyArray).") values (".implode(", ", array_keys($bindArray)).")".$returnStr;

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			if(sizeof($return) > 0) {
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
				return $result;
			}
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setNewUser: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setUpdateUser($data, $where, $return = array()) {

		if(sizeof($data) == 0 || sizeof($where) == 0) {
			return false;
		}

		$this->dbHandler = Database::connection($this->database);

		$bindArray = $updateArray = $whereArray = array();
		foreach($data as $key => $value) {
			$newKey = ':'.$key;
			$bindArray[$newKey] = $value;
			$updateArray[] = $key.' = '.$newKey;
		}

		foreach($where as $key => $value) {
			$newKey = ':'.$key;
			$bindArray[$newKey] = $value;
			$whereArray[] = $key.' = '.$newKey;
		}

		$returnStr = sizeof($return) > 0 ? " returning ".implode(", ", $return) : '';

		$query = "update ".$this->tableName." set ".implode(", ", $updateArray)." where ".implode(", ", $whereArray).$returnStr;

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			if(sizeof($return) > 0) {
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
				return $result;
			}
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setUpdateUser: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setUserRoles($user, $roles) {

		$this->dbHandler = Database::connection($this->database);

		if(empty($user) || sizeof($roles) == 0) {
			return false;
		}

		$insertArray = array();
		$bindArray = array(':user' => $user);
		foreach($roles as $key => $role) {
			$insertArray[] = "(:user, :role".$key.", '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
			$bindArray[':role'.$key] = $role;
		}

		$query = "insert into ".$this->userRolesTable." (user_id, role_id, start_date, timestamp) values ".implode(", ", $insertArray);

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setUserRoles: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function deleteUserRoles($user, $roles) {
		if(empty($user) || sizeof($roles) == 0) {
			return false;
		}

		$this->dbHandler = Database::connection($this->database);

		$bindArray = array(':user' => $user, ':date' => date('Y-m-d H:i:s'));
		foreach($roles as $key => $role) {
			$bindArray[':role'.$key] = $role;
		}

		$query = "update ".$this->userRolesTable." set end_date = :date, timestamp = '".date('Y-m-d H:i:s')."' where user_id = :user and role_id in (".implode(", ", array_keys($bindArray)).")";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("deleteUserRoles: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setUserAllocation($user, $regionType, $allocatedRegions = array()) {

		$this->dbHandler = Database::connection($this->database);

		$bindArray = array(':user' => $user, ':type' => $regionType, ':regions' => (sizeof($allocatedRegions) > 0 ? implode(",", $allocatedRegions) : ''));
		$query = "insert into ".$this->regionAllocation." (user_id, region_type, region_value) values (:user, :type, :regions)";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setUserAllocation: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setUpdateUserAllocations($user, $regionType, $allocatedRegions = array()) {
		
		$this->dbHandler = Database::connection($this->database);

		$bindArray = array(':user' => $user, ':type' => $regionType, ':regions' => (sizeof($allocatedRegions) > 0 ? implode(",", $allocatedRegions) : ''));
		$query = "update ".$this->regionAllocation." set region_type = :type, region_value = :regions where user_id = :user";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setUpdateUserAllocations: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setResetPassword($data) {
		
		$this->dbHandler = Database::connection($this->database);

		$bindArray = array(':user' => $data['user_id'], ':password' => $data['password']);
		$query = "update ".$this->tableName." set password = :password where user_id = :user";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setResetPassword: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getRolesByFilter($filters = array()) {
		$this->dbHandler = Database::connection($this->database);

		$bindArray = $whereArray = $inArray = array();
		if(isset($filters['where']) && is_array($filters['where'])) {
			foreach($filters['where'] as $key => $value) {
				$newKey = ':'.str_replace(array('soundex', '(', ')'), '', $key);
				$bindArray[$newKey] = $value;
				$whereArray[] = $key.' = '.$newKey;
			}
		}

		if(isset($filters['in']) && is_array($filters['in'])) {
			foreach($filters['in'] as $key => $value) {
				$newKey = ':role'.$key;
				$bindArray[$newKey] = $value;
				$inArray[] = $newKey;
			}
		}

		$inString = sizeof($inArray) > 0 ? " and r.role_id in (".implode(",", $inArray).")" : "";

		$whereStr = sizeof($whereArray) > 0 ? " and ".implode(" and ", $whereArray) : '';

		$orderBy = isset($filters['order']) ? " order by ".implode(", ", $filters['order']) : '';

		$fields = isset($filters['fields']) && $filters['fields'] == 'count' ? 'count(r.id) as total_rows' : 'r.*';

		$query = "select ".$fields." from ".$this->roleTable." as r where true ".$whereStr.$inString.$orderBy;

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);

			if(isset($filters['resultset']) && $filters['resultset'] == 'row') {
				$result = $stmt->fetch();
				if(isset($filters['fields']) && $filters['fields'] == 'count') {
					return $result['total_rows'];
				}
			} else {
				$result = $stmt->fetchAll();
			}
			return $result;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("getRolesByFilter: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function setRolePrivileges($role, $privileges) {
		$this->dbHandler = Database::connection($this->database);

		if(empty($role) || sizeof($privileges) == 0) {
			return false;
		}

		$bindArray = array(':role' => $role, ':privileges' => implode(",", $privileges));
		
		$query = "update ".$this->roleAccessTable." set accesspolicy = :privileges where role_id = :role";

		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute($bindArray);
			return true;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("setRolePrivileges: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function updatePassword($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$query = "select count(*) from $this->tableName where user_id = '".$data['username']."'";
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			if($result['count'] == 0)
			{
				$this->setFlash(['No user found','danger']);
				return false;
			}else{
				$this->setFlash(['Password updated successfully','success']);
				return true;
			}
		} catch(\PDOException $e) {
			echo $error = $e->getMessage();exit;
		} finally {
			$this->dbHandler = null;
		}
	}
}