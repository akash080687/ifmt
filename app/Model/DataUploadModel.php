<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* User Model 
* Standard File
*/
class DataUploadModel extends BaseModel
{
	protected $tableRange;
	protected $tableDivision;
	protected $tableBeat;
	protected $tableCompartment;
	protected $tableForestBoundariesUpload;

	public function __construct()
	{
		parent::__construct();
		$this->tableDivision = 'division_master';
		$this->tableRange = 'range_master';
		$this->tableBeat = 'range_master';
		$this->tableCompartment = 'compartment_master';
		$this->tableForestBoundariesUpload = 'forest_boundaries_data_upload';
		$this->stateCode = $_SESSION['user']['state_code'];
	}
	
	function checkForestAdminBoundaries($tablename, $userid){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select id, name from $tablename where create_by = ?");
		$statement->bindParam(1,$userid);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		
		return (isset($result)?$result : $error);
	}
	
	function importShapeFileToPG($shpFilePath, $tableName){
		$this->dbHandler = Database::connection($this->database);
		try{
			if(substr(php_uname(), 0, 7) == "Windows"){
				pclose(popen("start /B ". $command, "r"));
			} else{
				$tablenName = basename($shpFilePath, ".shp");
				$command = 'ogrinfo '.$shpFilePath.' -sql "alter table '.$tablenName.' add COLUMN create_by text"';
				exec($command, $output, $returnVal);
				
				//$command = 'ogrinfo '.$shpFilePath.' -dialect SQLite -sql "update '.$tablenName.' set create_by = \''.$_SESSION['user']['user_id'].'\'"';
				//exec($command." > /dev/null &", $output, $returnVal);
				
				$command = 'ogrinfo '.$shpFilePath.' -sql "alter table '.$tablenName.' add COLUMN s_info text"';
				exec($command, $output, $returnVal);
				
				$sessioninfo = $_SESSION['user']['user_id'].$_COOKIE['PHPSESSID'].time();
            	//$command = 'ogrinfo '.$shpFilePath.' -dialect SQLite -sql "update '.$tablenName.' set s_info = '.$sessioninfo.'"';
				//exec($command." > /dev/null &", $output, $returnVal);

				$command = 'ogr2ogr -append -s_srs EPSG:4326 -t_srs EPSG:4326 -f PostgreSQL PG:"dbname='.$this->database['database'].' host='.$this->database['host'].' user='.$this->database['username'].' password='.$this->database['password'].'" '.$shpFilePath.' -sql "select name, \''.$_SESSION['user']['user_id'].'\' as create_by, \''.$sessioninfo.'\' as s_info from '.$tablenName.'" -nln "'.$this->tableForestBoundariesUpload.'"';

            	exec($command, $output, $returnVal);
				
				$statement = $this->dbHandler->prepare("select * from ".$this->tableForestBoundariesUpload." where s_info = '".$sessioninfo."'");
				try{
					$statement->execute();
					$statement->setFetchMode(\PDO::FETCH_ASSOC);
					$result = $statement->fetchAll();
					if(sizeof($result)>0){
                    	$statement = null;
						$statement = $this->dbHandler->prepare("insert into ".$tableName." (name, create_by, geom) (select name, create_by, geom from ".$this->tableForestBoundariesUpload." where s_info = '".$sessioninfo."') returning id");
						try{
							$statement->execute();
							$statement->setFetchMode(\PDO::FETCH_ASSOC);
							$iResult = $statement->fetchAll();
						}catch(\PDOException $e){
							$errormsg = $e->getMessage();
							$error['error'] = "-1";
							$error['msg'] = $errormsg;
						}
						// Delete data from temporary table $this->tableForestBoundariesUpload
						$statement = $this->dbHandler->prepare("delete from ".$this->tableForestBoundariesUpload." where s_info = '".$sessioninfo."'");
						try{
							$statement->execute();
						}catch(\PDOException $e){
							$errormsg = $e->getMessage();
							$error['delete_error'] = "-1";
							$error['delete_msg'] = $errormsg;
						}
						$this->dbHandler = null;
						return (isset($iResult)?$iResult : $error);
					} else{
						$errormsg = "No data loaded";
						$error['error'] = "-1";
						$error['msg'] = $errormsg;
						$this->dbHandler = null;
						return $error;
					}
				}catch(\PDOException $e){
					$errormsg = $e->getMessage();
					$error['error'] = "-1";
					$error['msg'] = $errormsg;
					$this->dbHandler = null;
					return $error;
				}
			}
		} catch(Exception $e){
			//$e->getMessage();
		}
		$this->dbHandler = null;
		$msg = ["Error while importing shape file in database. Please verify the format is correct.",'danger'];
		return $msg;
	}
	
	public function setParentByCentroid($ids, $tableName, $parentOrder){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select table_name from forest_boundary_hierarchy where id = ".$parentOrder." and state_code = ".$_SESSION['user']['state_code']);
		echo"select table_name from forest_boundary_hierarchy where id = ".$parentOrder." and state_code = ".$_SESSION['user']['state_code'];
    	try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			foreach($ids as $row){
				// Find parent using centroid
				$statement = $this->dbHandler->prepare("update ".$tableName." set parent_id = (select id from ".$result[0]['table_name']." where ST_Contains(geom, ST_GeomFromText((select ST_AsText(ST_Centroid(geom)) from ".$tableName." where id = ".$row['id']."))) is true and create_by = '".$_SESSION['user']['user_id']."') where id = ".$row['id']);
				try{
					$statement->execute();
					$statement->setFetchMode(\PDO::FETCH_ASSOC);
					$uResult = $statement->fetchAll();
				}catch(\PDOException $e){
					$error['msg'] = $e->getMessage();
					$error['error'] = "-1";
                	if (strpos($error['msg'], 'SRID') === false) {
    					return $error;
					} else{
                    	$statement = $this->dbHandler->prepare("update ".$tableName." set parent_id = (select id from ".$result[0]['table_name']." where ST_Contains(geom, ST_GeomFromText((select ST_AsText(ST_Centroid(geom)) from ".$tableName." where id = ".$row['id']."),4326)) is true and create_by = '".$_SESSION['user']['user_id']."') where id = ".$row['id']);
                    	try{
                    		$statement->execute();
                  			$statement->setFetchMode(\PDO::FETCH_ASSOC);
                    		$uResult = $statement->fetchAll();
                		}catch(\PDOException $e){
                        	$error['msg'] = $e->getMessage();
							$error['error'] = "-1";
                        	return $error;
                        }
                   }
				}
			}
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
			$error['msg'] = $errormsg;
			return $error;
		}
 	   return true;
	}

	public function setParentByIntersect($ids, $tableName, $parentOrder){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select table_name from forest_boundary_hierarchy where id = ".$parentOrder." and state_code = ".$_SESSION['user']['state_code']);
		echo"select table_name from forest_boundary_hierarchy where id = ".$parentOrder." and state_code = ".$_SESSION['user']['state_code'];
    	try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			foreach($ids as $row){				
				// Find parent using intersect, match criteria is >95% intersection
				if($tableName == 'plot_master'){
                	$statement = $this->dbHandler->prepare("update ".$tableName." set parent_id = (select b.id from ".$tableName." a, ".$result[0]['table_name']." b where st_within(a.geom, b.geom) and a.create_by='".$_SESSION['user']['user_id']."' and b.create_by='".$_SESSION['user']['user_id']."' and a.id = ".$row['id'].") where id = ".$row['id']);
                } else{
					$statement = $this->dbHandler->prepare("update ".$tableName." set parent_id = (select b.id from ".$tableName." a, ".$result[0]['table_name']." b where st_intersects(a.geom, b.geom) and ((st_area(st_intersection(a.geom, b.geom))/st_area(a.geom))*100) > 95 and a.create_by='".$_SESSION['user']['user_id']."' and b.create_by='".$_SESSION['user']['user_id']."' and a.id = ".$row['id'].") where id = ".$row['id']);
                }
				try{
					$statement->execute();
					$statement->setFetchMode(\PDO::FETCH_ASSOC);
					$uResult = $statement->fetchAll();
				}catch(\PDOException $e){
					$error['msg'] = $e->getMessage();
					$error['error'] = "-1";
					return $error;
				}
			}
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
			$error['msg'] = $errormsg;
			return $error;
		}
 	   return true;
	}
	
	function rollbackBoundariesDataUpload($ids, $tableName){
		$idString = implode(",",$ids);
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("delete from ".$tableName." where id in (".$idString.")");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
			$error['msg'] = $errormsg;
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}
	
	public function getAllHabits()
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select * from habit_type order by id asc");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
			$error['msg'] = $errormsg;
		}
		$this->dbHandler = null;
		return (isset($result)?$result : null);
	}

	public function loadSpeciesList($speciesData, $parentId){
		$this->dbHandler = Database::connection($this->database);
		$failedRows = array('Spcies Id\tLocal name\tScientific name');
		$j=1;
		foreach($speciesData as $row){
			$statement = $this->dbHandler->prepare("insert into species_master (habit_id, sp_id, local_name, sc_name, parent_id, create_by) values(?, ?, ?, ?, ?, ?)");
			try{
				$statement->execute([$row[0], $row[1], $row[2], $row[3], $parentId, $_SESSION['user']['user_id']]);
				$statement->setFetchMode(\PDO::FETCH_ASSOC);
				$result = $statement->fetchAll();
			}catch(\PDOException $e){
				$errormsg = $e->getMessage();
				$error['error'] = "-1";
				$error['msg'] = $errormsg;
            	$failedRows[$j] = $row[1]."\t".$row[2]."\t".$row[3];
            	$j++;
			}
		}
		$this->dbHandler = null;
		return $failedRows;
	}
	
	public function getSpeciesListExists()
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select count(*) as count from species_master where create_by = '".$_SESSION['user']['user_id']."'");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
			$error['msg'] = $errormsg;
		}
		$this->dbHandler = null;
		return isset($result[0]['count']) ? $result[0]['count'] : null;
	}

	function getHabitType($whereField = "'1'", $whereValue = "'1'", $lower = false){
		$this->dbHandler = Database::connection($this->database);
		$whereCondition = ( $lower ? "lower($whereField) = lower('$whereValue')" : "$whereField = '$whereValue'" );
		$statement = $this->dbHandler->prepare("select * from habit_type where $whereCondition");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}
}