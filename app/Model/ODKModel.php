<?php
namespace IFMT\App\Model;
use IFMT\App\Core\Config;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* User Model 
* Standard File
*/
class ODKModel extends BaseModel
{
	protected $coreTable;

	public function __construct()
	{
		parent::__construct();

		$config = Config::init();

		$this->database = $config['database']['pgsql_odk'];
		$this->database['in_use'] = 'pgsql_odk';
	}

	public function setCoreTable($coreTable) {
		$this->coreTable = $coreTable;
	}

	public function getTableColumns() {

		$this->dbHandler = Database::connection($this->database); //print_r($this->dbHandler); exit;

		$query = "select column_name from information_schema.columns where table_name = '".$this->coreTable."' and table_schema = '".$this->database['schema']."' order by column_name";
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			echo $error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getCoreTable() {

		//print_r($this->database); exit;

		$this->dbHandler = Database::connection($this->database); //print_r($this->dbHandler); exit;

		$query = 'select * from '.$this->database['schema'].'."'.$this->coreTable.'"';
		try{
			$stmt = $this->dbHandler->prepare($query);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			echo $error = $e->getMessage();
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}
}