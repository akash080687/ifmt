<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
 *
 * Stats Model 
 */
class StatsModel extends BaseModel
{
	function __construct()
	{
		parent::__construct();
	}

	public function getNoOfRecords()
	{
		$sql = "select f_id from survey_master";
		$this->runQuery($sql);
	}

	public function getNoOfRecordsFormwise()
	{
		$sql = "select count(f_id) from survey_master group by f_id";
		$this->runQuery($sql);
	}

	public function getTotalDivisions()
	{
		$sql = "select count(*) from divsion_master";
		$this->runQuery($sql);
	}

	private function runQuery($query)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($query);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			echo $e->getMessage();exit;
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}
}