<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* User Model 
* Standard File
*/
class UtilityModel extends BaseModel
{
	public $forestHierarchyTable;
	public $circleTable;
	public $divisionTable;
	public $compartmentTable;
	public $rangeTable;
	public $beatTable;
	public $blockTable;
	public $stateTable;
	public $districtTable;

	public $plotTable;

	private $parent;
	private $regionAllocated;
	private $code;
	private $attrTable;
	public $stateCode = '';

	public function __construct()
	{
		parent::__construct();
		$this->forestHierarchyTable = 'forest_boundary_hierarchy';
		$this->circleTable = 'circle_master';
		$this->divisionTable = 'division_master';
		$this->rangeTable = 'range_master';
		$this->compartmentTable = 'compartment_master';
		$this->beatTable = 'beat_master';
		$this->blockTable = 'block_master';
		$this->stateTable = 'states_master';
		$this->districtTable = 'district_master';
		$this->plotTable = 'plot_master';
		$this->attrTable = 'attributes_master';
		$this->stateCode = isset($_SESSION['user']['state_code']) ? $_SESSION['user']['state_code'] : "";
	}

	public function setStateCode($stateCode) {
		$this->stateCode = $stateCode;
	}

	public function getStates() {
		$this->dbHandler = Database::connection($this->database);

		$query = "select s.mc as state_code, s.name as state_name  
				  from ".$this->stateTable." as s
				  order by s.name";
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getStates: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getCircleByDFO($userId = '') {
		$this->dbHandler = Database::connection($this->database);

		$whereStr = !empty($userId) ? " and c.create_by = :user" : "";
		$bindArray = !empty($userId) ? array(':user' => $userId) : array();

		$query = "select c.id, c.name  
				  from ".$this->circleTable." as c 
				  where true ".$whereStr." order by id limit 1";
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetch();
			return $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getCircleByDFO: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getAssignedPlots($regionArray) {
		$this->dbHandler = Database::connection($this->database);

		$bindArray = array();
		$where = '';
		if(isset($regionArray['parents']) && sizeof($regionArray['parents']) > 0) {
			$regionKeys = array();
			foreach($regionArray['parents'] as $key => $value) {
				$key = ':p'.$key;
				$bindArray[$key] = $value;
			}

			$where .= " and p.parent_id in (".implode(", ", array_keys($bindArray)).")";
		}

		$query = "select p.id, p.name, p.parent_id, pr.name as parent_name 
				  from ".$this->plotTable." as p 
				  inner join ".$regionArray['table_name']." as pr 
				  	on p.parent_id = pr.id 
				  where true".$where;
		try{
			$statement = $this->dbHandler->prepare($query);
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getAssignedPlots: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getStateDistrict($stateCode = '') {
		$this->dbHandler = Database::connection($this->database);

		$whereStr = !empty($stateCode) ? " and s.mc = :state" : '';

		$query = "select s.mc as state_code, s.name as state_name, d.mc as district_code, d.name as district_name 
				  from ".$this->districtTable." as d
				  inner join ".$this->stateTable." as s
					  on d.state_code = s.mc".$whereStr."
				  order by s.name, d.name";

		$statement = $this->dbHandler->prepare($query);
		try{
			$bindArray = !empty($stateCode) ? array(':state' => $stateCode) : array();
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getStateDistrict: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	/**
	 * Get user forest hierarchy as per state and region allocated to user
	 * @param double $stateCode: Assigned state code to the user
	 * @param integer $assignedOrder: Assigned region order in the hierarchy to the user, default is 0
	 * @param array $assigned: Array list of assigned region to the user, default is blank array
	 * @return array | boolean: Returns array of region hierarchy in order for the state as well as assigned region
	 * array list according to hierarchy for the user on success, otherwise return false
	 */
	public function getForestHierarchy($assignedOrder = 0, $assigned = array()) 
	{

		if(empty($this->stateCode)) {
			return false;
		}

		$keys = array();
		$values = array();
		if(sizeof($assigned) > 0) {
			foreach($assigned as $k => $val) {
				$newK = ':a'.$k;
				$keys[] = $newK;
				$values[$newK] = $val;
			}
		}

		$return = array();

		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select * from forest_boundary_hierarchy where state_code = :state order by h_order desc");
		try{
			$statement->bindParam(':state', $this->stateCode);
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			$return['hierarchy'] = $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("Hierarchy: ".$error);
			return false;
		}

		$fields = $orderFields = array();
		foreach($result as $hierarchy) {

			$order = $hierarchy['h_order'];

			if($order > $assignedOrder) {
                foreach($result as $parent) {

					if($parent['h_order'] == 6) continue;

                    $currentKey = "h".$parent['h_order'];
					$fields[] = $currentKey.'.id as '.$currentKey.'_id, '.$currentKey.'.name as '.$currentKey.'_name, '.$currentKey.'.parent_id as '.$currentKey.'_parent';
					$orderFields[] = $currentKey.'.name';
                }
			}
		}

		$unionArray = array();
		foreach($result as $hierarchy) {

			$order = $hierarchy['h_order'];

			if($order > $assignedOrder) {
				$l = 0;

				$query = "select ".implode(",", array_unique($fields));

                foreach($result as $parent) {

					if($parent['h_order'] == 6) continue;

                    if($l == 0) {
						$firstTable = $parent['table_name'];
						$childKey = $firstKey = "h".$parent['h_order'];
                        $query .= " from ".$parent['table_name']." as ".$childKey;
                    } else {
						$currentKey = "h".$parent['h_order'];
                        $query .= " inner join ".$parent['table_name']." as ".$currentKey."
									on ".$currentKey.".id = ".$childKey.".parent_id".($parent['table_name'] == 'compartment_master' ? " and ".$currentKey.".geom is NULL" : '');
									
						if($assignedOrder > 0 && $assignedOrder == $parent['h_order']) {
							$query .= " and ".$currentKey.".id in (".implode(",", $keys).")";
						}

						if($parent['h_order'] == 1) {
							$query .= " and ".$currentKey.".parent_id = :state";
						} 

						$childKey = $currentKey;
                    }
                    $l++;
				}
				
				$query .= " order by ".implode(", ", array_reverse(array_unique($orderFields)));
			}
		} 
		
		try{ 
			$statement2 = $this->dbHandler->prepare($query);
			$statement2->execute($values);
			$statement2->setFetchMode(\PDO::FETCH_ASSOC);
			$result2 = $statement2->fetchAll();
			$return['data'] = $result2;
			return $return;
		} catch(\PDOException $e) {
			$error = $e->getMessage();
			error_log("Hierarchy Data: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}
	
	public function forestAdminHierarchy($limit = "All", $maxOrder = 0){
		$state_code=$this->code;
		$this->dbHandler = Database::connection($this->database);
		$whereStr = '';
		$bindArray = array();
		if(!empty($this->stateCode)) {
			$whereStr .= " and state_code = :state";
			$bindArray[':state'] = $this->stateCode;
		}else
		{
			$this->setFlash(['State Code is empty','error']);
			return false;
		}

		if($maxOrder != 0) {
			$whereStr .= " and h_order <= :max_order";
			$bindArray[':max_order'] = $maxOrder;
		}

		$statement = $this->dbHandler->prepare("select * from forest_boundary_hierarchy where true ".$whereStr." order by h_order limit $limit");
		try{
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			return false;
		}
		$this->dbHandler = null;
		return $result;
	}
	
	public function getTableName($id){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select table_name from forest_boundary_hierarchy where id = ?");
		$statement->bindParam(1,$id, \PDO::PARAM_INT);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			
		}catch(\PDOException $e){
			$error = $e->getMessage();
			return false;
		}
		$this->dbHandler = null;
		return $result;
	}
	
	public function getParentField($id){
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select parent_code_field from forest_boundary_hierarchy where id = ?");
		$statement->bindParam(1,$id, \PDO::PARAM_INT);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		$this->dbHandler = null;
		return $result;
	}
	
	function getParentOptions($parentID, $ids=false){
		$tableName = $this->getTableName($parentID);
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select id, name from ".$tableName[0]['table_name']." where create_by = ?");
		$statement->bindParam(1,$_SESSION['user']['user_id'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}
	
	function getChildList($id, $parentCode){
		$tableName = $this->getTableName($id);
		//$parentField = $this->getParentField($id);
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select id, name from ".$tableName[0]['table_name']." where parent_id = $parentCode");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)?$result : $error);
	}

	public function getRegions($regionValue)
	{
		$hirearchy = $this->forestAdminHierarchy();
		
		// Matches with h_order from Forest Hierarchy
		// Always division i.e. 2 -- as per meeting on 25/06/18 
		$regionType = 2;//$regionId['region_type'];
		
		// The area/regions allocate to the user
		// division id allocated This is always one division per DFO -- as per meeting on 25/06/18
		$ids = is_array($regionValue) ? implode(",",$regionValue) : $regionValue;
		$details = array();
		$indexSearch = array_search($regionType, array_column($hirearchy, 'h_order'));
		
		for($i=$indexSearch; $i<count($hirearchy);$i++)
		{
			$condition = "";
			// if($hirearchy[$i]['name'] == 'Compartment')
			// {
			// 	$compParent = $this->getObjectId(strtolower($hirearchy[$i-1]['name']), "forest_boundary_hierarchy", 'id');
			// 	$condition = " and parent_value = $compParent";
			// }
			$parentField = ($hirearchy[$i]['name'] == 'Division') ? "id" : "parent_id";
			$sql = "select id,name,parent_id from ".$hirearchy[$i]['table_name']." where $parentField in (".$ids.") $condition";
			$data = $this->runSql($sql);
			$name = strtolower($hirearchy[$i]['name']);
			$details[$name] = $data;
			$ids = implode(",",array_column($data,'id'));
		}
		return $details;
	}

	public function getOrders()
	{
		$order[0] = "";
		$hirearchy = $this->forestAdminHierarchy();
		foreach ($hirearchy as $key => $value) {
			$order[] = strtolower($value['name']);
		}
		unset($order[0]);
		return $order;
	}

	public function getObjectId($valueToFind, $tableName, $column)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select id from $tableName where LOWER(name) = '$valueToFind'");
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchColumn();
			
		}catch(\PDOException $e){
			echo $error = $e->getMessage();
		}
		$this->dbHandler = null;
		return $result;
	}

	public function getRegionByType($typeData, $regionCode = 0, $userId = '', $firstHierarchy = 2, $regionCodes = array(), $createBy = '') {

		$typeId = $typeData['h_order'];

		$bindArray = array();
		$join = $fields = $where = '';

		if($regionCode != 0) {
			$where .= " and r.parent_id = :parent";
			$bindArray[':parent'] = $regionCode;
		}

		if(sizeof($regionCodes) > 0) {
			$regionKeys = array();
			foreach($regionCodes as $key => $rCode) {
				$key = ':region'.$key;
				$bindArray[$key] = $rCode;
				$regionKeys[] = $key;
			}

			$where .= " and r.id in (".implode(", ", $regionKeys).")";
		}

		if(!empty($createBy)) {
			$where .= " and r.create_by = :creator";
			$bindArray[':creator'] = $createBy;
		}

		$this->dbHandler = Database::connection($this->database);

		$query = "select r.id, r.*".$fields." from ".$typeData['table_name']." as r ".$join." where true ".$where." order by r.name";
		$statement = $this->dbHandler->prepare($query);
		try{
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getRegionByType: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getPlotsByCompartments($compartments) {

		if(!is_array($compartments) || sizeof($compartments) == 0) {
			throw new \Exception('ERROR! compartments not provided to retrieve list of plots');
		}

		$where = '';
		if(sizeof($compartments) > 0) {
			$compKeys = array();
			foreach($compartments as $compId) {
				$key = ':comp'.$compId;
				$bindArray[$key] = $compId;
				$compKeys[] = $key;
			}

			$where .= " and c.id in (".implode(", ", $compKeys).")";
		}

		$this->dbHandler = Database::connection($this->database);

		$query = "select p.id, p.name, p.parent_id 
				  from ".$this->plotTable." as p 
				  inner join ".$this->compartmentTable." as c 
					on c.id = p.parent_id".$where."
				  order by p.name";
		$statement = $this->dbHandler->prepare($query);
		try{
			$statement->execute($bindArray);
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			error_log("getPlotsByDivision: ".$error);
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function getAttrIdsFromName()
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select id,name from ".$this->attrTable);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$errormsg = $e->getMessage();
			$error['error'] = "-1";
		}
		$this->dbHandler = null;
		return (isset($result)? $this->attrReset($result) : $error);	
	}

	private function attrReset($result)
	{
		$tmpArray = array();
		foreach ($result as $value) {
			$tmpArray[$value['id']] = $value['name'];
		}
		return $tmpArray;
	}
}