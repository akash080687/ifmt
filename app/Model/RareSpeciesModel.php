<?php
namespace IFMT\App\Model;
use IFMT\App\Core\BaseModel;
use IFMT\App\Core\Database;
/**
* Data Edit Model 
* Standard File
*/
class RareSpeciesModel extends BaseModel
{
	private $table;
	private $syncTable;
	private $locationTable;
	public function __construct()
	{
		parent::__construct();
		$this->table = "rare_species ";
		$this->syncTable = "rare_data";
		$this->locationTable = "locationdata";
	}

	public function getList($regionId)
	{
		$stateCode = '910800000000000000';
		$this->dbHandler = Database::connection($this->database);
		$query = "select scientific_name || '-' || local_name || ' (' || min_girth || ')' as name, min_girth from ".$this->table." where state_code = ? order by id";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $stateCode, \PDO::PARAM_INT);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
			return $result;
		} catch(\PDOException $e) {
			echo $e->getMessage();exit;
			return false;
		} finally {
			$this->dbHandler = null;
		}
	}

	public function syncData($syncData)
	{
		$dir = $this->publicDir."/files/rare";
		if (!file_exists($dir) && !is_dir($dir)) {
		    mkdir($dir);
		}
		$error = 0;
		$lastId = null;
		$syncData = (array)$syncData;
		$images = (array) $syncData['images'];
		$createAt = date('Y-m-d H:i:s',strtotime(str_replace("_", " ",$syncData['data'][0]->datecreated)));
		if(!empty($syncData))
		{
			$this->runSql('begin;');
			$this->dbHandler = Database::connection($this->database);
			$query = "insert into $this->syncTable (device_id, range, state, block, division, circle, designation, name, name_other, phonenumber, treesperha, area, species_availability, datecreated, buildversion, collector_location, devicename) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id";
			$statement = $this->dbHandler->prepare($query);
			$statement->bindParam(1, $syncData['data'][0]->device_id, \PDO::PARAM_STR);
			$statement->bindParam(2, $syncData['data'][0]->range, \PDO::PARAM_STR);
			$statement->bindParam(3, $syncData['data'][0]->state, \PDO::PARAM_STR);
			$statement->bindParam(4, $syncData['data'][0]->block, \PDO::PARAM_STR);
			$statement->bindParam(5, $syncData['data'][0]->division, \PDO::PARAM_STR);
			$statement->bindParam(6, $syncData['data'][0]->circle, \PDO::PARAM_STR);
			$statement->bindParam(7, $syncData['data'][0]->designation, \PDO::PARAM_STR);
			$statement->bindParam(8, $syncData['data'][0]->name, \PDO::PARAM_STR);
			$statement->bindParam(9, $syncData['data'][0]->name_other, \PDO::PARAM_STR);
			$statement->bindParam(10, $syncData['data'][0]->phonenumber, \PDO::PARAM_STR);
			$statement->bindParam(11, $syncData['data'][0]->treesperha, \PDO::PARAM_INT);
			$statement->bindParam(12, $syncData['data'][0]->area, \PDO::PARAM_STR);
			$statement->bindParam(13, $syncData['data'][0]->species_availability, \PDO::PARAM_STR);
			$statement->bindParam(14, $createAt, \PDO::PARAM_STR);
			$statement->bindParam(15, $syncData['data'][0]->buildversion, \PDO::PARAM_STR);
			$statement->bindParam(16, $syncData['data'][0]->collector_location, \PDO::PARAM_STR);
			$statement->bindParam(17, $syncData['data'][0]->devicename, \PDO::PARAM_STR);
			try{
				$statement->execute();
				$statement->setFetchMode(\PDO::FETCH_ASSOC);
				$returnVal=$statement->fetchAll();
				$lastId = $returnVal[0]['id'];
			} catch(\PDOException $e) {
				// error_log(" $this->syncTable: ".$e->getMessage());
				echo "111 - ". $e->getMessage();exit;
				$error = 1;
			}

			if($error == 1)
			{
				$this->runSql('rollback;');
				return false;
			} 
			else if($lastId != null) 
			{
				$val = 1;
				foreach ($images as $value) {
					$query = "insert into rare_grp_images (rareid, uniq_id, rare_species_name, rare_species_other_name, image) values(?, ?, ?, ?, ?)";
					$statement = $this->dbHandler->prepare($query);
					$statement->bindParam(1, $lastId, \PDO::PARAM_STR);
					$statement->bindParam(2, $value->flag, \PDO::PARAM_STR);
					$statement->bindParam(3, $value->species, \PDO::PARAM_STR);
					$statement->bindParam(4, $value->other_species, \PDO::PARAM_STR);
					$data = base64_decode($value->Image);
					$filename = $dir."/".$lastId."_".$value->flag."_$val.jpeg";
					file_put_contents($filename, $data);
					$statement->bindParam(5, $filename, \PDO::PARAM_STR);
					try{
						$statement->execute();
					} catch(\PDOException $e) {
						echo $e->getMessage();exit;
						$error = 1;
					}
					$val = ($val == 1) ? 2 : 1;
				}

				foreach ($syncData['occurrence'] as $value) 
				{
					$query = "insert into $this->locationTable (rareId, uniq_id, height, girth, altitude, latitude, longitude, accuracy) values(?, ?, ?, ?, ?, ?, ?, ?)";
					$statement = $this->dbHandler->prepare($query);
					$statement->bindParam(1, $lastId, \PDO::PARAM_STR);
					$statement->bindParam(2, $value->sp_id, \PDO::PARAM_STR);
					$statement->bindParam(3, $value->height, \PDO::PARAM_STR);
					$statement->bindParam(4, $value->girth, \PDO::PARAM_STR);
					$statement->bindParam(5, $value->altitude, \PDO::PARAM_STR);
					$statement->bindParam(6, $value->latitude, \PDO::PARAM_STR);
					$statement->bindParam(7, $value->longitude, \PDO::PARAM_STR);
					$statement->bindParam(8, $value->accuracy, \PDO::PARAM_STR);
					try{
						$statement->execute();
					} catch(\PDOException $e) {
						echo $e->getMessage();exit;
						// error_log(" $this->locationTable: ".$e->getMessage());
						$error = 1;
					}
				}
				if($error == 1)
				{
					$this->runSql('rollback;');
					return false;
				}
			}
			if($error == 0)
			{
				$this->runSql('commit;');
				return true;
			}
		}else{
			return false;
		}
	}

	// Get User list
	public function getUsers($rangeId)
	{
		$users = null;
		$this->dbHandler = Database::connection($this->database);
		$query = "select name || '-' || hindiname as name, mobile from forestuser where range = ?";
		$statement = $this->dbHandler->prepare($query);
		$statement->bindParam(1, $rangeId, \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$users=$statement->fetchAll();
		} catch(\PDOException $e) {
			error_log(" $this->locationTable: ".$e->getMessage());
			$error = 1;
		}
		$this->dbHandler = null;
 		return $users;
	}

	/*
	Export Data in Excel without image
	==================================
	select distinct ri.uniq_id, ld.id, rd.id, 'Rajasthan' as state, 'Udaipur' as circle, 
	dm."name", rm."name", bm."name", rd.designation, 
	rd."name", rd.name_other, rd.phonenumber, rd.treesperha, rd.area, rd.species_availability, rd.datecreated,
	rd.collector_location, rd.devicename, ri.rare_species_name, ri.rare_species_other_name, ld.height, ld.girth, 
	ld.altitude, ld.longitude, ld.latitude, ld.accuracy
	from rare_data rd left join rare_grp_images ri on rd.id = ri.rareid, locationdata ld, division_master dm,
	range_master rm, block_master bm
	where rd.division = '91' and ld.rareid = rd.id and ld.uniq_id = ri.uniq_id and dm.id = rd.division::numeric
	and rm.id = rd."range"::numeric and bm.id = rd.block::numeric order by rd.id
	*/

	public function getData($rangeId, $allRange = false)
	{
		$this->dbHandler = Database::connection($this->database);
		$resultSet = null;
		$cond = $allRange == true ? 'select id::text from range_master' : "'$rangeId'";
		$sql = "select rm.name as range, 'Rajasthan' as state, bm.name as block, dm.name as division, 'Udaipur' as circle, rd.designation, rd.name, rd.name_other, rd.phonenumber, rd.treesperha, rd.area, rd.species_availability, rd.datecreated, rd.collector_location, rd.devicename, ld.height, ld.girth, ld.altitude, ld.longitude, ld.latitude, ld.accuracy, ri.rare_species_name, ri.rare_species_other_name, ri.image from rare_data rd, locationdata ld, rare_grp_images ri, range_master rm, division_master dm, block_master bm where rd.id = ld.rareid and ri.rareid = rd.id and ld.uniq_id = ri.uniq_id and rd.range in ( $cond )  and rm.parent_id = dm.id and bm.id::text = rd.block and bm.parent_id = rm.id";
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$resultSet=$statement->fetchAll();
		} catch(\PDOException $e) {
			error_log(" $this->locationTable: ".$e->getMessage());
			$error = 1;
		}
		$this->dbHandler = null;
 		return $resultSet;
	}

	public function getRanges($userId)
	{
		$this->dbHandler = Database::connection($this->database);
		$resultSet = null;
		$sql = "select name,id from range_master where create_by = ?";
		$statement = $this->dbHandler->prepare($sql);
		$statement->bindParam(1, $userId, \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$resultSet=$statement->fetchAll();
		} catch(\PDOException $e) {
			error_log(" $this->locationTable: ".$e->getMessage());
			$error = 1;
		}
		$this->dbHandler = null;
 		return $resultSet;	
	}
}
